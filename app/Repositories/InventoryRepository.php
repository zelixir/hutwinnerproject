<?php
/**
 * Created by PhpStorm.
 * User: samoumbarek
 * Date: 2019-01-12
 * Time: 1:41 PM
 */

namespace App\Repositories;
use App\User;

/**
 * Class InventoryRepository
 * Used to query the inventory of given user
 * @package App\Repositories
 */
class InventoryRepository
{
    /**
     * Returns all the cards in a user inventory.
     * @param User $user
     * @return mixed
     */
    public function getInventoryCards(User $user){
        if(empty($user->inventory()->get()->first())){
            return null;
        }else{
            return $user->inventory()->join('cards','card_id' , '=' , 'id')->get();
        }

    }

    /**
     * Returns all the packs that a user has yet to open.
     * @param User $user
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getInventoryPacks(User $user){
        if(empty($user->unopened_packs()->get()->first())){
            return null;
        }else{
            return $user->unopened_packs()->get();
        }
    }
}