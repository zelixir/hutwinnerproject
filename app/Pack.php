<?php

namespace App;
use Laravel\Scout\Searchable;

use Illuminate\Database\Eloquent\Model;

class Pack extends Model
{
    use Searchable;
    protected $primaryKey ='id';
    protected $table = 'packs';
    protected $asYouType=false;
    protected $fillable = ['name', 'xbox_price', 'ps4_price' , 'author_id', 'image','guaranteed_cards'];

    /**
     * Get the author (user) of the pack
     *
     */
    public function user(){
        return $this->belongsToMany(User::class)->withPivot('amount')->withTimestamps();
    }

    /**
     * Get the cards inside the pack
     *
     */
    public function cards(){
        return $this->belongsToMany(Card::class)
            ->withPivot( 'probability')
            ->withTimestamps();
    }
    public function author(){
        $authorId = $this->author_id;
        if(isset($authorId)){
            $value =  DB::table('user')
                ->select('firstname','lastname')
                ->where('id',$authorId)
            ->get();
            $value = $value->firstname . ' ' . $value->lastname;
        }
        else{
            $value= "Made By Us";
        }
        return $value;
    }
    public function toSearchableArray()
    {
        return array_only($this->toArray(), ['id', 'name']);
    }


    public function getCreatedAtAttribute($date)
    {
        return \Carbon\Carbon::parse($date)->timezone('America/Toronto');
    }

}
