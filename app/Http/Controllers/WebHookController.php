<?php
/**
 * Created by PhpStorm.
 * User: samoumbarek
 * Date: 2019-01-21
 * Time: 12:46 AM
 */

namespace App\Http\Controllers;

use App\Transaction;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Xsolla\SDK\Webhook\WebhookServer;
use Xsolla\SDK\Webhook\Message\Message;
use Xsolla\SDK\Exception\Webhook\XsollaWebhookException;
use Xsolla\SDK\Exception\Webhook\InvalidUserException;
use Xsolla\SDK\Webhook\Message\PaymentMessage;
use Xsolla\SDK\Webhook\Message\RefundMessage;

class WebHookController extends Controller
{


    public function index(Request $request)
    {
        $callback = function (Message $message) {
            switch ($message->getNotificationType()) {
                case Message::USER_VALIDATION:
                    if (is_numeric($message->getUserId())) {
                        $val = $message->getUserId();
                        try {
                            $user = User::find($val);
                        } catch (\Exception $exception) {
                            $user = null;
                        }
                    }

                    if (isset($user) && !empty($user)) {
                        $returnData = [
                            'success' => [
                                'code' => 'valid',
                                'message' => 'NICE',
                            ]
                        ];
                        return Response::json($returnData, 200);

                    } else {
                        throw new InvalidUserException('USER DOES NOT EXIST');
                    }
                    // TODO if user not found, you should throw Xsolla\SDK\Exception\Webhook\InvalidUserException
                    break;
                case Message::PAYMENT:

                    if (is_numeric($message->getUserId())) {
                        $val = $message->getUserId();
                        try {
                            $user = User::find($val);
                            $id = $user->id;
                        } catch (\Exception $exception) {
                            $user = null;
                        }
                        if (isset($user) && !empty($user)) {
                            $purchaseObj = $message->getPurchase();
                            $virtualCurrency = $purchaseObj['virtual_currency'];
                            $quantityCoins = $virtualCurrency['quantity'];
                            $currency = $virtualCurrency['currency'];
                            $amountOfCurrency = $virtualCurrency['amount'];
                            $transaction = $message->getTransaction();
                            $transactionId = $message->getExternalPaymentId();
                            $paymentId = $message->getPaymentId();
                            Transaction::create([
                                'user_id' => $id,
                                'payment_id' => '2',
                                'price' => $amountOfCurrency,
                                'points' => $quantityCoins,
                                'description' => 'User has bought the following number of coins: ' . $quantityCoins
                            ]);
                            $user = User::find($id);
                            Log::info("XSOLLA TRANSACTION: The following User: " . $user->email . " has bought " . $quantityCoins . ", for " . $virtualCurrency['currency']);
                            $user = User::find((int)$message->getUserId());
                            $user->coins += $quantityCoins;
                            $user->save();
                            $returnData = [
                                'success' => [
                                    'code' => 'valid',
                                    'message' => 'NICE',
                                ]
                            ];
                            return Response::json($returnData, 200);
                        } else {
                            throw new XsollaWebhookException();
                        }
                    }
                    break;
                case Message::REFUND:
                    /*
                     * Read on the documentation of refund details trying to retrieve a refund from a user and then update the database accordingly
                     *
                     */
                    $refundInfo = $message->getRefundDetails();
                    throw new XsollaWebhookException('Refund was not made. Please Review request');
                    break;
                default:
                    throw new XsollaWebhookException('Notification type not implemented');
            }
        };
        CLOSELOG();
        $webhookServer = WebhookServer::create($callback, env('PROJECT_KEY'));
        $webhookServer->start(null, false);


    }
}