<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminLoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:admin', ['except' => ['logout']]);
    }

    public function loginForm(){
        return view('auth.admin_login');
    }

    public function login(Request $request){

        //Validate information
        $this->validate($request, [
           'email' => 'required|email',
           'password' => 'required|min:8'
        ]);

        //Attempt to login
        if(Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)){
           //Login successful
            return redirect()->intended(route('admin.dashboard'));
        }

        //Login failed
        return redirect()->back()->withInput($request->only(['email', 'remember']))->withErrors(['email'=>['These credentials do not match our records.']]);
    }

    /**
     * Log the admin out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect('/admin');
    }
}
