<?php

namespace App\Http\Controllers;

use App\Card;
use App\Inventory;
use Illuminate\Http\Request;
use App\Filters\PackExist;
use App\Services\FilterValidator;
use App\Pack;
use App\User;
use App\Repositories\InventoryRepository;
use Illuminate\Support\Facades\Auth;

class OpenController extends Controller
{
    private $filterValidator;
    private $inventoryRepository;
    private $cards;

    public function __construct(InventoryRepository $inventoryRepository)
    {
        $this->inventoryRepository = $inventoryRepository;
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $packExist = new PackExist($request, 1);
        $this->filterValidator = new FilterValidator([
            $packExist
        ]);
        if (!$this->filterValidator->fails()) {
            if ($this->openPack($packExist->getPack(), $request->user())) {
                $this->cards = $this->cards->toArray();
                return view('open', [
                    'pack' => $packExist->getPack(),
                    'cards' => $this->cards,
                    'amount' => count($this->cards)
                ]);
            } else {
                return redirect()->back()->withErrors([
                    'error' => 'You tried to open a pack that you do not own in your inventory'
                ]);
            }
        } else {
            return redirect()->back()->withErrors([
                'error' => $this->filterValidator->error()
            ]);
        }

    }

    private function openPack(Pack $pack, User $user)
    {
        $val = $this->inventoryRepository->getInventoryPacks($user);
        if (isset($val) && !empty($val)) {
            if ($val->contains('id', $pack->id)) {
                $this->insertCardsInInventory($pack, $user);
                $pivot = $user->unopened_packs()->wherePivot('pack_id', '=', $pack->id)->wherePivot('user_id', '=', $user->id)->first()->pivot;
                $pivot->amount -= 1;
                $val = $pivot->amount;
                if ($val == 0) {
                    $user->unopened_packs()->detach($pack->id);
                }
                $pivot->save();
                $user->save();
                return true;
            }
        }
        return false;
    }

    private function insertCardsInInventory(Pack $pack, User $user)
    {
        $this->cards = $this->generateCards($pack->cards()->get(), $pack);
        foreach ($this->cards as $card) {
            if ($user->inventory()->where([
                ['card_id', '=', $card->id],
                ['user_id', '=', $user->id]
            ])->exists()) {
                $user->inventory()->where(([
                    ['card_id', '=', $card->id],
                    ['user_id', '=', $user->id]
                ]))->increment('amount');
                Log::info("OPENED: The following User: " . Auth::user()->id . " has now has the following card in his inventory:" . $card->id );


            } else {
                Inventory::create([
                    'card_id' => $card->id,
                    'user_id' => $user->id,
                    'amount' => 1
                ]);
                Log::info("OPENED: The following User: " . Auth::user()->id . " has now has the following card in his inventory:" . $card->id );
            }
        }
    }

    private function generateCards($cards, Pack $pack)
    {
        $idCards = [];
        $weights = [];
        $cardsArray = new \Illuminate\Database\Eloquent\Collection(new Card());
        for ($i = 0; $i < $pack->guaranteed_cards; $i++) {
            foreach ($cards as $card) {
                $idCards[] = $card->id;
                $weights[] = ($card->pivot->probability * 100) * 10000;
            }
            $id = $this->weighted_random($idCards, $weights, $pack->guaranteed_cards);
            $cardsArray->push(Card::find($id));
            $key = $cards->search(function($item) use ($id) {
                return $item->id == $id;
            });
            $cards->forget($key);
            $cards =$cards->values();
        }

        return $cardsArray->sortByDesc('overall');

    }

    private function binarySearchArray($weights)
    {
        $lookup = array();
        $total_weight = 0;

        for ($i = 0; $i < count($weights); $i++) {
            $total_weight += $weights[$i];
            $lookup[$i] = $total_weight;
        }
        return array($lookup, $total_weight);
    }

    private function weighted_random($values, $weights, $guarantedNumberOfCards, $lookup = null, $total_weight = null)
    {
        if ($lookup == null) {
            list($lookup, $total_weight) = $this->binarySearchArray($weights);
        }
        $r = mt_rand(0, $total_weight);
        $id = $values[$this->binary_search($r, $lookup)];

        return $id;


    }

    private function binary_search($needle, $haystack)
    {
        $high = count($haystack) - 1;
        $low = 0;

        while ($low < $high) {
            $probe = (int)(($high + $low) / 2);
            if ($haystack[$probe] < $needle) {
                $low = $probe + 1;
            } else if ($haystack[$probe] > $needle) {
                $high = $probe - 1;
            } else {
                return $probe;
            }
        }

        if ($low != $high) {
            return $probe;
        } else {
            if ($haystack[$low] >= $needle) {
                return $low;
            } else {
                return $low + 1;
            }
        }
    }

}
