<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\SearchService;

class CardSearchController extends Controller
{
    private $searchEngine;
    public function __construct(SearchService $searchEngine)
    {
        $this->middleware('auth');
        $this->searchEngine= $searchEngine;
    }

    public  function search(Request $request){
        return $this->searchEngine->cardSearch($request);
    }

}
