<?php

namespace App\Http\Controllers;


use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\InventoryRepository;

class UserController extends Controller
{

    private $inventoryRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(InventoryRepository $inventoryRepository)
    {
        $this->middleware('auth');
        $this->inventoryRepository = $inventoryRepository;
    }

    /**
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function profile(Request $request)
    {
        return view('profile');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function cards(Request $request)
    {
        $cards = $this->inventoryRepository->getInventoryCards($request->user());
        if (isset($cards)) {
            return view('home', [
                'cards' => $cards->toArray(),
            ]);
        } else {
            return view('home', [
                'cards' => $this->inventoryRepository->getInventoryCards($request->user()),
            ]);
        }
    }

    public function packs(Request $request)
    {
        return view('home', [
            'packs' => $this->inventoryRepository->getInventoryPacks($request->user()),
        ]);
    }

    public function transactions(Request $request)
    {
        return view('transactions',[
            'transactions' => $request->user()->transactions()->orderBy('created_at','desc')->get()
        ]);
    }

    public function withdraws(Request $request)
    {
        return view('withdraws',[
            'withdraws' => $request->user()->withdraws()->orderBy('created_at','desc')->get()
        ]);
    }

    public function updateInfo(Request $request)
    {
        /*
         * Check if the user has passed the proper params
         */
        $firstName = $request->post('First_name');
        $lastName = $request->post('Last_name');
        $preferredConsole = $request->post('Preferred_Console');
        $phone = $request->post('Phone_Number');
        if ((isset($firstName) && !empty($firstName)) && (isset($lastName) && !empty($lastName))
            && (isset($preferredConsole) && !empty($preferredConsole) && isset($phone) && !empty($phone))) {
            if (!is_numeric($firstName) && !is_numeric($lastName) && !is_numeric($preferredConsole) && $this->checkPhone($phone)) {
                if ((strcmp($preferredConsole, 'xbox') == 0) || (strcmp($preferredConsole, 'ps4') == 0)) {
                    try {
                        User::where('id', $request->user()->id)
                            ->update(
                                [
                                    'firstname' => $firstName,
                                    'lastname' => $lastName,
                                    'console' => $preferredConsole,
                                    'phone' => $phone
                                ]
                            );
                        return redirect()->back()->with([
                            'success' => 'Your information has been updated'
                        ]);
                    } catch (\Exception $exception) {
                        return redirect()->back()->withErrors([
                            'errors' => 'An error has occurred when updating your information. Try Again.'
                        ]);
                    }
                }
            }

        }
        return redirect()->back()->withErrors([
            'errors' => 'An error has occurred when updating your information. Try Again.'
        ]);
    }

    public function lastTransactions(){
        $totalAmount = Auth::user()->lastTransactions();

        return $totalAmount;
    }

    private function checkPhone($phone){
        $pattern='/(\+1)\s\([0-9]{3}\)\s[0-9]{3}\-[0-9]{4}/';
        if(preg_match($pattern, $phone))
            return true;
        else
            return false;
    }
}
