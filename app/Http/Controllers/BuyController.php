<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Filters\EnoughCoinsToBuy;
use App\Filters\PackExist;
use App\Services\FilterValidator;
use App\Pack;
use App\User;
use Exception;
use Illuminate\Support\Facades\Auth;

class BuyController extends Controller
{
    private $filterValidator;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function buy(Request $request)
    {
        $packExist = new PackExist($request, 2);
        $this->filterValidator = new FilterValidator([
            $packExist,
            new EnoughCoinsToBuy($request->user(), $packExist->getPack())
        ]);
        if (!$this->filterValidator->fails()) {
                $this->storePack($packExist->getPack(), $request->user());
                return redirect()->route('packs.marketplace')->with([
                    'success' => 'Your pack ' . $packExist->getPack()->name . ' has been added to <a   style="color:black !important"  href="' . route('user.packs') . '">My Packs</a>'
                ]);
            }
        return redirect()->back()->withErrors([
            'error' => $this->filterValidator->error()
        ]);
    }

    private function storePack(Pack $pack, User $user)
    {
        try {
            $packs = $user->unopened_packs()->where([
                ['pack_id', '=', $pack->id],
                ['user_id', '=', $user->id]
            ])->exists();
            if ($packs) {
                $pivot = $user->unopened_packs()->where([
                    ['pack_id', '=', $pack->id],
                    ['user_id', '=', $user->id]
                ])->first()->pivot;
                $pivot->amount += 1;
                $pivot->save();
                if(Auth::user()->console == 'ps4') {
                    $user->coins -= $pack->ps4_price;
                    Log::info("PACK OPENED: The following User: " . $user->id . " has opened the following the number of coins :" . $user->coins );
                }
                else{
                    $user->coins -= $pack->xbox_price;
                    Log::info("PACK OPENED: The following User: " . $user->id . " has opened the following the number of coins :" . $user->coins );
                }
                $user->save();
            } else {
                $packs = $user->unopened_packs()->attach($user->id, ['pack_id' => $pack->id]);
                if(Auth::user()->console == 'ps4') {
                    $user->coins -= $pack->ps4_price;
                    Log::info("PACK OPENED: The following User: " . $user->id . " has opened the following the number of coins :" . $user->coins );
                }
                else{
                    $user->coins -= $pack->xbox_price;
                    Log::info("PACK OPENED: The following User: " . $user->id . " has opened the following the number of coins :" . $user->coins );
                }
                $user->save();
            }
            Log::info("PACK OPENED: The following User: " . $user->id . " has opened the following the pack :" . $pack->name );
        } catch (Exception $e) {
            Log::errror("PACK OPENED: The following User: " . $user->id . " tried to opened the following the pack :" . $pack->name . " but an error occurred");
            return redirect()->back()->withErrors([
                'error' => $this->filterValidator->error()
            ]);
        }
    }
}
