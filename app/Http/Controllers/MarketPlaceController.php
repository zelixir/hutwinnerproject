<?php

namespace App\Http\Controllers;

use App\Card;
use App\User;
use Illuminate\Http\Request;
use App\Pack;
use Illuminate\Routing\Redirector;
use Illuminate\Routing\Route;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Exception;

class MarketPlaceController extends Controller
{


    public function index()
    {
        if(Auth::check()) {
            return view('marketplace',
                [
                    'packsFeatured' => Pack::all(),
                    'packs' => Pack::where('author_id', '=',null)->paginate(12),
                    'console' => Auth::user()->console,
                    'type'=>'normal'
                ]
            );
        }else{
            return view('marketplace',
                [
                    'packsFeatured' => Pack::all()->where('author_id', '=',null),
                    'packs' => Pack::where('author_id', '=',null)->paginate(12),
                    'console' => 'none',
                    'type'=>'normal'
                ]
            );
        }
    }
    public function customPacks(){
        if(Auth::check()) {
            return view('marketplace',
                [
                    'packsFeatured' => Pack::all(),
                    'packs' => Pack::where('author_id', '!=',null)->paginate(12),
                    'console' => Auth::user()->console,
                    'type'=>'custom'
                ]
            );
        }else{
            return view('marketplace',
                [
                    'packsFeatured' => Pack::all(),
                    'packs' => Pack::where('author_id', '!=',null)->paginate(12),
                    'console' => 'ps4',
                    'type'=>'custom'
                ]
            );
        }
    }

    public function console(Request $request){
        if(isset($request) && !empty($request)) {
            if ($request->console == 'ps4')
                $console = 'ps4';
            elseif ($request->console == 'xbox')
                $console = 'xbox';
            else
                $console = Auth::user()->console;
        }

        return view('marketplace',
            [
                'packsFeatured' => Pack::all(),
                'packs' => Pack::paginate(12),
                'console' => $console
            ]
        );
    }

    public function display($id)
    {
        try {
            if (empty(Pack::find($id)->author_id)) {
                $authorString = 'HUTWinner';
            } else {
                $user = Pack::find($id)->author_id;
                $name = User::select('firstname', 'lastname')->where('id', $user)->first();
                $authorString = $name->username;
            }
        } catch (Exception $e) {

            abort(404);
        }

        return view('packDisplay', [
            'pack' => Pack::find($id),
            'packImage' => '/img/hutbd-pack-icons/1.jpg.pagespeed.ce.0j6QTE0rlE.jpg',
            'author' => $authorString,
            'cards' => Pack::find($id)->cards()->orderBy('overall', 'desc')->get()->toArray(),
            'numberOfCards' => count(Pack::find($id)->cards()->orderBy('overall', 'desc')->get())
        ]);

    }


    public function purchase(Request $request)
    {
        if (Auth::check()) {
            try {
                $pack = Pack::find($request->post('pack'));
                $routeValue = strrpos(URL::previous(), '/');
                $packId = substr(URL::previous(), $routeValue + 1);
                if (isset($pack) && !empty($pack) && ($pack->id == $packId)) {
                    return redirect()->route('packs.marketplace')->with([
                        'success' => 'Your pack has been added to <a href="' . route('user.packs') . '">My Packs</a> : ' . $pack->name
                    ]);
                }
            } catch (Exception $e) {
                return redirect()->back()->withErrors([
                    'error' => 'An error has occur during your purchase please try again!'
                ]);
            }
        }
        return redirect()->back()->withErrors([
            'error' => 'An error has occur during your purchase please try again!']);
    }
}
