<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\SearchService;

class PackSearchController extends Controller
{
    private $searchEngine;

    public function __construct(SearchService $searchEngine)
    {
        $this->searchEngine= $searchEngine;
    }
    public function search(Request $request){
        return $this->searchEngine->packSearch($request);
    }
}
