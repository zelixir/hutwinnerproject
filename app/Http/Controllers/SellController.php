<?php

namespace App\Http\Controllers;

use App\Card;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Exception;

class SellController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function sell(Request $request)
    {
        if ($this->validateInput($request, 1))
            return redirect()->back()->with(
                [
                    'points'=> Auth::user()->coins,
                    'success' => 'Your card(s) has been sold!'
                ]
            );
        else
            return redirect()->back()->withErrors([
                'error' => 'An error has occurred while selling your card(s)'
            ]);
    }

    public function quickSell(Request $request)
    {
        if ($this->validateInput($request, 0))
            return response()->json(['points'=> Auth::user()->coins,'result' => 'Your card(s) has been sold!']);

    }

    private function validateInput(Request $request, $type)
    {
        if ($this->validateID($request->post('card'), $request->user())) {
            $this->decreaseAmount($request->post('card'), $request->user(), $type);
            return true;

        }
        return false;
    }

    private function validateID($cardID, User $user)
    {
        if (isset($cardID) && !empty($cardID) && is_numeric($cardID)) {
            try {
                if ($user->inventory()->where([
                    ['card_id', '=', $cardID],
                    ['user_id', '=', $user->id]
                ])->exists()) {
                    return true;
                }
            } catch (Exception $e) {
                Log::error("SELL: The following User: " . Auth::user()->id . " does not have the following card in his inventory: " .  $cardID);
                return false;
            }
        }
        return false;
    }

    private function decreaseAmount($cardID, User $user, $type)
    {
        try {
            $inventory = $user->inventory()->where([
                ['card_id', '=', $cardID],
                ['user_id', '=', $user->id]
            ])->first();
        } catch (Exception $e) {
            abort(404);
        }
        $inventory->amount -= 1;
        $inventory->save();

        if ($inventory->amount == 0) {
            $inventory->delete();
        }
        if ($type == 1) {
            /**
             * This is used by normal sell --> added this if the client ever wishes to change the value of the normal sell.
             */
            if ($user->console == 'ps4') {
                $user->coins += floor(((Card::find($cardID)->ps4_price) / Config::get('coins.ps4_price')) * 0.9);
                Log::info("SELL: The following User: " . Auth::user()->id . " has now sold the following card:" . $cardID . ". He has now " . $user->coins . " coins" );
            } else {
                $user->coins += floor(((Card::find($cardID)->xbox_price) / Config::get('coins.xbox_price')) * 0.9);
                Log::info("SELL: The following User: " . Auth::user()->id . " has now sold the following card:" . $cardID . ". He has now " . $user->coins . " coins" );
            }
            $user->save();
        } else if ($type == 0) {
            /**
             * This is used by quick sell --> added this if the client ever wishes to change the value of the quick sell.
             */
            if ($user->console == 'ps4') {
                $user->coins += floor(((Card::find($cardID)->ps4_price) / Config::get('coins.ps4_price')) * 0.9);
                Log::info("SELL: The following User: " . Auth::user()->id . " has now sold the following card:" . $cardID . ". He has now " . $user->coins . " coins" );

            } else {
                $user->coins += floor(((Card::find($cardID)->xbox_price) / Config::get('coins.xbox_price')) * 0.9);
                Log::info("SELL: The following User: " . Auth::user()->id . " has now sold the following card:" . $cardID . ". He has now " . $user->coins . " coins" );
            }
            $user->save();
        }

    }
}
