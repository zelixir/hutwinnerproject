<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactMessageController extends Controller
{
    public function store(Request $request){
        $this->validate($request, [
            'name' => 'required|alpha',
            'email' =>'required|email',
            'message' => 'required',
        ]);

        $data = array(
            'name' => $request->name,
            'email' => $request->email,
            'messageBody' => $request->message
        );

        Mail::send('emails.contact', $data, function ($message) use ($data){
           $message->from($data['email']);
           $message->to('example@gmail.com');
           $message->subject('HUT Contact');
        });

        return response()->json(['result' => 'Your Message Has Been Sent! Thank You For Contacting Us!']);
    }
}
