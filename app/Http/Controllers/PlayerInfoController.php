<?php

namespace App\Http\Controllers;

use App\Card;
use App\CardType;
use Illuminate\Http\Request;
use App\Team;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class PlayerInfoController extends Controller
{
    public function getInfoPlayers(Request $request){
        $responses = [];

        if($request->manageCards){
            foreach($request->ids as $id){
                $responses[] = $this->getInfoPlayerAdmin($id);
            }
        }
        else{
            foreach($request->ids as $id){
                $responses[] = $this->getInfoPlayer($id);
            }
        }
        return $responses;
    }

    private function getInfoPlayerAdmin($id){
        $player = Card::find($id);

        $names = $this->getFirstAndLastName($player->player_name);
        $fname = $names[0];
        $lname = $names[1];

        $card_type = CardType::find($player->type_id)->type;

        $playerPriceXbox = $this->getCardPrice($id, 'xbox');
        $playerPricePs4 = $this->getCardPrice($id, 'ps4');

        $team = $player->team;
        $league = $player->league;
        $overall = $player->overall;

        $teamLogo = 'url(/img/logos/' . strtolower($league) . '/' . strtolower($team) . '.png)';
        $team_name = Team::find($team)->team_name;
        $team_color = Team::find($team)->color;

        if($card_type == 'DIAM'){
            return response()->json([
                'img' => '/img/cards/19/diam/' . $this->getFullNameForUrl($fname, $lname) . '.png',
                'card_type' => $card_type,
                'playerid' => $id,
                'first_name' => $fname,
                'last_name' => $lname,
                'xbox_price' => $playerPriceXbox,
                'ps4_price' => $playerPricePs4,
                'league' => $league,
                'team_abbr' => $team,
                'team_name' => strtoupper($team_name),
                'overall' => $overall,
                'position' => $player->position
            ]);
        }

        if($card_type == 'MSP'){
            return response()->json([
                'img' => '/img/cards/19/msp/' . $this->getFullNameForUrl($fname, $lname) . '.png',
                'card_type' => $card_type,
                'playerid' => $id,
                'first_name' => $fname,
                'last_name' => $lname,
                'xbox_price' => $playerPriceXbox,
                'ps4_price' => $playerPricePs4,
                'league' => $league,
                'team_abbr' => $team,
                'team_name' => strtoupper($team_name),
                'overall' => $overall,
                'position' => $player->position
            ]);
        }

        if($card_type == 'LGD'){
            if($player->overall > 85){
                return response()->json([
                    'img' => '/img/cards/19/legends/' . $this->getFullNameForUrl($fname, $lname) . '_lgd.png',
                    'card_type' => $card_type,
                    'playerid' => $id,
                    'first_name' => $fname,
                    'last_name' => $lname,
                    'xbox_price' => $playerPriceXbox,
                    'ps4_price' => $playerPricePs4,
                    'league' => $league,
                    'team_abbr' => $team,
                    'team_name' => strtoupper($team_name),
                    'overall' => $overall,
                    'position' => $player->position
                ]);
            }
            else{
                return response()->json([
                    'img' => '/img/cards/19/legends/' . $this->getFullNameForUrl($fname, $lname) . '_lgd_blue.png',
                    'card_type' => $card_type,
                    'playerid' => $id,
                    'first_name' => $fname,
                    'last_name' => $lname,
                    'xbox_price' => $playerPriceXbox,
                    'ps4_price' => $playerPricePs4,
                    'league' => $league,
                    'team_abbr' => $team,
                    'team_name' => strtoupper($team_name),
                    'overall' => $overall,
                    'position' => $player->position
                ]);
            }
        }

        if($card_type == 'ALM' || $card_type == 'TOTW' || $card_type == 'MS' || $card_type == 'sGold')
            $teamLogo = 'url(/img/cards/19/gold_logos/' . strtolower($league) . '/' . strtolower($team) . '.png)';

        if($card_type == 'MS'){
            return response()->json([
                'first_name' => $fname,
                'last_name' => $lname,
                'headshot' => $this->getHeadshotUrl($fname, $lname, $overall),
                'position' => $player->position,
                'overall' => $overall,
                'synergy_symbols'=> $this->getSynergyArray($player->synergy),
                'team_name' => strtoupper($team_name),
                'team_backdrop' => 'url(/img/cards/19/backdrop/' . strtolower($league) . '/' . strtolower($team) . '.png)',
                'top_bg' => 'url(/img/cards/19/xmas/top/' . strtolower($team) . '.png)',
                'bottom_bg' => 'url(/img/cards/19/xmas/bottom/' . strtolower($team) . '.png)',
                'team_logo' => $teamLogo,
                'flair' => 'url(/img/cards/19/ms/flair/' . strtolower($team) . '.png)',
                'team_color' => $team_color,
                'card_type' => $card_type,
                'playerid' => $id,
                'xbox_price' => $playerPriceXbox,
                'ps4_price' => $playerPricePs4,
                'league' => $league,
                'team_abbr' => $team
            ]);
        }
        else if($card_type == 'HC'){
            return response()->json([
                'first_name' => $fname,
                'last_name' => $lname,
                'headshot' => $this->getHeadshotUrl($fname, $lname, $overall),
                'position' => $player->position,
                'overall' => $overall,
                'synergy_symbols'=> $this->getSynergyArray($player->synergy),
                'team_name' => strtoupper($team_name),
                'team_backdrop' => 'url(/img/cards/19/backdrop/' . strtolower($league) . '/' . strtolower($team) . '.png)',
                'top_bg' => 'url(/img/cards/19/xmas/top/' . strtolower($team) . '.png)',
                'bottom_bg' => 'url(/img/cards/19/xmas/bottom/' . strtolower($team) . '.png)',
                'team_logo' => $teamLogo,
                'bgImage' => 'url(/img/cards/19/hc/' . strtolower($team) .'.png)',
                'team_color' => $team_color,
                'card_type' => $card_type,
                'playerid' => $id,
                'xbox_price' => $playerPriceXbox,
                'ps4_price' => $playerPricePs4,
                'league' => $league,
                'team_abbr' => $team
            ]);
        }

        return response()->json([
            'first_name' => $fname,
            'last_name' => $lname,
            'headshot' => $this->getHeadshotUrl($fname, $lname, $overall),
            'position' => $player->position,
            'overall' => $overall,
            'synergy_symbols'=> $this->getSynergyArray($player->synergy),
            'team_name' => strtoupper($team_name),
            'team_backdrop' => 'url(/img/cards/19/backdrop/' . strtolower($league) . '/' . strtolower($team) . '.png)',
            'top_bg' => 'url(/img/cards/19/xmas/top/' . strtolower($team) . '.png)',
            'bottom_bg' => 'url(/img/cards/19/xmas/bottom/' . strtolower($team) . '.png)',
            'team_logo' => $teamLogo,
            'team_color' => $team_color,
            'card_type' => $card_type,
            'playerid' => $id,
            'xbox_price' => $playerPriceXbox,
            'ps4_price' => $playerPricePs4,
            'league' => $league,
            'team_abbr' => $team
        ]);
    }

    private function getInfoPlayer($id){
        $player = Card::find($id);
        
        $names = $this->getFirstAndLastName($player->player_name);
        $fname = $names[0];
        $lname = $names[1];

        //$card_type = $player->type_id;
        $card_type = CardType::find($player->type_id)->type;

        $playerPrice = $this->getCardPrice($id, false);

        if($card_type == 'DIAM'){
            return response()->json([
                'img' => '/img/cards/19/diam/' . $this->getFullNameForUrl($fname, $lname) . '.png',
                'card_type' => $card_type,
                'playerid' => $id,
                'first_name' => $fname,
                'last_name' => $lname,
                'price' => $playerPrice
            ]);
        }

        if($card_type == 'MSP'){
            return response()->json([
                'img' => '/img/cards/19/msp/' . $this->getFullNameForUrl($fname, $lname) . '.png',
                'card_type' => $card_type,
                'playerid' => $id,
                'first_name' => $fname,
                'last_name' => $lname,
                'price' => $playerPrice
            ]);
        }

        if($card_type == 'LGD'){
            if($player->overall > 85){
                return response()->json([
                    'img' => '/img/cards/19/legends/' . $this->getFullNameForUrl($fname, $lname) . '_lgd.png',
                    'card_type' => $card_type,
                    'playerid' => $id,
                    'first_name' => $fname,
                    'last_name' => $lname,
                    'price' => $playerPrice
                ]);
            }
            else{
                return response()->json([
                    'img' => '/img/cards/19/legends/' . $this->getFullNameForUrl($fname, $lname) . '_lgd_blue.png',
                    'card_type' => $card_type,
                    'playerid' => $id,
                    'first_name' => $fname,
                    'last_name' => $lname,
                    'price' => $playerPrice
                ]);
            }
        }

        $team = $player->team;
        $league = $player->league;
        $overall = $player->overall;
        $teamLogo = 'url(/img/logos/' . strtolower($league) . '/' . strtolower($team) . '.png)';
        $team_name = Team::find($team)->team_name;
        $team_color = Team::find($team)->color;

        if($card_type == 'ALM' || $card_type == 'TOTW' || $card_type == 'MS' || $card_type == 'sGold')
            $teamLogo = 'url(/img/cards/19/gold_logos/' . strtolower($league) . '/' . strtolower($team) . '.png)';

        if($card_type == 'MS'){
            return response()->json([
                'first_name' => $fname,
                'last_name' => $lname,
                'headshot' => $this->getHeadshotUrl($fname, $lname, $overall),
                'position' => $player->position,
                'overall' => $overall,
                'synergy_symbols'=> $this->getSynergyArray($player->synergy),
                'team_name' => strtoupper($team_name),
                'team_backdrop' => 'url(/img/cards/19/backdrop/' . strtolower($league) . '/' . strtolower($team) . '.png)',
                'top_bg' => 'url(/img/cards/19/xmas/top/' . strtolower($team) . '.png)',
                'bottom_bg' => 'url(/img/cards/19/xmas/bottom/' . strtolower($team) . '.png)',
                'team_logo' => $teamLogo,
                'flair' => 'url(/img/cards/19/ms/flair/' . strtolower($team) . '.png)',
                'team_color' => $team_color,
                'card_type' => $card_type,
                'playerid' => $id,
                'price' => $playerPrice
            ]);
        }
        else if($card_type == 'HC'){
            return response()->json([
                'first_name' => $fname,
                'last_name' => $lname,
                'headshot' => $this->getHeadshotUrl($fname, $lname, $overall),
                'position' => $player->position,
                'overall' => $overall,
                'synergy_symbols'=> $this->getSynergyArray($player->synergy),
                'team_name' => strtoupper($team_name),
                'team_backdrop' => 'url(/img/cards/19/backdrop/' . strtolower($league) . '/' . strtolower($team) . '.png)',
                'top_bg' => 'url(/img/cards/19/xmas/top/' . strtolower($team) . '.png)',
                'bottom_bg' => 'url(/img/cards/19/xmas/bottom/' . strtolower($team) . '.png)',
                'team_logo' => $teamLogo,
                'bgImage' => 'url(/img/cards/19/hc/' . strtolower($team) .'.png)',
                'team_color' => $team_color,
                'card_type' => $card_type,
                'playerid' => $id,
                'price' => $playerPrice
            ]);
        }

        return response()->json([
            'first_name' => $fname,
            'last_name' => $lname,
            'headshot' => $this->getHeadshotUrl($fname, $lname, $overall),
            'position' => $player->position,
            'overall' => $overall,
            'synergy_symbols'=> $this->getSynergyArray($player->synergy),
            'team_name' => strtoupper($team_name),
            'team_backdrop' => 'url(/img/cards/19/backdrop/' . strtolower($league) . '/' . strtolower($team) . '.png)',
            'top_bg' => 'url(/img/cards/19/xmas/top/' . strtolower($team) . '.png)',
            'bottom_bg' => 'url(/img/cards/19/xmas/bottom/' . strtolower($team) . '.png)',
            'team_logo' => $teamLogo,
            'team_color' => $team_color,
            'card_type' => $card_type,
            'playerid' => $id,
            'price' => $playerPrice
        ]);
    }

    private function getSynergyArray($synergy){
        $stripped = str_replace(' ', '', $synergy);
        $num = substr_count($stripped, ')');
        $synergySymbols = array();
        $startIndex = 0;
        for($i = 0; $i < $num; $i++){
            if(ctype_alpha($stripped[$startIndex+1])){
                $synergySymbols[] = [
                    substr($stripped, $startIndex, 2),
                    substr($stripped, $startIndex + 3, 1)
                ];
                $startIndex += 5;
            }
            else{
                $synergySymbols[] = [
                    substr($stripped, $startIndex, 1),
                    substr($stripped, $startIndex + 2, 1)
                ];
                $startIndex += 4;
            }

        }
        return $synergySymbols;
    }

    private function getFirstAndLastName($fullname){
        $names = explode(" ", $fullname);
        $last_name = $names[count($names) - 1];
        $first_name = '';
        for($i = 0; $i < count($names) -1; $i++){
            $first_name .= $names[$i] . ' ';
        }
        $first_name = trim($first_name);

        return [$first_name, $last_name];
    }

    private function getHeadshotUrl($fname, $lname, $overall){
        if($overall < 80){
            return 'url(/img/avatar.png)';
        }

        $url = "url(/img/headshots/active/" . $this->getFullNameForUrl($fname, $lname) . ".png)";
        return $url;
    }

    private function getFullNameForUrl($fname, $lname){
        $fname = strtolower($fname);
        $lname = strtolower($lname);
        $fullnameForUrl = "";

        if(ctype_alpha($fname) && ctype_alpha($lname)){
            $fullnameForUrl .= $fname . "_" . $lname;
        }
        else{
            for($i = 0; $i < strlen($fname); $i++){
                if(ctype_alpha($fname[$i]))
                    $fullnameForUrl .= $fname[$i];
            }
            $fullnameForUrl .= "_";

            if(ctype_alpha($lname)){
                $fullnameForUrl .= $lname;
            }
            else{
                for($i = 0; $i < strlen($lname); $i++){
                    if($lname[$i] != "'")
                        $fullnameForUrl .= $lname[$i];
                    else
                        $fullnameForUrl .= "\'";
                }
            }
        }

        return $fullnameForUrl;
    }

    private function getCardPrice($id, $console){
        if($console){
            if($console === 'xbox'){
                return Card::find($id)->xbox_price;
            }
            else{
                return Card::find($id)->ps4_price;
            }
        }
        else{
            if(Auth::check()){
                if (Auth::user()->console == 'ps4') {
                    return floor(((Card::find($id)->ps4_price) / Config::get('coins.ps4_price')) * 0.9);
                } else {
                    return floor(((Card::find($id)->xbox_price) / Config::get('coins.xbox_price')) * 0.9);
                }
            } else{
                return 0;
            }
        }
    }

}