<?php

namespace App\Http\Controllers;

use App\Card_pack;
use App\Pack;
use App\User;
use App\PackCover;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Card;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class CreatePackController extends Controller
{
    private $packID;

    public function __construct()
    {
        $this->middleware('auth');
    }


    function pack(Request $request)
    {
        $packNum = $request->user()->packs_created;
        $lastPack = $request->user()->pack_first;
        $last24ours = strtotime("-24 hours");

        if ($lastPack != null) {
            //more than 24 hours
            if ($lastPack >= $last24ours) {
                User::where('id', $request->user()->id)
                    ->update(
                        [
                            'packs_created' => null,
                            'pack_first' => null,
                        ]
                    );
                return view('customPack');
            }

            //less than 24 hours and has 2 packs
            if ($lastPack <= $last24ours && $packNum == 2)
                return view('customPack');

            if ($lastPack <= $last24ours && $packNum < 2)
                return view('customPack');
        } else {
            return view('customPack');
        }
    }

    /**
     * This checks the pack size selected and if the pack size is good it will return 'chooseCards' view with the pack size
     *
     * @param $num
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function packSize($num, Request $request)
    {
        $packNum = $request->user()->packs_created;
        $lastPack = strtotime($request->user()->pack_first);
        $last24ours = strtotime("-24 hours");

        if ($lastPack != null) {
            //more than 24 hours
            if ($lastPack <= $last24ours) {
                User::where('id', $request->user()->id)
                    ->update(
                        [
                            'packs_created' => null,
                            'pack_first' => null,
                        ]
                    );
                if (isset($num) && !empty($num) && is_numeric($num))
                    if ($this->checkPackSize($num)) {
                        return view('chooseCards', [
                            'packSize' => $num
                        ]);
                    }
                abort(404);
            }

            //less than 24 hours and has 2 packs
            if ($lastPack >= $last24ours && $packNum == 2)
                return redirect('/packs/create')->withErrors(
                    [
                        'errors' => 'You have reached your daily limit of creating packs',
                    ]
                );

            if ($lastPack >= $last24ours && $packNum < 2)
                if (isset($num) && !empty($num) && is_numeric($num))
                    if ($this->checkPackSize($num)) {
                        return view('chooseCards', [
                            'packSize' => $num
                        ]);
                    }
            abort(404);
        } else {
            if (isset($num) && !empty($num) && is_numeric($num))
                if ($this->checkPackSize($num)) {
                    return view('chooseCards', [
                        'packSize' => $num
                    ]);
                }
            abort(404);
        }

    }

    /**
     * This function checks if the right amount of cards are chosen and return 'cardProbability' view
     *
     * @param $num
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    function probabilityCards($num, Request $request)
    {
        $cards = $request->post('card');
        if (isset($num) && !empty($num) && is_numeric($num))
            if ($this->checkPackSize($num) && isset($cards) && !empty($cards) && (count($cards) == $num) && $this->checkIfContainsNumbers($cards) && $this->checkIfCardExists($cards)) {
                return view('cardProbability', [
                    'cards' => implode(',', $cards),
                    'packSize' => $num
                ]);
            }
        return redirect()->route('pack')->withErrors([
            'errors' => 'Error while adding cards'
        ]);
    }

    /**
     * This function checks if the user has chosen a cover and returns 'packOverview' view
     *
     * @param $num
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function packCover($num, Request $request)
    {
        return view('packCover');
    }

    /**
     * This function checks if the probabilities are chosen and the right amount of cards are passed.
     *
     * @param $num
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    function overviewPack($num, Request $request)
    {
        $cards = $request->post('cards');
        $probabilities = $request->post('probabilities');
        if (isset($num) && !empty($num) && is_numeric($num)) {
            if ($this->checkPackSize($num) && isset($cards) && !empty($cards) && isset($probabilities)
                && !empty($probabilities) && $this->checkIfContainsNumbers($cards) && $this->checkIfContainsNumbers($probabilities)
                && (count($cards) == $num) && (count($probabilities) == $num) && (count($cards) == count($probabilities))
                && $this->checkIfCardExists($cards)) {
                if ($this->sumProbability($probabilities)) {
                    return view('packOverview', [
                        'cards' => implode(',', $cards),
                        'probabilities' => implode(',', $probabilities),
                        'packSize' => $num,
                        'price_Xbox' => $this->getXboxPackSize($cards, $probabilities),
                        'price_Ps4' => $this->getPS4PackPrice($cards, $probabilities)

                    ]);
                }
                else {
                    return redirect('packs/create/'. $num .'/probability')->with([
                        'cards' => $cards,
                        'packSize' => $num
                    ]);
                }
            }else{
                return redirect()->route('pack')->withErrors([
                    'errors' => 'Error while adding cards'
                ]);
            }
        }else{
            return redirect()->route('pack')->withErrors([
                'errors' => 'Error while adding cards'
            ]);
        }
    }
    public function wrongProbabilities($num, Request $request){
        $cards = Session::get('cards');
        if (isset($num) && !empty($num) && is_numeric($num))
            if ($this->checkPackSize($num) && isset($cards) && !empty($cards) && (count($cards) == $num) && $this->checkIfContainsNumbers($cards) && $this->checkIfCardExists($cards)) {
                return view('cardProbability', [
                    'cards' => implode(',', $cards),
                    'packSize' => $num
                ])->withErrors([
                    'errors' => 'The sum of all probabilities must be equal to 100% '
                ]);
            }
        return redirect()->route('pack')->withErrors([
            'errors' => 'Error while adding cards'
        ]);
    }

    /**
     * This function stores the created pack in the database
     *
     * @param $num
     * @param Request $request
     * @return string
     */
    function store($num, Request $request)
    {
        $cards = $request->post('cards');
        $probabilities = $request->post('probabilities');
        $name = $request->post('name');

        if (isset($cards) && isset($probabilities) && isset($name) && !empty($cards) && !empty($probabilities) && !empty($name)
            && isset($num) && !empty($num) && is_numeric($num) && (count($cards) == $num) && (count(explode(",", $probabilities)) == $num)
            && $this->checkPackSize($num) && $this->checkIfContainsNumbers($cards) && $this->checkIfContainsNumbers(explode(",", $probabilities))
            && (count($cards) == count(explode(",", $probabilities))) && $this->checkIfCardExists($cards)
            && $this->checkProbabilities(explode(",", $probabilities))
            && $this->hasEnoughCoins($request,$cards,explode(",", $probabilities))) {

            DB::transaction(function () use($name, $request, $cards, $probabilities){
                $this->insertPack($name, $request->user()->id, 'Pack_Cover.png', $cards, explode(",", $probabilities));
                $this->insertCardPack($cards, explode(",", $probabilities));
                $this->insertUserPackCreated($request);
            });

            return redirect()->route('pack')->with([
                'success' => 'Your pack has been added to <a href="' . route('user.packs') . '" style="color: black">My Packs</a>'
            ]);
        } else
            return redirect()->route('pack')->withErrors([
                'errors' => 'Error while creating your pack'
            ]);
    }

    /**
     * Check if the passed array has only numbers
     *
     * @param $array
     * @return bool
     */
    private function checkIfContainsNumbers($array)
    {
        foreach ($array as $value) {
            if (!is_numeric($value))
                return false;
        }
        return true;
    }

    /**
     * Check if the card exist in the database using the id of each card
     *
     * @param $array
     * @return bool
     */
    private function checkIfCardExists($array)
    {
        foreach ($array as $value) {
            try {
                if (!Card::where('id', $value)->exists()) {
                    return false;
                }
            } catch (Exception $e) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check if a pack cover with a given id exists
     *
     * @param $cover
     * @return bool
     */
    private function checkIfCoverExists($cover)
    {
        try {
            if (PackCover::where('id', $cover)->exists()) {
                $foundCover = PackCover::find($cover)->image;
                if (isset($foundCover) && !empty($foundCover))
                    return true;
            }
        } catch (Exception $e) {
            return false;
        }
        return false;
    }

    /**
     * Get the image with the given id
     *
     * @param $cover
     * @return mixed
     */
    private function getImageUrl($cover)
    {
        return PackCover::find($cover)->image;
    }

    /**
     * Insert the pack in the database
     *
     * @param $name
     * @param $id
     * @param $img
     */
    private function insertPack($name, $id, $img, $cardsArray, $probabilityArray)
    {
        $this->packID = Pack::create([
            'name' => $name,
            'ps4_price' => $this->getPS4PackPrice($cardsArray, $probabilityArray),
            'xbox_price' => $this->getXboxPackSize($cardsArray, $probabilityArray),
            'author_id' => $id,
            'image' => $img,
            'guaranteed_cards' => count($cardsArray) * 0.25
        ])->id;
        Log::info("PACK CREATED: The following User: " . Auth::user()->id . " has created the following pack :" .  $this->packID );
        $this->insertIntoPackUser(Pack::find($this->packID), Auth::user());
    }


    private function insertCardPack($cardArray, $probabilityArray)
    {
        for ($i = 0; $i < count($cardArray); $i++) {
            Card_pack::create([
                'pack_id' => $this->packID,
                'card_id' => $cardArray[$i],
                'probability' => $probabilityArray[$i] / 100
            ]);
            Log::info("PACK CREATED: The following User: " . Auth::user()->id . " has added the following card: " .  $cardArray[$i] . " with the ". $probabilityArray[$i]. "% "  .  " to his pack :" .  $this->packID );
        }
    }

    private function insertUserPackCreated(Request $request)
    {
        $packNum = $request->user()->packs_created;
        $user = User::find($request->user()->id);

        if ($packNum == null) {
            $user->packs_created = 1;
            $val = Carbon::now()->toDateTimeString();
            $user->pack_first = $val;
            $user->save();

        } else {
            $user->packs_created = 2;
            $user->save();
        }
    }

    private function checkPackSize($packSize)
    {
        if ($packSize == 4 || $packSize == 8 || $packSize == 12 || $packSize == 16 || $packSize == 20)
            return true;
        else
            return false;
    }

    private function checkProbabilities($probabilityArray)
    {
        for ($i = 0; $i < count($probabilityArray); $i++) {
            if ($probabilityArray[$i] < 1 || $probabilityArray[$i] > 25)
                return false;
        }
        return true;
    }

    private function getPS4PackPrice($cardsArray, $probabilityArray)
    {
        $total = 0;
        for ($i = 0; $i < count($cardsArray); $i++) {
            $total += (Card::find($cardsArray[$i])->ps4_price/\Illuminate\Support\Facades\Config::get('coins.ps4_price')) * ($probabilityArray[$i] / 100) * count($cardsArray) / 2;
        }
        return ceil($total/100)* 100;
    }

    private function getXboxPackSize($cardsArray, $probabilityArray)
    {
        $total = 0;
        for ($i = 0; $i < count($cardsArray); $i++) {
            $total += (Card::find($cardsArray[$i])->xbox_price)/\Illuminate\Support\Facades\Config::get('coins.xbox_price') * ($probabilityArray[$i] / 100) * count($cardsArray) / 2;
        }
        return  ceil($total/100) * 100;
    }

    private function insertIntoPackUser(Pack $pack, User $user)
    {
        if( $user->console == 'ps4'){
            $user->coins -= $pack->ps4_price;
            $user->save();
        }
        else{
            $user->coins -= $pack->xbox_price;
            $user->save();
        }
        $user->unopened_packs()->attach($user->id, ['pack_id' => $pack->id]);
    }

    private function hasEnoughCoins(Request $request, $cardArray, $probabilityArray)
    {
        $userCoins = $request->user()->coins;
        if ($request->user()->console == 'ps4') {
            $packValue = $this->getPS4PackPrice($cardArray, $probabilityArray);
            if ($packValue > $userCoins)
                return false;
        } else {
            $packValue = $this->getXboxPackSize($cardArray, $probabilityArray);
            if ($packValue > $userCoins)
                return false;
        }
        return true;
    }

    private function sumProbability($probabilities){
        $sum = 0;
        foreach ($probabilities as $value) {
            $sum += $value;
        }
        if($sum == 100)
            return true;
        else
            return false;
    }
}
