<?php

namespace App\Http\Controllers;

use App\Withdraw;
use Illuminate\Http\Request;
use App\Card;
use App\Repositories\InventoryRepository;
use App\Filters\ValidWithdrawRequest;
use App\Services\FilterValidator;

class WithdrawController extends Controller
{
    private $userCards;


    public function __construct()
    {
        //Use the 'admin' guard
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $inventory = new InventoryRepository();
        $this->userCards = $inventory->getInventoryCards($request->user());

        return view('withdraw', [
            'cards' => $this->userCards
        ]);
    }

    public function withdraw(Request $request)
    {

        $filter = new FilterValidator([
            new ValidWithdrawRequest($request, $request->user())
        ]);
        if (!$filter->fails()) {
                return redirect()->back()->with([
                    'success' => 'Your withdraw has been sent. Check your requests  <a   style="color:black !important"  href="' . route('user.withdraws') . '"> My Withdraws</a>'
                ]);
        } else {
            return redirect()->back()->withErrors(
                [
                    'error' => $filter->error()
                ]
            );
        }


    }

    private function insertWithdraw(Request $request)
    {
        $message = $this->buildMessage($request);
            $withdraw = Withdraw::create([
                'user_id' => $request->user()->id,
                'status_id' => 1,
                'value' => $message

            ]);
            foreach ($request->post('value') as $card) {
                $withdraw->cards()->save(Card::find($card));
            }

    }

    private function buildMessage(Request $request)
    {
        $playerName = $request->post('player_name');
        $teamName = $request->user()->hut_team;
        $startPrice = $request->post('start_price');
        $buyNowPrice = $request->post('buy_now');
        $transferDuration = '3 Days';

        return 'Player Name: ' . $playerName . PHP_EOL
            . 'HUT Team Name: ' . $teamName . PHP_EOL
            . 'Start Price: ' . $startPrice . PHP_EOL
            . 'Buy Now Price: ' . $buyNowPrice . PHP_EOL
            . 'Transfer Duration: ' . $transferDuration . PHP_EOL;

    }
}
