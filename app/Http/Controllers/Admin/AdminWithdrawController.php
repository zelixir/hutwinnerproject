<?php

namespace App\Http\Controllers\Admin;


use Gate;
use App\Note;
use App\Withdraw;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class AdminWithdrawController extends Controller
{
    private $userCards;


    public function __construct()
    {
        //Use the 'admin' guard
        $this->middleware('auth:admin');
    }

    /**
     * Show a list of withdraws
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getWithdraws()
    {
        //TODO: could maybe store the pagination number in a config file ??
        $withdraws = Withdraw::orderBy('created_at', 'desc')->paginate(10);
        return view('admin.withdraws')->with('withdraws', $withdraws);
    }

    public function getWithdrawById($id)
    {
        try {
            $withdraw = Withdraw::findOrFail($id);
            return view('admin.withdraw')->with('withdraw', $withdraw);
        } catch (ModelNotFoundException $exception) {
            session()->put('warning', 'Record not found.');
            return redirect()->route('admin.withdraws');
        }
    }

    public function handleWithdraw(Request $request)
    {
        if (!Gate::allows('admin-level2', Auth::user())) {
            return view('admin.adminhome');
        }

        //Validate information
        $this->validate($request, [
            'withdrawId' => 'required|integer',
            'action' => 'required'
        ]);

        try {
            $withdraw = Withdraw::findOrFail($request->withdrawId);
            $action_type = $request->action;

            if (array_key_exists($action_type, config('statuses.withdraw'))) {
                $withdraw->update([
                    'status_id' => config('statuses.withdraw.' . $action_type)
                ]);

                return redirect()->route('admin.withdraw', ['id' => $withdraw->id]);
            } else {
                session()->put('warning', 'Unable to perform action.');
                return redirect()->back();
            }
        } catch (ModelNotFoundException $exception) {
            session()->put('warning', 'Error processing the withdraw.');
            return redirect()->route('admin.withdraws');
        }
    }

    public function addNote(Request $request)
    {
        //Validate information
        $this->validate($request, [
            'withdrawId' => 'required|integer',
            'note' => 'required|string'
        ]);

        try {
            $adminId = Auth::guard('admin')->user()->id;
            Note::create([
                'withdraw_id' => $request->withdrawId,
                'admin_id' => $adminId,
                'note' => $request->note
            ]);

            return redirect()->route('admin.withdraw', ['id' => $request->withdrawId]);
        } catch (ModelNotFoundException $exception) {
            session()->put('warning', 'Unable to add note.');
            return redirect()->route('admin.withdraws');
        }
    }
}
