<?php

namespace App\Http\Controllers\Admin;


use Gate;
use App\User;
use App\Status;
use App\Note;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;


class AdminUserController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('auth:admin');
    }

    public function displayUsers()
    {
        if (!Gate::allows('admin-level1', Auth::user())) {
            return view('admin.adminhome');
        }

        $users = User::orderBy('firstname')->paginate(10);
        return view('admin.users', ['users' => $users]);
    }

    public function modifyUserStatus(Request $request)
    {
        if (!Gate::allows('admin-level1', Auth::user())) {
            return view('admin.adminhome');
        }

        //Validate information
        $this->validate($request, [
            'userId' => 'required|integer',
            'action' => 'required'
        ]);

        $msg = 'Successfully updated user status.';

        try {
            DB::beginTransaction();

            $user = User::findOrFail($request->userId);
            $action_type = $request->action;

            if (array_key_exists($action_type, config('statuses.user'))) {
                $status = config('statuses.user.' . $action_type);

                $user->update([
                    'status_id' => $status
                ]);

                $statusType = Status::where('id', $status)->get();

                DB::commit();

                return response()->json(['message'=> $msg, 'statustype' => $statusType[0]->type], 200);
            } else {
                $msg = 'Unable to perform action.';
            }
        } catch (ModelNotFoundException $exception) {
            $msg = 'Error processing the user.';
            DB::rollBack();
        }

        return response()->json(['message'=> $msg], 400);
    }

    public function getUserById($id)
    {
        try {
            $user = User::findOrFail($id);
            return view('admin.user')->with('user', $user);
        } catch (ModelNotFoundException $exception) {
            session()->put('warning', 'Record not found.');
            return redirect()->route('admin.users');
        }
    }

    public function addNote(Request $request)
    {
        //Validate information
        $this->validate($request, [
            'userId' => 'required|integer',
            'note' => 'required|string'
        ]);

        try {
            $adminId = Auth::guard('admin')->user()->id;
            Note::create([
                'user_id' => $request->userId,
                'admin_id' => $adminId,
                'note' => $request->note
            ]);

            return redirect()->route('admin.user', ['id' => $request->userId]);
        } catch (ModelNotFoundException $exception) {
            session()->put('warning', 'Unable to add note.');
            return redirect()->route('admin.users');
        }
    }
}
