<?php

namespace App\Http\Controllers\Admin;

use Gate;
use Exception;
use App\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //Use the 'admin' guard
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.adminhome');
    }

    public function adminForm()
    {
        if (!Gate::allows('admin-level1', Auth::user())) {
            return view('admin.adminhome');
        }

        return view('admin.adminform');
    }

    public function displayAdmins()
    {
        if (!Gate::allows('admin-level1', Auth::user())) {
            return view('admin.adminhome');
        }

        $admins = Admin::orderBy('firstname')->paginate(10);
        return view('admin.admins', ['admins' => $admins]);
    }

    public function createAdmin(Request $request)
    {
        if (!Gate::allows('admin-level1', Auth::user())) {
            return view('admin.adminhome');
        }

        //Validate information
        $this->validate($request, [
            'email' => 'required|email',
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'password' => 'required|string|min:8|confirmed', //TODO: maybe use regex
            'admintype' => 'required|integer'
        ]);

        try {
            Admin::create([
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'firstname' => $request->firstname,
                'lastname' => $request->lastname,
                'admintype' => $request->admintype
            ]);

            session()->put('success', 'Admin successfully created.');
        } catch (Exception $exception) {
            session()->put('warning', 'Unable to create admin.');
        }

        return redirect()->route('admin.form');
    }
}
