<?php

namespace App\Http\Controllers\Admin;

use Gate;
use Exception;
use App\Card;
use App\Pack;
use App\CardType;
use App\Card_pack;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Collection;
use App\Http\Controllers\Controller;

class CardPackController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //Use the 'admin' guard
        $this->middleware('auth:admin');
    }

    public function displayPacks()
    {
        if (!Gate::allows('admin-level1', Auth::user())) {
            return view('admin.adminhome');
        }

        $packs = Pack::orderBy('name', 'asc')->paginate(9);
        return view('admin.managepack')->with('packs', $packs);
    }

    public function singlePackView($id)
    {
        if (!Gate::allows('admin-level1', Auth::user())) {
            return view('admin.adminhome');
        }

        try {
            $pack = Pack::findOrFail($id);
            $cards = Card::orderBy('player_name', 'asc')->groupBy('id')->get();
            $card_pack = Card_pack::where('pack_id', $id)->get();
            return view('admin.singlepack', ['pack' => $pack, 'cards' => $cards, 'card_pack' => $card_pack]);
        } catch (ModelNotFoundException $exception) {
            session()->put('warning', 'Unable to find card.');
        }

        return redirect()->back();
    }

    public function packFind(Request $request)
    {
        if (!Gate::allows('admin-level1', Auth::user())) {
            return view('admin.adminhome');
        }

        //Validate information
        $this->validate($request, [
            'packname' => 'required|string',
            'xboxpackprice' => 'required|integer',
            'ps4packprice' => 'required|integer',
            'guaranteedcard' => 'required|integer'
        ]);

        if ($request->all) {
            $all = Card::orderBy('player_name')->get();
            return view('admin.packdata', ['players' => $all, 'packname' => $request->packname, 'xboxprice' => $request->xboxpackprice, 'ps4price' => $request->ps4packprice, 'guaranteedcard' => $request->guaranteedcard]);
        }

        $results = new Collection();
        $teams = empty($request->team) ? config('team') : $request->team ;
        $leagues = $request->league;
        foreach ($leagues as $team => $type) {
            $typeArr = [];

            foreach ($type as $typeId) {
                array_push($typeArr, intval($typeId));
            }

            $cards = Card::where('league', $team)->whereIn('type_id', $typeArr)->whereIn('team', $teams)->get();
            $results = $results->merge($cards);
        }

        $sorted = $results->sortBy(function ($card) {
            return $card->player_name;
        });

        return view('admin.packdata', ['players' => $sorted, 'packname' => $request->packname, 'xboxprice' => $request->xboxpackprice, 'ps4price' => $request->ps4packprice, 'guaranteedcard' => $request->guaranteedcard]);
    }

    public function createPack(Request $request)
    {
        if (!Gate::allows('admin-level1', Auth::user())) {
            return view('admin.adminhome');
        }

        //Validate information
        $validator = Validator::make($request->all(), [
            'packname' => ['required', 'string'],
            'xboxprice' => ['required', 'integer'],
            'ps4price' => ['required', 'integer'],
            'guaranteedcard' => ['required', 'integer'],
            'card' => [
                'required',
                'array',
                function ($attribute, $value, $fail) {
                    foreach ($value as $key => $val) {
                        if ($val > 100 || $val < 0)
                            $fail('Invalid probability.');
                    }
                }
            ]
        ]);

        if ($validator->fails()) {
            session()->put('error', $validator->messages()->first());
            return redirect()->back()->withInput();
        }

        try {
            DB::beginTransaction();

            $pack = Pack::create([
                'name' => $request->packname,
                'xbox_price' => 0,
                'ps4_price' =>0,
                'author_id' => null,
                'image' => 'logo.png',
                'guaranteed_cards' => $request->guaranteedcard
            ]);

            $pack->xbox_price = $this->getXboxPackPrice($pack->id);
            $pack->ps4_price = $this->getPS4PackPrice($pack->id);
            $pack->save();

            foreach ($request->card as $key => $value) {
                Card_pack::create([
                    'card_id' => $key,
                    'pack_id' => $pack->id,
                    'probability' => $value
                ]);
            }

            DB::commit();
            session()->put('success', 'Successfully created a pack.');
        } catch (Exception $exception) {
            DB::rollBack();
            session()->put('warning', 'Unable to create pack.');
        }

        return redirect()->route('admin.createpack');
    }

    public function modifyPack(Request $request)
    {
        if (!Gate::allows('admin-level1', Auth::user())) {
            return view('admin.adminhome');
        }

        //Validate information
        $validator = Validator::make($request->all(), [
            'packid' => ['required', 'integer'],
            'packname' => ['required', 'string'],
            'xboxprice' => ['required', 'integer'],
            'ps4price' => ['required', 'integer'],
            'card' => [
                'required',
                'array',
                function ($attribute, $value, $fail) {
                    foreach ($value as $key => $val) {
                        if ($val > 100 || $val < 0)
                            $fail('Invalid probability.');
                    }
                }
            ]
        ]);

        if ($validator->fails()) {
            session()->put('error', $validator->messages()->first());
            return redirect()->back()->withInput();
        }

        try {
            DB::beginTransaction();

            $removedArr = $request->removeCard;
            $packId = $request->packid;

            Pack::find($packId)->update([
                'name' => $request->packname,
                'xbox_price' => $this->getXboxPackPrice($packId),
                'ps4_price' => $this->getPS4PackPrice($packId)
            ]);

            if (is_array($removedArr) && !empty($removedArr))
                Card_pack::where('pack_id', $packId)->whereIn('card_id', $removedArr)->delete();

            foreach ($request->card as $key => $value) {
                try {
                    $card_pack = Card_pack::where([
                        'card_id' => $key,
                        'pack_id' => $packId
                    ])->firstOrFail();
                    $card_pack->probability = $value;
                    $card_pack->save();
                } catch (ModelNotFoundException $exception) {
                    Card_pack::create([
                        'card_id' => $key,
                        'pack_id' => $packId,
                        'probability' => $value
                    ]);
                }
            }

            DB::commit();
            session()->put('success', 'Successfully updated pack.');
        } catch (Exception $exception) {
            DB::rollBack();
            session()->put('warning', 'Unable to update pack.');
        }

        return redirect()->route('admin.displaypacks');
    }

    public function packForm()
    {
        if (!Gate::allows('admin-level1', Auth::user())) {
            return view('admin.adminhome');
        }

        $types = CardType::orderBy('type', 'asc')->get();
        return view('admin.createpackform')->with('types', $types);
    }

    public function displayCards()
    {
        if (!Gate::allows('admin-level1', Auth::user())) {
            return view('admin.adminhome');
        }

        $cards = Card::orderBy('player_name', 'asc')->groupBy('id')->paginate(12);
        return view('admin.managecard')->with('cards', $cards);
    }

    public function cardForm()
    {
        if (!Gate::allows('admin-level1', Auth::user())) {
            return view('admin.adminhome');
        }

        $types = CardType::orderBy('type', 'asc')->get();
        return view('admin.createcard')->with('types', $types);
    }

    public function updateCardPrice(Request $request)
    {
        if (!Gate::allows('admin-level1', Auth::user())) {
            return view('admin.adminhome');
        }

        //Validate information
        $this->validate($request, [
            'cardId' => 'required|integer',
            'xboxPrice' => 'required|regex:/^\d*(\.\d{1,2})?$/',
            'ps4Price' => 'required|regex:/^\d*(\.\d{1,2})?$/'
        ]);

        try {
            $cardId = $request->cardId;
            $ps4Price = $request->ps4Price;
            $xboxPrice = $request->xboxPrice;

            $card = Card::find($cardId);

            if ($ps4Price == 0 && $xboxPrice == 0) {
                $card->delete();
                return response()->json(["change" => "deleted", "cardId" => $cardId]);
            } else {
                $card->ps4_price = $ps4Price;
                $card->xbox_price = $xboxPrice;
                $card->save();

                $allPacksWithThisCard = Card_pack::where('card_id', $cardId)->get();
                foreach ($allPacksWithThisCard as $packDetail) {
                    try {
                        $packId = $packDetail->pack_id;
                        $pack = Pack::findOrFail($packId);

                        $cardArr = [];
                        $allCardsInPack = Card_pack::where('pack_id', $packId)->get();
                        foreach ($allCardsInPack as $cardDetail) {
                            $cardArr[$cardDetail->card_id] = $cardDetail->probability;
                        }

                        $pack->update([
                            'xbox_price' => $this->getXboxPackPrice( $packId),
                            'ps4_price' => $this->getPS4PackPrice( $packId)
                        ]);
                    } catch (ModelNotFoundException $exception) {
                        //TODO: do something?
                    }
                }
                return response()->json(["change" => "updated"]);
            }
        } catch (Exception $exception) {
            //TODO: LOG ERRORS SOMEHOW
        }

        return null;
    }

    public function createCard(Request $request)
    {
        if (!Gate::allows('admin-level1', Auth::user())) {
            return view('admin.adminhome');
        }

        $minYear = date("Y", strtotime("-1 year"));
        $maxYear = date("Y", strtotime("+1 year"));

        //Validate information
        $validator = Validator::make($request->all(), [
            'cardtype' => 'required|integer',
            'league' => [
                'required',
                'string',
                Rule::in(config('league'))
            ],
            'team' => [
                'required',
                'string',
                Rule::in(config('team'))
            ],
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'position' => [
                'required',
                'string',
                Rule::in(config('position'))
            ],
            'synergy' => [
                'required',
                'array',
                function ($attribute, $value, $fail) {
                    foreach ($value as $key => $val) {
                        if (!in_array($key, config('synergy')))
                            $fail('Synergy ' . $key . ' not found');
                        elseif (isset($val) && $val <= 0)
                            $fail('Invalid synergy value: ' . $key);

                    }
                }
            ],
            'overall' => 'required|integer|digits_between:1,99',
            'xboxprice' => 'required|numeric|gte:0',
            'ps4price' => 'required|numeric|gte:0',
            'year' => 'required|integer|max:' . $maxYear . '|min:' . $minYear
        ]);

        if ($validator->fails()) {
            session()->put('error', $validator->messages()->first());
            return redirect()->back()->withInput();
        }

        try {
            $synergyArr = $request->synergy;
            $synergyVal = '';
            foreach ($synergyArr as $key => $val) {
                if ($val > 0) {
                    $strVal = $key . ' (' . $val . ') ';
                    $synergyVal .= $strVal;
                }
            }

            Card::create([
                'type_id' => $request->cardtype,
                'league' => $request->league,
                'team' => $request->team,
                'player_name' => $request->firstname . ' ' . $request->lastname,
                'position' => $request->position,
                'synergy' => $synergyVal,
                'overall' => $request->overall,
                'xbox_price' => $request->xboxprice,
                'ps4_price' => $request->ps4price,
                'year' => $request->year
            ]);

            session()->put('success', 'Card successfully created.');
        } catch (Exception $exception) {
            session()->put('warning', 'Unable to create card.');
        }

        return redirect()->route('admin.createcard');
    }

    // IMPORTANT: cardArr should be in the format:  cardId => probability
    private function getPS4PackPrice( $packId)
    {
        $total = 0;
        $pack = Pack::find($packId);
        $cards =  $pack->cards()->get();
        foreach($cards as $card){
            $total += (($card->ps4_price / \Illuminate\Support\Facades\Config::get('coins.ps4_price')) *  $card->pivot->probability) * $pack->guaranteed_cards;
        }

        return ceil($total/100) * 100;

    }

    private function getXboxPackPrice( $packId)
    {
        $total = 0;
        $pack = Pack::find($packId);
        $cards =  $pack->cards()->get();
        foreach($cards as $card){
            $total += (($card->xbox_price / \Illuminate\Support\Facades\Config::get('coins.xbox_price')) *  $card->pivot->probability) * $pack->guaranteed_cards;
        }
        return ceil($total/100) * 100;

    }
}
