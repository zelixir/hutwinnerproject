<?php

namespace App\Http\Controllers\Admin;

use App\Note;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;

class AdminTransactionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function getTransactions()
    {
        //TODO: could maybe store the pagination number in a config file ??
        $transactions = Transaction::orderBy('created_at', 'desc')->paginate(10);
        return view('admin.transactions')->with('transactions', $transactions );
    }

    /**
     * Get the transaction by id
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getTransactionById($id)
    {
        try {
            $transaction = Transaction::findOrFail($id);
            return view('admin.transaction')->with('transaction', $transaction);
        } catch (ModelNotFoundException $exception) {
            session()->put('warning', 'Record not found.');
            return redirect()->route('admin.transactions');
        }
    }

    public function addNote(Request $request)
    {
        //Validate information
        $this->validate($request, [
            'transactionId' => 'required|integer',
            'note' => 'required|string'
        ]);

        try {
            $adminId = Auth::guard('admin')->user()->id;
            Note::create([
                'transaction_id' => $request->transactionId,
                'admin_id' => $adminId,
                'note' => $request->note
            ]);

            return redirect()->route('admin.transaction', ['id' => $request->transactionId]);
        } catch (ModelNotFoundException $exception) {
            session()->put('warning', 'Unable to add note.');
            return redirect()->route('admin.transactions');
        }
    }
}
