<?php

namespace App\Http\Controllers;

use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class TransactionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show(Request $request){
        $user = Auth::user();
        if(Config::get('statuses.user.unverify') != $user->status()->first()->id){
            return view('purchase',[
                'option1' => true,
                'option2' => true,
                'option3' => true,
                'option4' => true,
                'option5' => true
            ]);
        }
        $amount = $user->lastTransactions();
        if($amount > 45){
            return view('purchase',[
                'option1' => false,
                'option2' => false,
                'option3' => false,
                'option4' => false,
                'option5' => true
            ]);
        }

        if($amount > 40){
            return view('purchase',[
                'option1' => true,
                'option2' => false,
                'option3' => false,
                'option4' => false,
                'option5' => false
            ]);
        }

        if ($amount > 30){
            return view('purchase',[
                'option1' => true,
                'option2' => true,
                'option3' => false,
                'option4' => false,
                'option5' => true
            ]);
        }

        if($amount > 0){
            return view('purchase',[
                'option1' => true,
                'option2' => true,
                'option3' => true,
                'option4' => false,
                'option5' => true
            ]);
        }

        if($amount == 0){
            return view('purchase',[
                'option1' => true,
                'option2' => true,
                'option3' => true,
                'option4' => true,
                'option5' => true
            ]);
        }
    }


    public function insertTransaction(Request $request){
        if($this->checkData($request)){
            $user = Auth::user();
            $userId = $user->id;

            Transaction::create(
                [
                    'user_id'=> $userId,
                    'payment_id'=> 1,
                    'points' => $request->coins,
                    'price' => $request->price,
                    'description' => $request->description
                ]
            );
            $user->coins = $user->coins + $request->coins;
            $user->save();

            //TODO: syslog thing

            //openlog("paypal", LOG_PID | LOG_PERROR);
            //syslog(LOG_ERR, $text);
            //closelog();

            Log:info("PAYPAL TRANSACTION: The following User: " . $user->email . " has bought " . $request->coins . " Points for " . $request->price . "USD");

            return response()->json(['message' => $user->coins]);
        }
        else
            return response()->json(['message' => 'fail']);
    }

    private function checkData(Request $request){
        $price = $request->price;
        $coins = $request->coins;
        $description = $request->description;

        if(isset($price) && isset($coins) && isset($description) && !empty($price) && !empty($coins) && !empty($description))
            if(is_numeric($price) && is_numeric($coins) && $this->compareAmountAndCoins($price, $coins))
                return true;
            return false;
    }

    private function compareAmountAndCoins($amount, $coins){
        if($amount == 12.8 && $coins == 5820)
            return true;
        else if ($amount == 32 && $coins == 14550)
            return true;
        else if ($amount == 64 && $coins == 29100)
            return true;
        else if ($amount == 128 && $coins == 58200)
            return true;
        else
            return false;
    }
}
