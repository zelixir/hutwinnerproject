<?php
/**
 * Created by PhpStorm.
 * User: samoumbarek
 * Date: 2019-01-13
 * Time: 11:02 PM
 */

namespace App\Console\Commands;

use App\Card;
use Illuminate\Console\Command;
use TeamTNT\TNTSearch\Indexer\TNTIndexer;

class IndexCards extends Command
{
    private $tnt;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'index:cards';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Index the cards table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->tnt = new TNTIndexer;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cards = Card::all();
        foreach ($cards as $card) {
           $val = $this->createNGrams($card->player_name);
           echo $val;
           $card['n_grams']=$val;
           $card->save();
        }

    }

    public function createNGrams($word)
    {
        return utf8_encode($this->tnt->buildTrigrams($word));
    }
}