<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;
use TeamTNT\TNTSearch\TNTSearch;

class CreatePackTrigrams extends Command
{/**
 * The name and signature of the console command.
 *
 * @var string
 */
    protected $signature = 'pack:trigrams';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates an index of pack trigrams';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Creating index of pack trigrams");
        $tnt = new TNTSearch;
        $driver = config('database.default');
        $config = config('scout.tntsearch') + config("database.connections.$driver");
        $tnt->loadConfig($config);
        $tnt->setDatabaseHandle(app('db')->connection()->getPdo());
        $indexer = $tnt->createIndex('packngrams.index');
        $indexer->query('SELECT id,name, n_grams FROM  packs;');
        $indexer->setLanguage('no');
        $indexer->run();
    }
}
