<?php

namespace App\Console\Commands;

use App\Pack;
use Illuminate\Console\Command;
use TeamTNT\TNTSearch\Indexer\TNTIndexer;
class IndexPacks extends Command
{
    private $tnt;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'index:packs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Index the packs table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->tnt = new TNTIndexer;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $packs = Pack::all();
        foreach ($packs as $pack) {
            $val = $this->createNGrams($pack->name);
            echo $val;
            $pack['n_grams']=$val;
            $pack->save();
        }

    }

    public function createNGrams($word)
    {
        return utf8_encode($this->tnt->buildTrigrams($word));
    }
}
