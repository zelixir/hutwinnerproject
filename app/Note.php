<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $fillable =['transaction_id', 'admin_id', 'note', 'withdraw_id', 'user_id'];
    protected $table = 'notes';


    public function admin(){
        return $this->belongsTo(Admin::class);
    }

    public function transaction(){
        return $this->belongsTo(Transaction::class);
    }

    public function withdraw()
    {
        return $this->belongsTo(Withdraw::class);
    }

    public function getCreatedAtAttribute($date)
    {
        return \Carbon\Carbon::parse($date)->timezone('America/Toronto');
    }
}
