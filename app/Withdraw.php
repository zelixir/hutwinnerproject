<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Withdraw extends Model
{
    protected $table = 'withdraws';
    protected $fillable = ['user_id', 'card_id', 'status_id', 'amount'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function card()
    {
        return $this->belongsTo(Card::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function note()
    {
        return $this->hasMany(Note::class)->orderBy('created_at', 'desc');
    }

    public function getCreatedAtAttribute($date)
    {
        return \Carbon\Carbon::parse($date)->timezone('America/Toronto');
    }
}
