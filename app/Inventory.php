<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
class Inventory extends Model
{
    /**
     * primaryKey
     *
     * @var integer
     * @access protected
     */
    protected $primaryKey = ['user_id', 'card_id'];
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    protected $table = 'inventories';

    protected $fillable = ['user_id', 'card_id', 'amount'];

    /**
     * Return the user of a given inventory
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Returns all the cards that a user has in his inventory
     */
    public function cards()
    {
        return $this->belongsTo(Card::class,'card_id');
    }

    protected function setKeysForSaveQuery(Builder $query)
    {
        return $query->where('user_id', $this->getAttribute('user_id'))
            ->where('card_id', $this->getAttribute('card_id'));
    }
}
