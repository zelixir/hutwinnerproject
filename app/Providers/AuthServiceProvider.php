<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('admin-level1', function ($admin) {
            if ($admin->admintype == 1)
                return true;
            else
                return false;
        });

        Gate::define('admin-level2', function ($admin) {
            if ($admin->admintype <= 2)
                return true;
            else
                return false;
        });

        Gate::define('admin-level3', function ($admin) {
            if ($admin->admintype <= 3)
                return true;
            else
                return false;
        });
    }
}
