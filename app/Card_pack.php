<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;

class Card_pack extends Model
{
    protected $table = 'card_pack';
    public $incrementing = false;
    protected $fillable = ['probability','card_id','pack_id'];
    protected $primaryKey = ['card_id', 'pack_id'];


    /**
     * Get the probability of a Card
     *
     * @return mixed
     */
    public function probability(){
        return DB::table('card_pack')
            ->select('probability')
            ->join('cards', 'card_pack.card_id', '=', 'cards.id')
            ->join('packs', 'card_pack.card_id', '=', 'packs.id')
            ->get()->first();
    }
    public function card(){
        return DB::table('card')
            ->join('cards', 'card_pack.card_id', '=', 'cards.id')
            ->get()->first();
    }
    public function pack(){
        return DB::table('card_pack')
            ->join('packs', 'card_pack.card_id', '=', 'packs.id')
            ->get()->first();
    }

    /**
     * Set the keys for a save update query.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function setKeysForSaveQuery(Builder $query)
    {
        $keys = $this->getKeyName();
        if(!is_array($keys)){
            return parent::setKeysForSaveQuery($query);
        }

        foreach($keys as $keyName){
            $query->where($keyName, '=', $this->getKeyForSaveQuery($keyName));
        }

        return $query;
    }

    /**
     * Get the primary key value for a save query.
     *
     * @param mixed $keyName
     * @return mixed
     */
    protected function getKeyForSaveQuery($keyName = null)
    {
        if(is_null($keyName)){
            $keyName = $this->getKeyName();
        }

        if (isset($this->original[$keyName])) {
            return $this->original[$keyName];
        }

        return $this->getAttribute($keyName);
    }
}
