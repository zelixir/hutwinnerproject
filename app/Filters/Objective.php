<?php
/**
 * Created by PhpStorm.
 * User: samoumbarek
 * Date: 2019-01-12
 * Time: 1:30 PM
 */

namespace App\Filters;


interface Objective
{


    public function passes();

    public function message();

}