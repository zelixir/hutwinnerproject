<?php
/**
 * Created by PhpStorm.
 * User: samoumbarek
 * Date: 2019-01-14
 * Time: 4:16 PM
 */

namespace App\Filters;


use App\Card;
use App\Pack;

class CardExist implements  Objective
{
    private $userInput;
    private $errorMesssage;

    public function __construct($userInput)
    {
        $this->userInput=$userInput;
    }

    public function passes()
    {
        $card = Card::all()->where([
            'player_name'=>$this->userInput,
        ])->get();
        if(isset($card)){
            return true;
        }
        else{
            $this->errorMesssage = 'An error has occurred';
            return false;
        }
    }

    public function message()
    {
    }
}