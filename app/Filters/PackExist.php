<?php
/**
 * Created by PhpStorm.
 * User: samoumbarek
 * Date: 2019-01-14
 * Time: 4:16 PM
 */

namespace App\Filters;


use App\Pack;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Exception;

class PackExist implements Objective
{
    private $userInput;
    private $errorMesssage;
    private $pack;
    private $typeCheck;

    public function __construct(Request $userInput, $typeCheck)
    {
        $this->typeCheck = $typeCheck;
        $this->userInput = $userInput;
        try {
            $this->pack = Pack::find($this->userInput->post('pack'));
        }catch (Exception $e){
            $this->pack = null;
        }
    }


    public function passes()
    {
        if ($this->exist())
            if ($this->typeCheck == 1)
                return true;
            else
                if ($this->matchurl())
                    return true;
        $this->errorMesssage = 'An error has occurred';
        return false;

    }

    public function message()
    {
        return $this->errorMesssage;
    }

    private function matchurl()
    {
        if ($this->exist()) {
            $routeValue = strrpos(URL::previous(), '/');
            $packId = substr(URL::previous(), $routeValue + 1);
            if ($this->pack->id == $packId)
                return true;
        }
        return false;


    }

    private function exist()
    {
        if (isset($this->pack) && !empty($this->pack))
            if ($this->pack->name == $this->userInput->post('name'))
                return true;
        return false;
    }

    public function getPack()
    {
        return $this->pack;
    }

}