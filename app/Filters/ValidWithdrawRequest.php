<?php
/**
 * Created by PhpStorm.
 * User: samoumbarek
 * Date: 2019-01-18
 * Time: 11:26 PM
 */

namespace App\Filters;


use App\User;
use Illuminate\Http\Request;
use App\Repositories\InventoryRepository;
use App\Card;


class ValidWithdrawRequest implements Objective
{
    private $user;
    private $userinput;
    private $cards;
    private $errorMessage;

    public function __construct(Request $userInput, User $user)
    {
        $this->user = $user;
        $this->userinput = $userInput;
    }


    public function passes()
    {
        $repo = new InventoryRepository();
        $this->cards = $repo->getInventoryCards($this->user);
        if ($this->validAmount())
            if ($this->validMesasge($this->userinput))
                if ($this->hasCards())
                    return true;

        $this->errorMessage = 'An error has occurred during withdraw request';
        return false;


    }

    public function message()
    {
        return $this->errorMessage;
    }

    private function validAmount()
    {
        $val = $this->userinput->post('value');
        if (isset($val) && !empty($val)) {
            if (count($val) >= 1 && count($val) <= 4) {
                foreach ($val as $value) {
                    if (!is_numeric($value)) {
                        return false;
                    }
                }
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private function hasCards()
    {
        foreach ($this->userinput->post('value') as $id) {
            $val = $this->cards->where(
                'card_id', '=', $id
            )->all();

            if (isset($val) && !empty($val)) {
                if (count($val) == 1) {
                    $val = $this->cards->where('card_id', $id)->first();
                    $val->amount -= 1;
                    $val->save();
                    if ($val->amount == 0)
                        $val->delete();
                } else {
                    return false;
                }
            } else {
                return false;
            }


        }
        return true;
    }

    private function validMesasge(Request $request)
    {
        $playerName = $request->post('player_name');
        $teamName = $request->user()->hut_team;
        $startPrice = $request->post('start_price');
        $buyNowPrice = $request->post('buy_now');

        if (empty($playerName) || !isset($playerName) || empty($teamName) || !isset($teamName) || !is_numeric($startPrice) || !isset($startPrice) || empty($startPrice) || !is_numeric($buyNowPrice) || !isset($buyNowPrice) || empty($buyNowPrice)) {
            return false;
        }
        return true;
    }
}