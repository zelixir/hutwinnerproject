<?php
/**
 * Created by PhpStorm.
 * User: samoumbarek
 * Date: 2019-01-12
 * Time: 2:18 PM
 */

namespace App\Filters;


use App\Card;
use App\User;

class ValidQuickSell implements Objective
{
    private $user;
    private $card;
    private $errorMessage;

    /**
     * ValidQuickSell constructor.
     * @param $user
     * @param $card
     */
    public function __construct(User $user, Card $card)
    {
        $this->user = $user;
        $this->card = $card;
    }


    public function passes()
    {
        $dbCard = $this->user->inventory()->cards()->where([
            'id', '=', $this->card->id
        ])->exist();
        if ($dbCard) {
            return true;
        } else {
            $this->errorMessage = "The following card {$this->card->name} is not in you inventory";
            return false;
        }


    }

    public function message()
    {
        return $this->errorMessage;
    }
}