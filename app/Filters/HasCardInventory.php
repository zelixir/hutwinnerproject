<?php
/**
 * Created by PhpStorm.
 * User: samoumbarek
 * Date: 2019-01-17
 * Time: 1:58 PM
 */

namespace App\Filters;


use App\User;
use Illuminate\Http\Request;

class HasCardInventory implements Objective
{

    private $userInput;
    private $errorMessage;
    private $card;
    private $user;


    public function __construct(Request $userInput, User $user)
    {
        $this->user=$user;
        $this->userInput = $userInput;
        $this->card = Card::find($this->userInput->post('card'));
    }

    public function passes()
    {
        if($this->user->inventory()->where([
            ['card_id' ,'=', $this->card],
            ['user_id' , '=',$this->user->id]
        ])->exists()){
            return true;
        }
        else{
            $this->errorMessage= "You do not have that card in your inventory";
            return false;
        }
    }

    public function message()
    {
        return $this->errorMessage;
    }
}