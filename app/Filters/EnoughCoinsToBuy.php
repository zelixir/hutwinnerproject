<?php
/**
 * Created by PhpStorm.
 * User: samoumbarek
 * Date: 2019-01-12
 * Time: 2:03 PM
 */

namespace App\Filters;


use App\Pack;
use App\User;
use Illuminate\Support\Facades\Auth;

/**
 * Class EnoughCoinsToBuy
 * @package App\Filters
 */
class EnoughCoinsToBuy implements Objective
{
    private $user;
    private $pack;
    private $console;
    private $errorMesssage;
    public function __construct(User $user , $pack)
    {
        $this->user=$user;
        $this->pack=$pack;
    }

    /**
     * Checks if user can buy the pack.
     * User must have enough coins in his balance to be able to buy a pack, if not then
     * error message will be set and will return false , otherwise returns true.
     * @return bool
     */
    public function passes()
    {
        if(!isset($this->pack))
            return false;
        if(Auth::user()->console == 'ps4')
            $amountPurchase= $this->pack->ps4_price;
        else
            $amountPurchase= $this->pack->xbox_price;
        $userBalance= $this->user->coins;
        $userCoins = intval($this->user->coins);
        if($userBalance<$amountPurchase){
            $this->errorMesssage="Insufficient Points. You have {$userCoins} Points but you need $amountPurchase Points to buy this pack";
            return false;
        }
        else{
            return true;
        }
    }
    /**
     * Returns String representation of the error when user tried to make a purchase of a pack.
     * @return mixed
     */
    public function message()
    {
        return $this->errorMesssage;
    }
}