<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PaymentMethod
 * @package App
 */
class PaymentMethod extends Model
{
    protected $fillable = ['method'];
    protected $table = 'payment_methods';

    /**
     * Returns all transactions for a given payment method.
     */
    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }
}
