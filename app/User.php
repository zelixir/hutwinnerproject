<?php

namespace App;


use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;


/**
 * Class User
 * @author Sammy Chaouki
 * @package App
 */
class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'firstname', 'lastname', 'console', 'phone','packs_created','pack_first','hut_team','username','status_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Returns all the cards that a user has in his inventory
     */
    public function inventory()
    {
        return $this->hasMany(Inventory::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    /**
     * Returns all the packs that a user has created.
     */
    public function user_packs()
    {
        return $this->hasMany(Pack::class)->where('author', $this->id);
    }

    /**
     * Returns all the packs that a user has bought, but did not yet open.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function unopened_packs()
    {
        return $this->belongsToMany(Pack::class)->withPivot('amount')->withTimestamps();
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public function withdraws(){
        return $this->hasMany(Withdraw::class);
    }

    public function lastTransactions(){
        return $this->hasMany(Transaction::class)
            ->where('created_at', '>=', Carbon::parse('-24 hours'))
            ->sum('price');
    }
    public function unopenedPacks_number(){
        $packs = $this->unopened_packs()->get();
        $value = 0;
        foreach ($packs as $pack){
            $value += $pack->pivot->amount;
        }
        return $value;
    }

    public function note()
    {
        return $this->hasMany(Note::class)->orderBy('created_at', 'desc');
    }
}
