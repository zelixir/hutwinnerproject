<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackCover extends Model
{
    protected $fillable =['image'];
    protected $table = 'pack_cover';
}
