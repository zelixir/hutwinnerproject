<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CardType extends Model
{
    protected $table = 'card_types';

    protected $fillable = ['type'];

    /**
     * Returns all the cards of a given type of cards
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cards()
    {
        // Would be good when using filters.
        return $this->hasMany(Card::class,'type_id');
    }
}
