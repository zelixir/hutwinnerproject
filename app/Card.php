<?php

namespace App;

use Laravel\Scout\Searchable;
use App\CardType;
use Illuminate\Database\Eloquent\Model;
use App\Pack;

class Card extends Model
{
    use Searchable;

    protected $primaryKey = 'id';
    public $asYouType = false;
    protected $table = 'cards';

    protected $fillable = ['type_id', 'league', 'team', 'player_name', 'position', 'synergy', 'overall', 'xbox_price', 'ps4_price', 'year'];


    /**
     * Get the pack that belongs to the card.
     *
     */
    public function packs()
    {
        return $this->belongsToMany(Pack::class)
            ->withPivot('probability')
            ->withTimestamps();
    }


    public function type()
    {
        return $this->belongsTo(CardType::class);
    }

    public function team()
    {
        return $this->belongsTo(Team::class, 'team');
    }

    public function cardType()
    {
        return $this->belongsTo(CardType::class, 'type_id');
    }

    public function inventory()
    {
        return $this->hasMany(Inventory::class, 'id');
    }

    public function toSearchableArray()
    {
        return array_only($this->toArray(), ['id', 'player_name']);
    }

    public function withdraws()
    {
        return $this->belongsToMany(Withdraw::class)->withTimestamps();
    }
}
