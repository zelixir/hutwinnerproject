<?php
/**
 * Created by PhpStorm.
 * User: samoumbarek
 * Date: 2019-01-14
 * Time: 3:10 PM
 */

namespace App\Services;

use App\Card;
use App\Pack;
use TeamTNT\TNTSearch\Indexer\TNTIndexer;
use TeamTNT\TNTSearch\TNTSearch;

class SearchService
{
    public function cardSearch($request)
    {
        $val = $request->get('playername');
        if (isset($val)) {
            $cards = Card::search($val)->get();
            if (isset($cards[0])) {
                return $cards;
            } else {
                return
                    $this->suggestionCards($this->suggesstionsetUp(), $request->get('playername'));
            }

        } else {
            return null;
        }
    }

    public function packSearch($request)
    {
        $val = $request->get('name');
        if (isset($val)) {
            $packs = Pack::search($val)->get();
            if (isset($packs[0])) {
                    return $packs;

            } else {
                return $this->suggestionPacks($this->suggesstionsetUp(), $request->get('name'));

            }

        } else {
            return null;
        }
    }

    private function suggesstionsetUp()
    {
        $tnt = new TNTSearch;
        $driver = config('database.default');
        $config = config('scout.tntsearch') + config("database.connections.$driver");
        $tnt->loadConfig($config);
        $tnt->setDatabaseHandle(app('db')->connection()->getPdo());
        return $tnt;
    }

    private function suggestionCards(TNTSearch $tnt, $userInput)
    {
        $TNTIndexer = new TNTIndexer();
        $trigrams = utf8_encode($TNTIndexer->buildTrigrams($userInput));
        $tnt->selectIndex("cardngrams.index");
        $res = $tnt->search($trigrams, 9);
        $keys = collect($res['ids'])->values()->all();
        $cardSuggestions = Card::whereIn('id', $keys)->get();
        $cardSuggestions->map(function ($card) use ($userInput) {
            $card->distance = levenshtein($userInput, $card->player_name);
        });

        $sortedCards = $cardSuggestions->sort(function ($a, $b) {
            if ($a->distance === $b->distance) {
                if ($a->player_name === $b->player_name) {
                    return 0;
                }
                return $a->player_name > $b->player_name ? -1 : 1;
            }
            return $a->distance < $b->distance ? -1 : 1;
        });
        return $sortedCards->values()->all();
    }

    private function suggestionPacks(TNTSearch $tnt, $userInput)
    {
        $TNTIndexer = new TNTIndexer();
        $trigrams = utf8_encode($TNTIndexer->buildTrigrams($userInput));
        $tnt->selectIndex("packngrams.index");
        $res = $tnt->search($trigrams, 10);
        $keys = collect($res['ids'])->values()->all();
        $packSuggestions = Pack::whereIn('id', $keys)->get();
        $packSuggestions->map(function ($pack) use ($userInput) {
            $pack->distance = levenshtein($userInput, $pack->player_name);
        });

        $sortedPacks = $packSuggestions->sort(function ($a, $b) {
            if ($a->distance === $b->distance) {
                if ($a->player_name === $b->player_name) {
                    return 0;
                }
                return $a->player_name > $b->player_name ? -1 : 1;
            }
            return $a->distance < $b->distance ? -1 : 1;
        });
        return $sortedPacks->values()->all();
    }

}