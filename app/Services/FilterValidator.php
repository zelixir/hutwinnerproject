<?php
/**
 * Created by PhpStorm.
 * User: samoumbarek
 * Date: 2019-01-14
 * Time: 4:15 PM
 */

namespace App\Services;

use App\Filters\Objective;


class FilterValidator
{
    private $filters;
    private $error;

    public function __construct(array $filters)
    {
        $this->filters = $filters;
    }

    /**
     * Validates all the passed objectives in a the order they were passed.
     * Returns false as soon as one of the objectives is not passed.
     *
     * @return True if validation fails, false if validation does not fail
     */
    public function fails()
    {
        $error = null;
        foreach ($this->filters as $filter) {
            if (!$filter->passes()) {
                $this->error= $filter->message();
               return  true;
            }
        }
        return false;

    }

    /**
     * @return string Returns the error message
     */
    public function error()
    {
        return $this->error;
    }
}