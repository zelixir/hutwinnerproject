<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table='statuses';
    protected $fillable=['type'];

    public function withdraws(){
        return $this->hasMany(Withdraw::class);
    }

}
