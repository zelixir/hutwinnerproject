<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $team_abr
 * @property string $team_name
 * @property string $color
 */
class Team extends Model
{


    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'team';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'team_abr';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['team_abr','team_name', 'color'];

    public function cards(){
        return $this->hasMany(Card::class);
    }

}
