// function for verify button
$('.verify-user-btn').click(function () {
    let getUrl = window.location;
    let baseUrl = getUrl.protocol + "//" + getUrl.host;
    let userId = $(this).val().trim();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: baseUrl + '/admin/users/modify',
        type: 'post',
        data: {'userId': userId, 'action': 'verify'},
        dataType: 'json',
        success: function (response) {
            $(`#user-${userId}-status`).html(response.statustype);
            $.notify({
                message: response.message
            }, {
                type: 'success',
                animate: {
                    enter: 'animated fadeInUp',
                    exit: 'animated fadeOutRight'
                }
            }).show();
        },
        error: function (response) {
            $.notify({
                message: response.message
            }, {
                type: 'warning',
                animate: {
                    enter: 'animated fadeInUp',
                    exit: 'animated fadeOutRight'
                }
            }).show();
        }

    });
});

// function for unverify button
$('.unverify-user-btn').click(function () {
    let getUrl = window.location;
    let baseUrl = getUrl.protocol + "//" + getUrl.host;
    let userId = $(this).val().trim();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: baseUrl + '/admin/users/modify',
        type: 'post',
        data: {'userId': userId, 'action': 'unverify'},
        dataType: 'json',
        success: function (response) {
            $(`#user-${userId}-status`).html(response.statustype);
            $.notify({
                message: response.message
            }, {
                type: 'success',
                animate: {
                    enter: 'animated fadeInUp',
                    exit: 'animated fadeOutRight'
                }
            }).show();
        },
        error: function (response) {
            $.notify({
                message: response.message
            }, {
                type: 'warning',
                animate: {
                    enter: 'animated fadeInUp',
                    exit: 'animated fadeOutRight'
                }
            }).show();
        }

    });
});