$(document).ready(function () {
    $('.close').on('click', function () {
        $('#myModal').modal('hide')
    });

    $('#Confirm').on('click',function(){
        $('#myModal').modal('hide')
    })
    $(window).click(function (e) {
        if (e.target == modal) {
            $('#myModal').modal('hide')
        }
    });
    $('.dropdown-toggle').dropdown()

});

$(document).on('click', '.sellNow', function(){
    preparePopup($(this).next());
    $('#myModal').modal('show')
})

function preparePopup(elementContainer) {
    let x = 0;
    let y = $(elementContainer).children();
    child = $(elementContainer).children().each(function () {
        if (x == 0) {
            storeCardPrincipalInfo($(this));
        } else {
            storePlayerName($(this));
        }
        x++;
    });

}

function storeCardPrincipalInfo(element) {
    /**
     * Create input hidden field with value of element
     */

    $("#card").val($(element).val());

}

function storePlayerName(element) {
    $('#header').text('Are you sure you want to Sell this item? You are going to receive ' + $(element).val() + ' Points. This action is irreversible');
}

function storeQuantity(element) {
    let ele = $('#quantityCard');
    $(ele).empty();
    for (let i = 1 ; i <= $(element).val();i++){
        $('<option value= " '+ i+' "  >'+ i+'</option>').appendTo($(ele));
    }
}