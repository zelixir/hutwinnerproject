$(document).ready(function () {
    function insertTransaction(price, description, coins) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/paypalPayement',
            type: 'post',
            data: {"price": price, "description": description, "coins": coins},
            dataType: 'json',
            success: function (response) {
                if (response['message'] === 'fail') {
                    console.log('returned fail');
                    $('#failed').css('display', 'block');
                    window.setTimeout(function() {
                        $("#failed").fadeTo(1000, 0).slideUp(1000, function(){
                            $(this).remove();
                        });
                    }, 5000);
                }
                else {
                    location.reload();
                }
            },
            error: function (response) {
                console.log('in error');
                $('#failed').css('display', 'block');
                window.setTimeout(function() {
                    $("#failed").fadeTo(1000, 0).slideUp(1000, function(){
                        $(this).remove();
                    });
                }, 5000);
            }
        });
    }

    $('.how-to-etransfer').click(function (e) {
        e.preventDefault();
    });

    let package_1 = '5,820 Points';
    let pricePackage_1 = 12.8;

    let package_2 = '14,550 Points';
    let pricePackage_2 = 32;

    let package_3 = '29,100 Points';
    let pricePackage_3 = 64;

    let package_4 = '58,200 Points';
    let pricePackage_4 = 128;

    let price;
    let description;

    paypal.Button.render({
        env: 'production', // 'production'/ 'sandbox'
        style: {
            size: 'small',
            color: 'gold',
            shape: 'pill',
            label: 'paypal',
            tagline: false
        },
        // Set up the payment:
        // 1. Add a payment callback
        payment: function (data, actions) {
            /*$.ajaxSetup({
                headers:{
                    Allow: 'GET, POST, HEAD'
                }
            });*/
            // 2. Make a request to your server
            return actions.request.post('/api/create-payment', {
                item: package_1,
                price: pricePackage_1
            })
                .then(function (res) {
                    // 3. Return res.id from the response
                    price = res['transactions'][0]['amount']['total'];
                    description = res['transactions'][0]['item_list']['items'][0]['name'];
                    console.log('create payment complete');
                    return res.id;
                });
        },
        // Execute the payment:
        // 1. Add an onAuthorize callback

        onAuthorize: function (data, actions) {
            // 2. Make a request to your server
            return actions.request.post('/api/execute-payment', {
                paymentID: data.paymentID,
                payerID: data.payerID,
                price: price,
                description: description,
                coins: 5820
            })
                .then(function (res) {
                    // 3. Show the buyer a confirmation message.
                    insertTransaction(price, description, 5820);
                });
        }
    }, '#paypal-2000pts');
    paypal.Button.render({
        env: 'production', // 'production'/ 'sandbox'
        style: {
            size: 'small',
            color: 'gold',
            shape: 'pill',
            label: 'paypal',
            tagline: false
        },
        // Set up the payment:
        // 1. Add a payment callback
        payment: function (data, actions) {
            // 2. Make a request to your server
            return actions.request.post('/api/create-payment', {
                item: package_2,
                price: pricePackage_2
            })
                .then(function (res) {
                    // 3. Return res.id from the response
                    price = res['transactions'][0]['amount']['total'];
                    description = res['transactions'][0]['item_list']['items'][0]['name'];
                    return res.id;
                });
        },
        // Execute the payment:
        // 1. Add an onAuthorize callback
        onAuthorize: function (data, actions) {
            // 2. Make a request to your server
            return actions.request.post('/api/execute-payment', {
                paymentID: data.paymentID,
                payerID: data.payerID,
                price: price,
                description: description,
                coins: 14550
            })
                .then(function (res) {
                    // 3. Show the buyer a confirmation message.
                    insertTransaction(price, description, 14550);
                });
        }
    }, '#paypal-4500pts');
    paypal.Button.render({
        env: 'production', // 'production'/ 'sandbox'
        style: {
            size: 'small',
            color: 'gold',
            shape: 'pill',
            label: 'paypal',
            tagline: false
        },
        // Set up the payment:
        // 1. Add a payment callback
        payment: function (data, actions) {
            // 2. Make a request to your server
            return actions.request.post('/api/create-payment', {
                item: package_3,
                price: pricePackage_3
            })
                .then(function (res) {
                    // 3. Return res.id from the response
                    price = res['transactions'][0]['amount']['total'];
                    description = res['transactions'][0]['item_list']['items'][0]['name'];
                    return res.id;
                });
        },
        // Execute the payment:
        // 1. Add an onAuthorize callback
        onAuthorize: function (data, actions) {
            // 2. Make a request to your server
            return actions.request.post('/api/execute-payment', {
                paymentID: data.paymentID,
                payerID: data.payerID,
                price: price,
                description: description,
                coins: 29100
            })
                .then(function (res) {
                    // 3. Show the buyer a confirmation message.
                    insertTransaction(price, description, 29100);
                });
        }
    }, '#paypal-9000pts');
    paypal.Button.render({
        env: 'production', // 'production'/ 'sandbox'
        style: {
            size: 'small',
            color: 'gold',
            shape: 'pill',
            label: 'paypal',
            tagline: false
        },
        // Set up the payment:
        // 1. Add a payment callback
        payment: function (data, actions) {
            // 2. Make a request to your server
            return actions.request.post('/api/create-payment', {
                item: package_4,
                price: pricePackage_4
            })
                .then(function (res) {
                    // 3. Return res.id from the response
                    price = res['transactions'][0]['amount']['total'];
                    description = res['transactions'][0]['item_list']['items'][0]['name'];
                    return res.id;
                });
        },
        // Execute the payment:
        // 1. Add an onAuthorize callback
        onAuthorize: function (data, actions) {
            // 2. Make a request to your server
            return actions.request.post('/api/execute-payment', {
                paymentID: data.paymentID,
                payerID: data.payerID,
                price: price,
                description: description,
                coins: 58200
            })
                .then(function (res) {
                    // 3. Show the buyer a confirmation message.
                    insertTransaction(price, description, 58200);
                });
        }
    }, '#paypal-22500pts');
});