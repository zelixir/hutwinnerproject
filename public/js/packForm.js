function init(){
    var count = 0;
    document.getElementById("nextPageBtn").addEventListener("click", function(){
        if(validateInput()){
            count++;
            showPage(count);
        }else{
            document.getElementById("name").focus();
            document.getElementById("name").blur();
        }
    });

    document.getElementById("backPageBtn").addEventListener("click", function () {
        if(count != 0){
            count--;
            showPage(count);
        }
    });

    $("#card-name").on("keyup", function () {
        val = $(this).val().toLowerCase();
        $(".card-player-name").each(function () {
            $(this).toggle($(this).text().toLowerCase().includes(val));
        });

        //Always show the chosen cards
        $('#chosen-cards').children().css('display', 'block');
    });

    $('.card-player-name').on('click', function () {
        //Get the clicked card
        var clickedCard = $(this);

        //Check if it's already chosen
        if (clickedCard.hasClass('clickedCard')) {
            //Remove the border
            clickedCard.removeClass('clickedCard');

            //Remove the card from the chosen slide view
            $('#chosen-cards').children().each(function () {
                if(clickedCard.children().eq(2).val() === $(this).children().eq(2).val()) {
                    $(this).remove();
                }
            });
            enableDisableButton();
        }
        else {
            $(this).addClass('clickedCard');
            $('#chosen-cards').append($(this).clone().css('border', 'none'));
            enableDisableButton();
        }
    });

    $('.pack-cover').on('click', function () {
        if($(this).hasClass('clickedCard')) {
            $(this).removeClass('clickedCard');
            $('#nextPageBtn').prop("disabled", true);

        }
        else {
            $('.pack-cover').removeClass('clickedCard');
            $(this).addClass('clickedCard');
            $('#image').val("");
            $('#image').val($(this).attr('id'));
            $('#nextPageBtn').prop( "disabled", false);
        }
    });
}

function showPage(count){
    //Page 1
    if(count == 0){
        $('#page2').css('display', 'none');
        $('#page3').css('display', 'none');
        $('#page4').css('display', 'none');
        $('#page5').css('display', 'none');
        $('#page1').css('display', 'block');

        $('#backPageBtn').css('display', 'none');
        $('#nextPageBtn').prop( "disabled", false );
    }

    //Page 2
    else if(count == 1) {
        $('#page1').css('display', 'none');
        $('#page3').css('display', 'none');
        $('#page4').css('display', 'none');
        $('#page5').css('display', 'none');
        $('#page2').css('display', 'block');

        $('#nextPageBtn').prop( "disabled", true );
        $('#backPageBtn').css('display', 'block');
        $('.fixed').css('display', 'none');
    }

    //Page 3
    else if(count == 2){
        $('#page2').css('display', 'none');
        $('#page1').css('display', 'none');
        $('#page4').css('display', 'none');
        $('#page5').css('display', 'none');
        $('#page3').css('display', 'block');

        $('.fixed').css('display', 'block');
        enableDisableButton();
    }

    //Page 4
    else if(count == 3){
        $('#page3').css('display', 'none');
        $('#page2').css('display', 'none');
        $('#page1').css('display', 'none');
        $('#page5').css('display', 'none');
        $('#page4').css('display', 'block');

        $('#finishBtn').css('display', 'none');
        $('#nextPageBtn').css('display', 'block');
        $('.fixed').css('display', 'none');
        showCards();
    }

    //Page 5
    else if(count == 4){
        $('#page4').css('display', 'none');
        $('#page2').css('display', 'none');
        $('#page1').css('display', 'none');
        $('#page3').css('display', 'none');
        $('#page5').css('display', 'block');

        $('#nextPageBtn').css('display', 'none');
        $('#finishBtn').css('display', 'block');
        showPackInfo();
    }
}

function validateInput() {
    if (!document.getElementById("name").value.trim())
        return false;
    else
        $('#pack').val("");
        $('#pack').val(document.getElementById("name").value.trim());
        return true;
}

function enableDisableButton(){
    if($('#chosen-cards').children().length >= 4)
        $('#nextPageBtn').prop('disabled', false);
    else
        $('#nextPageBtn').prop('disabled', true);
}

function showPackInfo(){
    var i=0;
    var percentage = document.getElementsByClassName("percentage");
    $('#pack-name').empty();
    $('#pack-cards').empty();
    $('#pack-name').append($('#name').val());
    $('#percentage-div').empty();
    $('#chosen-cards').children().each(function () {
        var per = percentage[i].options[percentage[i].selectedIndex].value;
        $('#percentage-div').append('<input type="hidden" name="percentage[]" value="'+per+'">');
       $('#pack-cards').append($(this).clone().css('cursor', 'default').append(per + " %"));
       i++;
    });
}

function showCards(){
    $('#card-percentage').empty();
    $('#chosen-cards').children().each(function () {
        var select = "<select class='percentage'>";
        for(var i=1; i<=25; i++){
            select += '<option value="'+i+'">'+i+'</option>';
        }
        select += "</select>";
        $('#card-percentage').append($(this).clone().css('cursor', 'default').append(select));
    });
}

window.onload = init;