// on hover add outline
$('.card').mouseover(function () {
    $(this).addClass('custom-card-outline');
});

// on out remove outline
$('.card').mouseout(function () {
    $(this).removeClass('custom-card-outline');
});