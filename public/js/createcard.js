// JavaScript for disabling form submissions if there are invalid fields
'use strict';
window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    let forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    let validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {

                /* Block of code for finding a div and setting its validation state (green/red border) */
                for(let i = 0; i < form.childNodes.length; i++) {
                    let child = form.childNodes[i];
                    if(child.nodeName == 'DIV' && child.className == 'form-group') {
                        for(let j = 0; j < child.childNodes.length; j++) {
                            let nestedChild = child.childNodes[j];
                            if (nestedChild.nodeName == 'DIV' && nestedChild.id == 'synergyGroup') {
                                nestedChild.childNodes.forEach(function (firstElement) {
                                    if (firstElement.nodeName == 'DIV' && firstElement.className == 'row') {
                                        firstElement.childNodes.forEach(function (secondElement) {
                                            if (secondElement.nodeName == 'DIV') {
                                                secondElement.childNodes.forEach(function (thirdElement) {
                                                    if (thirdElement.nodeName == 'INPUT') {
                                                        if (!thirdElement.checkValidity())
                                                            nestedChild.classList.add('is-invalid');
                                                        else
                                                            nestedChild.classList.add('is-valid');
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    }
                }
                /*  End of block of code */

                event.preventDefault();
                event.stopPropagation();
            }
            form.classList.add('was-validated');
        }, false);
    });

    // validation for synergies
    function synergiesValidation() {
        let valid = false;
        $('input[name^="synergy"]').each(function() {
            let parsed = parseInt($(this).val());
            if(!isNaN(parsed) && parsed > 0) {
                valid = true;
                return;
            }
        });

        if (valid) {
            $('input[name^="synergy"]').each(function() {
                $(this).get(0).setCustomValidity('');
            });

            $('#synergyGroup').removeClass('is-invalid').addClass('is-valid');
        } else {
            $('input[name^="synergy"]').each(function() {
                $(this).get(0).setCustomValidity('Please select at least one synergy');
            });

            if ($('#synergyGroup').attr('class').indexOf('is-valid') >= 0)
                $('#synergyGroup').removeClass('is-valid').addClass('is-invalid');
        }
    }

    $('input[name^="synergy"]').each(function() {
        $(this).on('change', synergiesValidation);
    });

    synergiesValidation();
}, false);