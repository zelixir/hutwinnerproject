
$(document).ready(function () {
    $('#nextPageBtn').on('click', function () {
        if ($('.clickedCard').length < 1 && $('.clickedCard').length > 4) {
            $('#nextPageBtn').attr('disabled', true)
        }
        else{
            let formContainer= $('.modal-footer').first().children().first();
            let container = $('.modal-body').first().empty();
            clearForm(formContainer);
            let val = $('.clickedCard ');
            $.each(val,function(index,value){
                $.each($(value).parent().parent().children(),function(index2,value2){
                    if($(value2).attr('name')=='name'){
                        $('<p>'+ $(value2).val()+'</p>').appendTo($(container));
                    }
                    else if($(value2).attr('name') == 'id'){
                        $('<input type="hidden" value="' + $(value2).val() +'" name="value[]">').appendTo($(formContainer))
                    }
                });
            });
        }
    });


});
function clearForm(element){
    $.each((element).children(),function(index,value){
        if($(value).is('input')){
            if($.isNumeric($(value).val())){
                $(value).remove();
            }
        }
    });

}

$(document).on('click', ".cardContainer", function () {
    if ($(this).hasClass('clickedCard')) {
        $(this).removeClass('clickedCard');
    }
    else {
        if($('.clickedCard').length < 4) {
            $(this).addClass('clickedCard');
            console.log('added clickedcard class');
        }
    }
    if ($('.clickedCard').length <= 4 && $('.clickedCard').length >=1)  {
        $('#nextPageBtn').removeAttr('disabled')
    }
    else {
        $('#nextPageBtn').attr('disabled',true);
    }
});
