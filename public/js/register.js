$(document).ready(function(){
    $("#firstname").keypress(function(event){
        var inputValue = event.charCode;

        if(inputValue == 32 && $('#firstname').val().substr(-1) == ' ') {
            event.preventDefault();
        }

        if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)){
            event.preventDefault();
        }
    });

    $("#lastname").keypress(function(event){
        var inputValue = event.charCode;

        if(inputValue == 32 && $('#lastname').val().substr(-1) == ' ') {
            event.preventDefault();
        }

        if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)){
            event.preventDefault();
        }
    });
});