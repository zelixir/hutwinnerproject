$(document).ready(function () {
    $('#validation').on('click',function(e){
        clearInformation();
        insertIntoModal();
    });
})
function insertIntoModal(){
    let containerModal = $('.modal-body')[0];
    $(".form-group").each(function(){

        if($(this).children().eq(0).is('label') && $(this).children().eq(1).hasClass('form-control')){
            if($(this).children().length == 2){
                let x =$(this).children().eq(0);
                let value = $(this).children().eq(1);
                insertValues(x,value,containerModal)
            }
        }
    });
}
function insertValues(first,second,container){
    $(container).append('<p>' + $(first).text() + ': ' + $(second).val()  );
    $('#containerValues').append('<input type="hidden" name="'+$(first).text()+'" value="'+(second).val() +'">')
}
function clearInformation(){
    let selector =  $('#containerValues');
   let val= $(selector).find('input[name="_token"]');
    $(selector).find('input[type=hidden]').remove();
    $(selector).append($(val));
    $('.modal-body').children().remove();
}