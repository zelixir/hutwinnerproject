$(document).ready(function () {
    $('#nextBtn').on('click', function () {
        if($('#search-player').val().trim()){
            getCardID();
            getPackName();
        }
        else{
            event.preventDefault();
        }
    })
});

function getCardID() {
    $(".cardContainer").each(function () {
        $('#submitForm').append('<input type="hidden" name="cards[]" value="'+$(this).parent().attr('playerid')+'">');
    });
}

function getPackName() {
    $('#submitForm').append('<input type="hidden" name="name" value="'+$('#search-player').val().trim()+'">');
}