$(document).ready(function () {
    disintegrate.init();
    let element = $('.shake-bottom')[0];
    element.addEventListener("animationend", function () {
        let card = $('.card-img-top')[0],
            disObj = disintegrate.getDisObj(card);
        while (disObj == null) {
            disObj = disintegrate.getDisObj(card);
        }
        // Create our particles
        disintegrate.createSimultaneousParticles(disObj);
        $(card).remove();
        $('#displayContainer').remove();
        // Hide the original card

        // Remove the whole thing after the animation completes
        disObj.elem.addEventListener("disComplete", (e) => {
            $('#toggle').remove();
            $('#mainContainer').fadeIn('slow');
        });

    }, false);
});




