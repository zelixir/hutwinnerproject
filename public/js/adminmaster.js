// if scroll past a certain point, fix top navbar
$(window).on('scroll', function() {
    let y_scroll_pos = window.pageYOffset;
    let scroll_pos_test = 150;             // set to whatever you want it to be

    if(y_scroll_pos > scroll_pos_test)
        $('#mainNavbar').addClass('fixed-top');
    else
        $('#mainNavbar').removeClass('fixed-top');
});