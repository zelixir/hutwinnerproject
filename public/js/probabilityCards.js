$(document).ready(function () {

    $('#nextBtn').on("click", function () {
        getProbabilities();
        getCardID();
    });
});

$(document).on('change', "select", function () {
    let sum = 0;
    $("select option:selected").map(function() {sum += parseInt($(this).val());});
    $('.total-percentage').text(sum);
    if(sum == 100)
        $("#nextBtn").attr("disabled", false);
    else
        $("#nextBtn").attr("disabled", true);
});

function getProbabilities() {
    var probabilities = $('select');
    for(var i=0; i<probabilities.length; i++){
        $('#submitForm').append('<input type="hidden" name="probabilities[]" value="'+probabilities[i].options[probabilities[i].selectedIndex].value+'">');
    }
}

function getCardID() {
    $(".cardContainer").each(function () {
        $('#submitForm').append('<input type="hidden" name="cards[]" value="'+$(this).parent().attr('playerid')+'">');
    });
}