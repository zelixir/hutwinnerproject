$(document).ready(function () {
    $('.buttons').on('click', function (event) {
        event.preventDefault();
        if ($('.support-container').css('display') == 'none') {
            $('.support-container').slideDown(function () {
                $('.contact-form').fadeIn();
            });
            $('#response').empty();
            $('#response').empty();
        }
        else {
            $('.contact-form').fadeOut(function () {
                $('.support-container').slideUp();
            });
        }
        return false;
    });

    $('.contact-form').submit(function (event) {
        //Prevent the form from submitting
        event.preventDefault();

        var name = $('#contact-name').val().trim();
        var email = $('#contact-email').val().trim();
        var message = $('#contact-message').val().trim();

        //Check if all the fields are filled before making an Ajax call
        if (name && email && message) {
            var image = $('<img id="loadingRing" src="/images/loadingRing.svg" style="height: 40px; width: 50px;">');
            $('#submit-message').empty().append(image);
            $('#submit-message').prop('disabled', true);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: event.target.action,
                type: 'post',
                data: $('.contact-form').serialize(),
                dataType: 'json',
                success: function (response) {
                    $('#response').empty().append('<p>Your Message Has Been Sent! Thank You For Contacting Us!</p>');
                    $('#submit-message').prop('disabled', false);
                    $('#submit-message').empty().append('Send Message');
                    $('#contact-name,#contact-email,#contact-message').val('');
                },
                error: function (response) {
                    $('#response').empty().append('<p>Something Went Wrong.</p>');
                    $('#submit-message').prop('disabled', false);
                    $('#submit-message').empty().append('Send Message');
                }
            });
        }
        return false;
    });
});
$('body').scroll(function(){
    if(Math.trunc($('body').scrollTop()+$(window).height()) >= ($(document).height() - 60))
        $('.floating-container').css('display', 'none');
    else
        $('.floating-container').css('display', 'block');
});