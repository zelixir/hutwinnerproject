var chosenCards = [];
var addedCards = 0;
$(document).ready(function () {
    var getUrl = window.location;
    var baseUrl = getUrl.protocol + "//" + getUrl.host;

    //setup before functions
    var typingTimer;                //timer identifier
    var doneTypingInterval = 500;  //time in ms, 500 millisecond

    // on keyup, start the countdown
    $('#search-player').on('keyup', function () {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
    });

    //on keydown, clear the countdown
    $('#search-player').on('keydown', function () {
        clearTimeout(typingTimer);
    });

    //user is "finished typing," do something
    function doneTyping() {
        if ($('#search-player').val().trim().length > 2) {
            $.ajax({
                url: baseUrl + '/search/cards',
                type: 'get',
                data: {'playername': $('#search-player').val().trim()},
                dataType: 'json',
                success: function (response) {
                    $('#search-container').find('*').not('#search-player').remove();
                    let ids = '';
                    for (var i = 0; i < response.length; i++) {
                        if (i == response.length - 1)
                            ids += response[i]['id'];
                        else
                            ids += response[i]['id'] + ',';
                    }
                    $('#search-container').append("<cards-container ids='" + ids + "' idsSelected='" + getChosenCards() + "'/>");

                },
                error: function (response) {

                }
            });
        } else {
            $('#search-container').find('*').not('#search-player').remove();
        }
    }

    $('#nextBtn').on('click', function () {
        addCardID();
    });

});

$(document).on('click', ".cardContainer", function () {
    if ($(this).hasClass('clicked-card')) {
        $(this).removeClass('clicked-card');
        searchForCard($(this).parent().attr('playerid'));
        arrayRemove($(this).parent().attr('playerid'));
        addedCards--;
        enableNextButton();
        console.log(getChosenCards());
    }

    else {
        if(chosenCards.length < $('#packSize').val()) {
            $(this).addClass('clicked-card');
            $('#chosen-cards').append($(this).parent().parent().clone().css('pointer-events', 'none'));
            $("#chosen-cards").find("*").css({"border": "none"});
            chosenCards.push($(this).parent().attr('playerid'));
            addedCards++;
            enableNextButton();
            console.log(getChosenCards());
        }
    }
});

function searchForCard(card_id) {
    var cards = $('#chosen-cards').children().children();
    for (var i = 0; i < cards.length; i++) {
        if (card_id == cards[i].getAttribute("playerid")) {
            $(cards[i].parentNode).remove();
            return true;
        }
    }
    return false;
}

function getChosenCards() {
    var str = "";
    for (var i = 0; i < chosenCards.length; i++) {
        if (i === chosenCards.length - 1)
            str += chosenCards[i];
        else
            str += chosenCards[i] + ',';
    }
    return str;
}

function arrayRemove(value) {
    chosenCards.splice($.inArray(value, chosenCards), 1);
}

function enableNextButton() {
    if ($('#packSize').val() == addedCards)
        $('#nextBtn').prop('disabled', false);
    else
        $('#nextBtn').prop('disabled', true);
}

function addCardID() {
    for (var i = 0; i < chosenCards.length; i++) {
        $('#submitForm').append('<input type="hidden" name="card[]" value="' + chosenCards[i] + '">')
    }
}