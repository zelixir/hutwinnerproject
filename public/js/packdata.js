// JavaScript for disabling form submissions if there are invalid fields
'use strict';
window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    let forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    let validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
            }
            form.classList.add('was-validated');
        }, false);
    });
}, false);

// ensures that the probability entered is in the format xx.xxxx
$('.probability-input').on('input', function () {
    let regex = new RegExp("^\\d{0,2}(\\.\\d{1,4})?$");
    if (this.value !== '') {
        if (!regex.test(this.value))
            this.value = this.defaultValue;
    }
});