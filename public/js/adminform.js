// JavaScript for disabling form submissions if there are invalid fields
'use strict';
window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    let forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    let validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
            }
            form.classList.add('was-validated');
        }, false);
    });
}, false);

// Validation to compare the password with the confirmed password
$('[data-pass-conf]').bind('input', function() {
    let to_confirm = $(this);
    let to_equal = $('#' + to_confirm.data('passConf'));

    if(to_confirm.val() !== to_equal.val()) {
        let error_msg = 'Password must be equal';
        document.getElementById("password-error").innerHTML = error_msg;
        this.setCustomValidity(error_msg);
    } else {
        document.getElementById("password-error").innerHTML = "Invalid password";
        this.setCustomValidity('');
    }
});