$(document).ready(function () {
    let bloodhound = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/search/packs?name=%QUERY%',
            wildcard: '%QUERY%'
        },
    });

    $('#searchInput').typeahead({
        hint: true,
        highlight: true,
        minLength: 3
    }, {
        name: 'name',
        source: bloodhound,
        display: function (data) {
            return data.name  //Input value to be set when you select a suggestion.
        },
        templates: {
            empty: [
                '<div class="list-group search-results-dropdown"><div class="list-group-item">Nothing found.</div></div>'
            ],
            header: [
                '<div class="list-group search-results-dropdown">'
            ],
            suggestion: function (data) {

                let name;
                let card;
                if(Array.isArray(data)){
                    name =data[0].name;
                    card=data[0].id;
                }
                else{
                    name = data.name;
                    card=data.id;
                }
                let x = '/img/hutbd-pack-icons/x7.jpg.pagespeed.ic.8-NtslfRji.jpg';
                let y= "<a href='/packs/display/"+card+"' style='border: none;'>" +"<div style='font-weight:normal; color: #000; width: 276px;' class='list-group-item'>"
                    + "<img src='" + x + "' style='height: 80px; width: 68px; background-size: cover;' '>" +
                    "</img>"
                    + "<div style='margin-left: 30%;'>" +
                    name +
                    "</div>"
                    + "<input type='hidden' value=' " + card +" ' class='card-value'>"+'</div>'+ "</a>";
                return y;
            }
        }
    });
});