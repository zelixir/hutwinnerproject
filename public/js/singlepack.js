// JavaScript for disabling form submissions if there are invalid fields
'use strict';
window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    let forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    let validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
            }
            form.classList.add('was-validated');
        }, false);
    });
}, false);

// ensures that the probability entered is in the format xx.xxxx
$('.probability-input').on('input', function () {
    let regex = new RegExp("^\\d{0,2}(\\.\\d{1,4})?$");
    if (this.value !== '') {
        if (!regex.test(this.value))
            this.value = this.defaultValue;
    }
});

$('.price-input').on('input', function () {
    if (this.value < 0)
        this.value = this.defaultValue;
});

// on remove btn click, delete row
$('#cardList').on('click', '.card-remove-btn', function() {
    let classname = $(this).closest ('tr').attr('id');
    let id = classname.split('-')[1];

    // append hidden removed card id
    $('#cardList').find('#hidden-input-data').append(`<input type="hidden" name="removedCard[]" value="${id}">`);

    $(this).closest ('tr').remove ();
});

// on page load hide the data not found row
$( document ).ready(function() {
    $('#cards-search-notfound').hide();
});

// search function for add card(s)
function search() {
    let inputVal = $('#nameInput').val().toUpperCase();
    let rows = $('#cardsTable').children('tbody').children('tr');
    let found = 0;

    for (let i = 0; i < rows.length; i++) {
        let td = $(rows[i]).find('.player-name');
        if (td) {
            let textVal = $(td).text().toUpperCase();
            if (textVal.indexOf(inputVal) > -1) {
                $(rows[i]).show();
                found++;
            } else {
                $(rows[i]).hide();
            }
        }
    }

    if (found === 0)
        $('#cards-search-notfound').show();
}

// display the number of checkbox checked
$(".select-card-chkbox").change(function() {
    $('#card-select-count').html($(":checkbox:checked").length || '');
});

// add row to table
function addRow() {
    $('input[name^="ids"]:checked').each(function() {
        let id = $(this).val();

        // checks if card is already in the pack
        if (!$(`#cardView-${id}`).length) {
            let htmlContent = $(`#card-${id}`).clone();
            $(htmlContent).find('.input-id').remove();
            $(htmlContent).append(`<td><div class="input-group w-100"><input type="number" class="form-control w-75" name="card[${id}]" required><div class="input-group-append w-25"><span class="input-group-text">%</span></div></div></td>`);
            $(htmlContent).append('<td><div class="input-group w-100"><button class="btn btn-danger card-remove-btn">Remove</button></div></td>');
            $('#cardViewTable').find('tbody').append(`<tr id="cardView-${id}">`+ $(htmlContent).html() + '</tr>>');

            // resets the modal
            $('#modal-add-form')[0].reset();
            $('#card-select-count').html('');
        }
    });

    $('.probability-input').on('input', function () {
        let regex = new RegExp("^\\d{0,2}(\\.\\d{1,4})?$");
        if (this.value !== '') {
            if (!regex.test(this.value))
                this.value = this.defaultValue;
        }
    });
}