$(document).ready(function () {
    $('#contactForm').submit(function (event) {
        //Prevent the form from submitting
        event.preventDefault();

        var name = $('#name').val().trim();
        var email = $('#email').val().trim();
        var message = $('#message').val().trim();

        //Check if all the fields are filled before making an Ajax call
        if (name && email && message) {
            //Show the loading ring and disable the 'SEND MESSAGE' button
            var image = $('<img id="loadingRing" src="images/loadingRing.svg" style="height: 50px; width: 50px;">');
            $('#submitBtn').empty().append(image);
            $('#submitBtn').prop('disabled', true);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: event.target.action,
                type: 'post',
                data: $('#contactForm').serialize(),
                dataType: 'json',
                success: function (response) {
                    $('#msgSubmit').empty().append('<p>' + response['result'] + '</p>');
                    $('#submitBtn').prop('disabled', false);
                    $('#submitBtn').empty().append('SEND MESSAGE');
                },
                error: function (response) {
                    $('#msgSubmit').empty().append('<p>Something Went Wrong.</p>');
                    $('#submitBtn').prop('disabled', false);
                    $('#submitBtn').empty().append('SEND MESSAGE');
                }
            });
        }
        return false;
    });
});