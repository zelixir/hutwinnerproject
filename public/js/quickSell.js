let cardContainer;
let container;
let number;
$(document).ready(function () {
    number = $('.card-container').length;
    let modal = $('#myModal')[0];
    let x =  $('.sellNow')
    $('.dropdown-toggle').dropdown()
    $('.close').on('click', function () {
        $('#myModal').modal('hide')
    });
    $('#Confirm').on('click', function () {
        $('#myModal').modal('hide')
    })
    $(window).click(function (e) {
        if (e.target == modal) {
            $('#myModal').modal('hide')
        }
    });

    $('#userForm').submit(function (event) {
        event.preventDefault();

        $.ajax({
            url: event.target.action,
            type: 'post',
            data: $('#userForm').serialize(),
            dataType: 'json',
            success: function (response) {
                cardContainer.remove();
                $('#points').text(response['points']);
                $('#myModal').css('display', 'none');
                $('#success-div').css('display', 'block');
                number--;
                redirect(number);

            },

            error: function (response) {
                $('#myModal').css('display', 'none');
                $('#failed-div').css('display', 'block');
            }

        });
        return false;
    });

});

$(document).on('click', '.sellNow', function () {
    preparePopup($(this).next());
    $('#myModal').modal('show')
    cardContainer = $(this)[0].closest('.card-container');
});

function preparePopup(elementContainer) {
    let x = 0;
    child = elementContainer.children().each(function () {
        if (x == 0) {
            storeCardPrincipalInfo($(this));
        } else {
            storePlayerName($(this));
        }
        x++;
    });

}

function storeCardPrincipalInfo(element) {
    /**
     * Create input hidden field with value of element
     */

    $("#card").val($(element).val());
}

function storePlayerName(element) {

    $('#header').text('Are you sure you want to Quick Sell this item? You are going to receive ' + $(element).val() + ' Points. This action is irreversible');

    //$('#header').text('Are you sure to sell the following card ' + $(element).val());

}

function storeQuantity(element) {
    let ele = $('#quantityCard');
    $(ele).empty();
    for (let i = 1; i <= $(element).val(); i++) {
        $('<option value= " ' + i + ' "  >' + i + '</option>').appendTo($(ele));
    }
}

function redirect(value) {
    if (value === 0) {
        $('.container').append('<h2 style="text-align: center">No more cards to sell</h2>');
    }
}
