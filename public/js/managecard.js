// ensures that only 2 decimal places can be entered
$(':input[type="number"]').keypress(function () {
    let value = $(this).val().split('.');
    return !(value.length > 0 && value[1].length > 1);
});

// ajax call to update the xbox/ps4 price of the card
$(document).on('click', '.price-submit', function () {
    let getUrl = window.location;
    let baseUrl = getUrl.protocol + "//" + getUrl.host;
    let cardId = $(this).val().trim();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: baseUrl + '/admin/card/update',
        type: 'post',
        data: {'cardId': cardId, 'xboxPrice': $(`#xboxVal${cardId}`).val().trim(), 'ps4Price': $(`#ps4Val${cardId}`).val().trim()},
        dataType: 'json',
        success: function (response) {
            if (response.change === 'updated') {
                $.notify({
                    message: "Successfully updated card prices."
                }, {
                    type: 'success',
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutRight'
                    }
                }).show();
            } else if (response.change === 'deleted') {
                $(`#card-${response.cardId}`).hide();
                $.notify({
                    message: "Successfully deleted card."
                }, {
                    type: 'success',
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutRight'
                    }
                }).show();
            } else {
                $.notify({
                    message: "Unknown action."
                }, {
                    type: 'warning',
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutRight'
                    }
                }).show();
            }
        },
        error: function (response) {
            $.notify({
                message: "Unable to update card prices."
            }, {
                type: 'warning',
                animate: {
                    enter: 'animated fadeInUp',
                    exit: 'animated fadeOutRight'
                }
            }).show();
        }

    });
});