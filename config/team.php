<?php


$arr = [
    /*
    |--------------------------------------------------------------------------
    | List of teams
    |--------------------------------------------------------------------------
    */

    'ISE',
    'NHL',
    'RCH',
    'KRL',
    'ONT',
    'BOS',
    'CLE',
    'POR',
    'PIT',
    'THO',
    'VIT',
    'DAL',
    'CBJ',
    'STO',
    'MAN',
    'HAM',
    'QC',
    'BID',
    'WOL',
    'MIL',
    'MED',
    'ADL',
    'SKO',
    'SAR',
    'STL',
    'LEH',
    'IOW',
    'WSH',
    'TOR',
    'NYI',
    'BES',
    'BRI',
    'SCL',
    'BRY',
    'PIR',
    'BER',
    'BRN',
    'PAR',
    'SJB',
    'HAR',
    'MIN',
    'VER',
    'TUC',
    'MUN',
    'AMP',
    'KOL',
    'KRE',
    'COL',
    'SDG',
    'CAR',
    'LAK',
    'NJD',
    'ANA',
    'PHI',
    'LAU',
    'BAN',
    'TUR',
    'HER',
    'WPG',
    'AIS',
    'DET',
    'EHC',
    'RAJ',
    'TYG',
    'BAR',
    'CHI',
    'MNS',
    'LIN',
    'TEX',
    'MIS',
    'ROC',
    'DAV',
    'LAV',
    'GRA',
    'MAL',
    'HFX',
    'SJS',
    'ZSC',
    'SYR',
    'PRO',
    'GES',
    'EV',
    'JYV',
    'AUG',
    'SPA',
    'BUF',
    'SPT',
    'FA',
    'VAN',
    'FRG',
    'DJU',
    'ARZ',
    'OTT',
    'MTL',
    'VAX',
    'VGK',
    'UTI',
    'HV',
    'EDM',
    'FWK',
    'BAK',
    'WBS',
    'LUG',
    'CGY',
    'CHW',
    'CHA',
    'FRO',
    'NSH',
    'NYR',
    'RBS',
    'SKE',
    'ING',
    'FLA',
    'TBL'
];

sort($arr);

return $arr;