<?php


return [
    /*
    |--------------------------------------------------------------------------
    | List of navbar links
    |--------------------------------------------------------------------------
    */

    [
        'name' => 'Transactions',
        'icon' => 'fas fa-file-invoice-dollar',
        'route' => 'admin.transactions',
        'adminlevel' => 3,
        'description' => 'List of transactions.'
    ],
    [
        'name' => 'Withdrawals',
        'icon' => 'fas fa-file-invoice',
        'route' => 'admin.withdraws',
        'adminlevel' => 3,
        'description' => 'List of withdraws.'
    ],
    [
        'name' => 'Create Pack',
        'icon' => 'fas fa-plus-square',
        'route' => 'admin.createpack',
        'adminlevel' => 1,
        'description' => 'Form to create a pack.'
    ],
    [
        'name' => 'Manage Packs',
        'icon' => 'fas fa-plus-square',
        'route' => 'admin.displaypacks',
        'adminlevel' => 1,
        'description' => 'List of packs. Admins can edit and add new cards to the pack.'
    ],
    [
        'name' => 'Create Card',
        'icon' => 'fas fa-plus-square',
        'route' => 'admin.createcard',
        'adminlevel' => 1,
        'description' => 'Form to create a card.'
    ],
    [
        'name' => 'Manage Cards',
        'icon' => 'fas fa-plus-square',
        'route' => 'admin.displaycard',
        'adminlevel' => 1,
        'description' => 'List of cards. Admins can edit and change the price of the card.'
    ],
    [
        'name' => 'Users',
        'icon' => 'fas fa-users',
        'route' => 'admin.users',
        'adminlevel' => 1,
        'description' => 'List of users.'
    ],
    [
        'name' => 'Admins',
        'icon' => 'fas fa-users',
        'route' => 'admin.list',
        'adminlevel' => 1,
        'description' => 'List of admins.'
    ],
    [
        'name' => 'Create Admin',
        'icon' => 'fas fa-user-plus',
        'route' => 'admin.form',
        'adminlevel' => 1,
        'description' => 'Form to create admins.'
    ]
];