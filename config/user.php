<?php


return [
    /*
    |--------------------------------------------------------------------------
    | Default Coin
    |--------------------------------------------------------------------------
    |
    | Specifies the default coin balance when user is created
    |
    */

    'default_coins' => 0,
];