<?php


$arr = [
    /*
    |--------------------------------------------------------------------------
    | List of leagues
    |--------------------------------------------------------------------------
    */

    'NHL',
    'AHL',
    'ECHL',
    'CHL',
    'ELH',
    'DEL',
    'OHL',
    'Allsvenska',
    'SHL',
    'NL',
    'Liiga',
    'QMJHL',
    'EBEL',
    'WHL'
];

sort($arr);

return $arr;