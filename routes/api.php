<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\InputFields;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\WebProfile;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('create-payment', function (Request $request){
    function getBaseUrl()
    {
        // output: /myproject/index.php
        $currentPath = $_SERVER['PHP_SELF'];

        // output: Array ( [dirname] => /myproject [basename] => index.php [extension] => php [filename] => index )
        $pathInfo = pathinfo($currentPath);

        // output: localhost
        $hostName = $_SERVER['HTTP_HOST'];

        // output: http://
        $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https://'?'https://':'http://';

        // return: http://localhost/myproject/
        return $protocol.$hostName.$pathInfo['dirname']."/";
    }

    $apiContext = new \PayPal\Rest\ApiContext(
        new \PayPal\Auth\OAuthTokenCredential(
            env('PAYPAL_CLIENT'),
            env('PAYPAL_SECRET')
        )
    );

    $apiContext->setConfig(
        array(
            'log.LogEnabled' => true,
            'log.FileName' => 'PayPal.log',
            'log.LogLevel' => 'DEBUG',
            'mode' => 'live'
        )
    );

    $payer = new Payer();
    $payer->setPaymentMethod("paypal");

    $item1 = new Item();
    $item1->setName($request->item)
        ->setCurrency('USD')
        ->setQuantity(1)
        ->setPrice($request->price);

    $itemList = new ItemList();
    $itemList->setItems(array($item1));

    $amount = new Amount();
    $amount->setCurrency("USD")
        ->setTotal($request->price);

    $transaction = new Transaction();
    $transaction->setAmount($amount)
        ->setItemList($itemList)
        ->setDescription("Payment description")
        ->setInvoiceNumber(uniqid());

    $baseUrl = getBaseUrl();
    $redirectUrls = new RedirectUrls();
    $redirectUrls->setReturnUrl("$baseUrl/purchase")
        ->setCancelUrl("$baseUrl/purchase");

    $inputFields = new InputFields();
    $inputFields->setNoShipping(1);

    $webProfile = new WebProfile();
    $webProfile->setName('test' . uniqid())->setInputFields($inputFields);

    $webProfileId = $webProfile->create($apiContext)->getId();

    $payment = new Payment();
    $payment->setExperienceProfileId($webProfileId);
    $payment->setIntent("sale")
        ->setPayer($payer)
        ->setRedirectUrls($redirectUrls)
        ->setTransactions(array($transaction));

    try {
        $payment->create($apiContext);
    } catch (\PayPal\Exception\PayPalConnectionException $ex) {
        echo $ex->getData();
        exit(1);
    } catch (Exception $ex) {
        echo $ex;
        exit(1);
    }

    return $payment;

});

Route::post('execute-payment', function (Request $request){
    $apiContext = new \PayPal\Rest\ApiContext(
        new \PayPal\Auth\OAuthTokenCredential(
            env('PAYPAL_CLIENT'),
            env('PAYPAL_SECRET')
        )
    );

    $apiContext->setConfig(
        array(
            'log.LogEnabled' => true,
            'log.FileName' => 'PayPal.log',
            'log.LogLevel' => 'DEBUG',
            'mode' => 'live'
        )
    );

    $paymentId = $request->paymentID;
    $payment = Payment::get($paymentId, $apiContext);

    $execution = new PaymentExecution();
    $execution->setPayerId($request->payerID);

    try{
        $result = $payment->execute($execution, $apiContext);
    } catch (\PayPal\Exception\PayPalConnectionException $ex) {
        echo $ex->getData();
        exit(1);
    } catch(Exception $ex){
        echo $ex;
        exit(1);
    }

    /*$user = auth('api')->user();
    $userId = $user->id;
    //$user = \App\User::find($userId);

    try{
        \App\Transaction::create(
            [
                'user_id'=> $userId,
                'payment_id'=> 1,
                'number' => 1,
                'amount' => $request->amount,
                'description' => $request->description
            ]
        );
        $user->coins = $user->coins + $request->coins;
        $user->save();
    } catch(ErrorException $ex){
        echo $ex->getMessage();
    }*/

    return $result;
});