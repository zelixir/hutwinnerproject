<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('xsolla' , 'WebHookController@index')->name('xsolla');


Route::get('/about', function () {
    return view('about');
})->name('about');
Route::get('/privacy', function () {
    return view('privacy');
})->name('privacy');
Route::get('/terms', function () {
    return view('terms');
})->name('terms');
Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Route::get('/cards', function () {
    return view('cards_example');
});


Route::get('/purchase', 'TransactionController@show')->name('payment');


Route::get('/faq', function () {
    return view('faq');
})->name('faq');
Route::get('/verification',function(){
   return view('verification');
})->name('remove.deposit')->middleware(\App\Http\Middleware\Authenticate::class);
Auth::routes();

Route::prefix('user')->group(function () {
    Route::get('/', 'UserController@profile')->name('profile');
    Route::get('/inventory/cards', 'UserController@cards')->name('home')->middleware(\App\Http\Middleware\ClearCache::class);
    Route::get('/inventory/packs', 'UserController@packs')->name('user.packs')->middleware(\App\Http\Middleware\ClearCache::class);
    Route::get('/inventory/open', function () {
        return view('inventory');
    })->middleware(\App\Http\Middleware\ClearCache::class);
    Route::post('/inventory/open', 'OpenController@index')->middleware(\App\Http\Middleware\ClearCache::class)->name('user.open');
    Route::post('/inventory/sell', 'SellController@sell')->name('user.sell');
    Route::post('/inventory/quicksell', 'SellController@quickSell')->name('user.quickSell');
    Route::get('/inventory/withdraw', 'WithdrawController@index')->name('user.withdraw');
    Route::get('/transactions', 'UserController@transactions')->name('user.transactions');
    Route::get('/withdraws', 'UserController@withdraws')->name('user.withdraws');
    Route::post('/inventory/withdraw', 'WithdrawController@withdraw')->name('user.post.withdraw');
    Route::post('/updateInfo', 'UserController@updateInfo')->name('user.update');
});

Route::post('/lastTransactions', 'UserController@lastTransactions');

Route::get('/users/logout', 'Auth\LoginController@userLogout')->name('user.logout');
Route::post('/cardinfo', 'PlayerInfoController@getInfo');
Route::post('/manycardsinfo', 'PlayerInfoController@getInfoPlayers');


Route::prefix('packs')->group(function () {
    Route::get('/display/{id}', 'MarketPlaceController@display')->name('packs.displaySingle');
    Route::get('/', 'MarketPlaceController@index')->name('packs.marketplace');
    Route::get('/custom','MarketPlaceController@customPacks')->name('packs.custom.marketplace');
    Route::post('/buy', 'BuyController@buy')->name('packs.purchase');
    Route::get('/buy', function () {
        return redirect()->back();
    });
    Route::get('/create', 'CreatePackController@pack')->name('pack');
    Route::get('/create/{num}', 'CreatePackController@packSize')->name('pack.chooseCards');
    Route::post('/create/{num}/probability', 'CreatePackController@probabilityCards')->name('pack.probability');
    Route::get('/create/{num}/probability', 'CreatePackController@wrongProbabilities')->name('pack.get.probability');
    Route::post('/create/{num}/overview', 'CreatePackController@overviewPack')->name('pack.overview');
    Route::post('/create/submit/{num}', 'CreatePackController@store')->name('pack.store');
    Route::get('/console', 'MarketPlaceController@console')->name('pack.console');
});
Route::prefix('admin')->group(function () {
    Route::get('/login', 'Auth\AdminLoginController@loginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/', 'Admin\AdminController@index')->name('admin.dashboard');
    Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');

    //Routes for transactions
    Route::get('/transactions', 'Admin\AdminTransactionController@getTransactions')->name('admin.transactions');
    Route::get('/transaction/{id}', 'Admin\AdminTransactionController@getTransactionById')->name('admin.transaction');
    Route::post('/transaction/add/note', 'Admin\AdminTransactionController@addNote');
    Route::get('/transaction/add/note', function () {
        return redirect()->back();
    });

    //Routes for withdraws
    Route::get('/withdraws', 'Admin\AdminWithdrawController@getWithdraws')->name('admin.withdraws');
    Route::get('/withdraw/{id}', 'Admin\AdminWithdrawController@getWithdrawById')->name('admin.withdraw');
    Route::post('/withdraw/manage', 'Admin\AdminWithdrawController@handleWithdraw');
    Route::get('/withdraw/manage', function () {
        return redirect()->back();
    });
    Route::post('/withdraw/add/note', 'Admin\AdminWithdrawController@addNote');
    Route::get('/withdraw/add/note', function () {
        return redirect()->back();
    });

    //Routes for admin
    Route::get('/form', 'Admin\AdminController@adminForm')->name('admin.form');
    Route::post('/create', 'Admin\AdminController@createAdmin');
    Route::get('/create', function () {
        return redirect()->back();
    });
    Route::get('/list', 'Admin\AdminController@displayAdmins')->name('admin.list');
    Route::get('/users', 'Admin\AdminUserController@displayUsers')->name('admin.users');
    Route::get('/users/{id}', 'Admin\AdminUserController@getUserById')->name('admin.user');
    Route::post('/users/modify', 'Admin\AdminUserController@modifyUserStatus');
    Route::get('/users/modify', function () {
        return redirect()->back();
    });
    Route::post('/users/add/note', 'Admin\AdminUserController@addNote');
    Route::get('/users/add/note', function () {
        return redirect()->back();
    });

    //Routes for packs
    Route::get('/pack', 'Admin\CardPackController@displayPacks')->name('admin.displaypacks');
    Route::get('/pack/view/{id}', 'Admin\CardPackController@singlePackView')->name('admin.packview');
    Route::get('/pack/form', 'Admin\CardPackController@packForm')->name('admin.createpack');
    Route::post('/pack/create', 'Admin\CardPackController@createPack');
    Route::get('/pack/create', function () {
        return redirect()->back();
    });
    Route::post('/pack/search', 'Admin\CardPackController@packFind');
    Route::get('/pack/search', function () {
        return redirect()->back();
    });
    Route::post('/pack/modify', 'Admin\CardPackController@modifyPack');
    Route::get('/pack/modify', function () {
        return redirect()->back();
    });

    //Routes for cards
    Route::get('/card', 'Admin\CardPackController@displayCards')->name('admin.displaycard');
    Route::get('/card/form', 'Admin\CardPackController@cardForm')->name('admin.createcard');
    Route::get('/card/create', function () {
        return redirect()->back();
    });
    Route::post('/card/create', 'Admin\CardPackController@createCard');
    Route::post('/card/update', 'Admin\CardPackController@updateCardPrice');
    Route::get('/card/update', function () {
        return redirect()->back();
    });

    //Routes to reset Passwords
    Route::post('/password/email', 'Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::get('/password/reset', 'Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::post('/password/reset', 'Auth\AdminResetPasswordController@reset');
    Route::get('/password/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');
});


Route::prefix('search')->group(function () {
    Route::get('/cards', 'CardSearchController@search')->name('search.card');
    Route::get('/packs', 'PackSearchController@search')->name('search.pack');
});

Route::post('/contact', 'ContactMessageController@store')->name('contact.message');

Route::post('/paypalPayement', 'TransactionController@insertTransaction')->name('payment.paypal');

