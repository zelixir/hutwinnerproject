<html xmlns="http://www.w3.org/1999/html">
<head>
    <!-- META TAGS -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- OG meta tags that improve the look of your post on social media -->
    <meta property="og:site_name" content=""/><!--website name-->
    <meta property="og:site" content=""/> <!--website link-->
    <meta property="og:title" content=""/> <!--Post title-->
    <meta property="og:description" content=""/> <!--Post description-->
    <meta property="og:image" content=""/><!-- Image link (jpg only)-->
    <meta property="og:url" content=""/> <!--where do you want your post to link to-->
    <meta property="og:type" content="article"/>
    <link rel="apple-touch-icon" sizes="57x57" href="images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <title>HUTWINNER</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/MarketPlace.css') }}">
    <link rel="stylesheet" href="{{ asset('css/2019_HUT_Style.css')}}">
    <link rel="stylesheet" href="{{ asset('css/nav.css')}}">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="/css/floatingSupport.css" rel="stylesheet">
    <script src="https://cdn.rawgit.com/webcomponents/webcomponentsjs/v0.7.24/webcomponents-lite.js"></script>
    <style>
        .navbar, #footer {
            background: #20202A !important;
        }

        .row-Container {
            background: #30303F !important;
            border-radius: 15px 15px 15px 15px;
        }

        .elementInRow {
            border-radius: 15px 15px 15px 15px;
        }

        .containerForElements {
            border-radius: 15px 15px 15px 15px;
            background: #363647 !important;
            -webkit-box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.3);
            box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.3);
        }

        img {
            border-radius: 15px 15px 15px 15px;
            -webkit-box-shadow: 0 35px 35px 0 rgba(0, 0, 0, 0.3);
            box-shadow: 0 30px 35px 0 rgba(0, 0, 0, 0.3);
        }

        @media (min-width: 1200px) {
            .container {
                max-width: 90%;
            }
        }
    </style>
</head>
<body style="position: relative;  background: #202033 !important;">
<header id="main-header">
    @component('components.nav', ['header'=> 'Custom ', 'coloredHeader' => 'Pack'])
    @endcomponent
</header>
<div id="container">



        <div class="container mt-5 containerForElements" style="position: relative; padding-bottom: 70px">
            <h1 class="animation-element slide-down marketPlaceTitle headerMarketplace"><span class="colored">SELECT YOUR PACK SIZE</span>
            </h1>
            @if(Session::has('success'))
                <div id="success" class="alert alert-success">
                    {!!  Session::get('success') !!}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger">
                    <p>{{ $errors->first() }}</p>
                </div>
            @endif

            <div class="row d-flex justify-content-center ">
                <div class="col-md-4 col-lg-3 col-sm-6 col-xl-2 elementInRow">
                    <a href="{{route('pack.chooseCards', 4)}}"><img src="/images/4cards.png" class="mt-5"
                                                                    style="width: 90%"></a>
                </div>

                <div class="col-md-4 col-lg-3 col-sm-6 col-xl-2 elementInRow">
                    <a href="{{route('pack.chooseCards', 8)}}"><img src="/images/8cards.png" class="mt-5"
                                                                    style="width: 90%"></a>
                </div>

                <div class="col-md-4 col-lg-3 col-sm-6 col-xl-2 elementInRow">
                    <a href="{{route('pack.chooseCards', 12)}}"><img src="/images/12cards.png" class="mt-5"
                                                                     style="width: 90%"></a>
                </div>

                <div class="col-md-4 col-lg-3 col-sm-6 col-xl-2 elementInRow">
                    <a href="{{route('pack.chooseCards', 16)}}"><img src="/images/16cards.png" class="mt-5"
                                                                     style="width: 90%"></a>
                </div>

                <div class="col-md-4 col-lg-3 col-sm-6 col-xl-2 elementInRow">
                    <a href="{{route('pack.chooseCards', 20)}}"><img src="/images/20cards.png" class="mt-5"
                                                                     style="width: 90%"></a>
                </div>
            </div>

    </div>
        @component('components.support')
        @endcomponent
    @component('components.footer')
    @endcomponent

</div>

<script src="../../js/app.js"></script>
<script src="/js/jquery-3.2.1.min.js"></script>
<script src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>
<script src="/js/search.js"></script>
<script src="/js/floatingSupport.js"></script>
<script>
    $(".alert").delay(4000).slideUp(200, function () {
        $(this).alert('close');
    });
</script>
</body>
</html>
