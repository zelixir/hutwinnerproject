<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <title>HUTWINNER</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <!-- OG meta tags that improve the look of your post on social media -->
    <meta property="og:site_name" content=""/><!--website name-->
    <meta property="og:site" content=""/> <!--website link-->
    <meta property="og:title" content=""/> <!--Post title-->
    <meta property="og:description" content=""/> <!--Post description-->
    <meta property="og:image" content=""/><!-- Image link (jpg only)-->
    <meta property="og:url" content=""/> <!--where do you want your post to link to-->
    <meta property="og:type" content="article"/>
    <link rel="apple-touch-icon" sizes="57x57" href="images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('css/MarketPlace.css') }}">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <link href="/css/font-awesome.min.css" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('css/purchase.css') }}">
    <link rel="stylesheet" href="{{asset('css/nav.css')}}">
    <link href="/css/floatingSupport.css" rel="stylesheet">
    <script src="https://cdn.rawgit.com/webcomponents/webcomponentsjs/v0.7.24/webcomponents-lite.js"></script>
    <style>
        .link-MarketPlace {
            text-align: center;
            font-style: italic;
            font-size: 68px !important;
            letter-spacing: 2.05px;
            text-transform: uppercase;
            font-weight: 400;
            font-family: Steelfish;
        }

        img {
            vertical-align: middle;
            border-style: none;
            width: 100%;
        }

        .btn-light {
            background-color: #F6EA9C;
            color: #000;
            font-style: italic;
            border: unset;
        }

        h1 {
            font-family: unset;
        }

        a:visited {
            color: #F6EA9C;
        }

        img {
            width: 100%;
            height: 100%;
        }

        .element-Container {
            background: #30303F !important;
            border-bottom-left-radius: unset !important;
            border-bottom-right-radius: unset !important;
            border-top-left-radius: 20px;
            border-top-right-radius: 20px;
        }

        /*ml-5 columnValue*/
        .element-ContainerSecond {
            padding-left: 5%;
            background: #30303F !important;
            border: 2px solid #F6EA9C !important;;
            -webkit-border-radius: 20px;
            -moz-border-radius: 20px;
            border-radius: 20px;
        }

        a {
            color: #F6EA9C;
        }

        @media (min-width: 1200px) {
            .container {
                max-width: 95%;
            }
        }

        body .containerResized {
            background: #222 !important;
        }

        .navbar {
            background: #20202A !important;
        }

        .main-container {
            background: #363647 !important;
            border-radius: 15px 15px 15px 15px;
            -webkit-box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.3);
            box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.3);
        }

        .card-img-top {
            background: url(/img/hutbd-pack-icons/x7.jpg.pagespeed.ic.8-NtslfRji.jpg);
            background-repeat: no-repeat;
            -webkit-background-size: cover;
            background-size: cover;
            height: 300px;
            width: 300px;
            display: inline-block;
        }

        .columnValue {
            min-height: 342px;
            min-width: 242px;
            padding: unset;
        }

        .clickedCard {
            border: 5px solid #F6EA9C;
            border-radius: 10px;
        }

        .show{
            padding: 0 !important;
        }
    </style>
</head>
<body style="position: relative; background: #202033 !important;">
<header id="main-header">
    @component('components.nav',['header' => 'Payment','coloredHeader' => ''])
    @endcomponent
</header>
<div id="container">
    <div class="container main-container">
        <h1 class="paymentTitle"><span class="colored">CHOOSE PACKAGE</span></h1>
        <div style="width: 100%; text-align: center; padding-bottom: 10px">
            <a class="how-to-etransfer" data-toggle="modal" data-target="#exampleModal" href="" >How to purchase using E-transfer?</a>
        </div>
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Payment</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Please send a payment to admin@Hutwinner.com with the note attached as your HUTWinner username.
                    </div>
                    <div class="modal-footer">
                        <form action="{{ route('packs.purchase') }}" method="post">
                            @csrf
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <div id="failed" class="alert alert-danger" style="display: none;">
            <p>Transaction Failed</p>
        </div>
        <div class="row  justify-content-center coinsList">
            <div class="col-sm-6 col-lg-2  col-md-3 d-flex justify-content-center mt-3 mb-5">
                <div class="buyCoinsContainer">
                    <div class="buyCoinsTop">
                        <img src="/images/coins.png" alt="coins" class="buyCoinsImage">
                    </div>
                    <div class="buyCoinsBottom">
                        <div class="buyCoinsPoints">5,820 POINTS</div>
                        <div class="buyCoinsPrice">$12.80 USD</div>
                        <div class="buyCoinsBtns">
                            @if($option1)
                                <div id="paypal-2000pts"></div>
                            @else
                                <div style="border: 1px solid white; width: 80%; text-align: center; border-radius: 10px">
                                    Daily Limit Reached
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-2 col-md-3 d-flex justify-content-center mt-3 mb-5">
                <div class="buyCoinsContainer">
                    <div class="buyCoinsTop">
                        <img src="/images/coins.png" alt="coins" class="buyCoinsImage">
                    </div>
                    <div class="buyCoinsBottom">
                        <div class="buyCoinsPoints">14,550 POINTS</div>
                        <div class="buyCoinsPrice">$32 USD</div>
                        <div class="buyCoinsBtns">
                            @if($option2)
                                <div id="paypal-4500pts"></div>
                            @else
                                <div style="border: 1px solid white; width: 80%; text-align: center; border-radius: 10px">
                                    Daily Limit Reached
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-2 col-md-3 d-flex justify-content-center mt-3 mb-5">
                <div class="buyCoinsContainer">
                    <div class="buyCoinsTop">
                        <img src="/images/coins.png" alt="coins" class="buyCoinsImage">
                    </div>
                    <div class="buyCoinsBottom">
                        <div class="buyCoinsPoints">29,100 POINTS</div>
                        <div class="buyCoinsPrice">$64 USD</div>
                        <div class="buyCoinsBtns">
                            @if($option3)
                                <div id="paypal-9000pts"></div>
                            @else
                                <div style="border: 1px solid white; width: 80%; text-align: center; border-radius: 10px">
                                    Daily Limit Reached
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-2 col-md-3 d-flex justify-content-center mt-3 mb-5">
                <div class="buyCoinsContainer">
                    <div style="height: 50%;margin-bottom: 5px;margin-top: 5px">
                        <img src="/images/coins.png" alt="coins" class="buyCoinsImage">
                    </div>
                    <div class="buyCoinsBottom">
                        <div class="buyCoinsPoints">58,200 POINTS</div>
                        <div class="buyCoinsPrice">$128 USD</div>
                        <div class="buyCoinsBtns">
                            @if($option4)
                                <div id="paypal-22500pts"></div>
                            @else
                                <div style="border: 1px solid white; width: 80%; text-align: center; border-radius: 10px">
                                    Daily Limit Reached
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <?php
            use Xsolla\SDK\API\XsollaClient;
            use Xsolla\SDK\API\PaymentUI\TokenRequest;

            $tokenRequest = new TokenRequest((int)env('PROJECT_ID'), '1');
            $tokenRequest->setUserName(\Illuminate\Support\Facades\Auth::user()->firstname . ' ' . \Illuminate\Support\Facades\Auth::user()->lastname)
                ->setUserEmail(\Illuminate\Support\Facades\Auth::user()->email . '')
                ->setSandboxMode(true);

            $xsollaClient = XsollaClient::factory(array(
                'merchant_id' => (int)env('MERCHANT_ID'),
                'api_key' => env('API_KEY')
            ));
            $response = $xsollaClient->ListPaymentAccounts(array(
                'project_id' => (int)env('PROJECT_ID'),
                'user_id' => \Illuminate\Support\Facades\Auth::id() . ''
            ));
            $token = $xsollaClient->createPaymentUITokenFromRequest($tokenRequest);
            \Xsolla\SDK\API\PaymentUI\PaymentUIScriptRenderer::send($token, $isSandbox = true)
            ?>
            <div class="col-sm-6 col-lg-2 col-md-3 d-flex justify-content-center mt-3 mb-5">
                <div class="buyCoinsContainer">
                    <div style="height: 50%;margin-bottom: 5px;margin-top: 5px">
                        <img src="/images/coins.png" alt="coins" class="buyCoinsImage">
                    </div>
                    <div class="buyCoinsBottom">
                        <div class="buyCoinsPoints">PAY THROUGH XSOLLA</div>
                        <div class="buyCoinsPrice"></div>
                        <div class="buyCoinsBtns">
                            @if($option5)
                                <button data-xpaystation-widget-open class="purchaseButton btn">XSOLLA</button>
                            @else
                                <div style="border: 1px solid white; width: 80%; text-align: center; border-radius: 10px">
                                    Daily Limit Reached
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <?php \Xsolla\SDK\API\PaymentUI\PaymentUIScriptRenderer::send($token, $isSandbox = true); ?>
        </div>
    </div>
    @component('components.support')
    @endcomponent
    @component('components.footer')
    @endcomponent
</div>
<script src="/js/jquery-3.2.1.min.js"></script>
<script src="https://www.paypalobjects.com/api/checkout.js"></script>
<script src="/../../js/app.js"></script>

<script src="/js/purchase.js"></script>

<script src="/js/search.js"></script>

<script src="/js/floatingSupport.js"></script>

<script src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>
</body>
</html>