<!doctype html>
<html>
<head>
    <!-- META TAGS -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- WEBSITE TITLE & DESCRIPTION -->
    <title>HUTWINNER</title>
    <!-- OG meta tags that improve the look of your post on social media -->
    <meta property="og:site_name" content=""/><!--website name-->
    <meta property="og:site" content=""/> <!--website link-->
    <meta property="og:title" content=""/> <!--Post title-->
    <meta property="og:description" content=""/> <!--Post description-->
    <meta property="og:image" content=""/><!-- Image link (jpg only)-->
    <meta property="og:url" content=""/> <!--where do you want your post to link to-->
    <meta property="og:type" content="article"/>
    <link rel="apple-touch-icon" sizes="57x57" href="images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="{{ asset('css/2019_HUT_Style.css') }}" rel="stylesheet">
    <!-- Favicon and Apple Icons -->

    <!-- STYLES -->
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- FontAwesome -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <!-- Animations -->
    <link href="css/animations.css" rel="stylesheet">
    <!-- Lightbox -->
    <link href="css/lightbox.min.css" rel="stylesheet">
    <!-- Video Lightbox -->
    <link href="css/modal-video.min.css" rel="stylesheet">
    <!-- Main Stylesheet -->
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/nav.css')}}">
    <link href="css/floatingSupport.css" rel="stylesheet">

    <script src="https://cdn.rawgit.com/webcomponents/webcomponentsjs/v0.7.24/webcomponents-lite.js"></script>

    <style>
        html {

        }

        .col-12 > .descriptionText {
            font-size: 1.5rem;
            margin-top: 5%;
            text-align: center;
        }

        body .containerResized {
            background: #222 !important;
        }
        .imgValue{
            border-radius: 15px 15px 15px 15px;
            -webkit-box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.3);
            box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.3);
            border: 2px solid #F6EA9C;
        }
        .main-container {
            background: #363647 !important;
            border-radius: 15px 15px 15px 15px;
            -webkit-box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.3);
            box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.3);
            margin-bottom: 120px !important;
        }

        @media (min-width: 1200px) {
            .container {
                max-width: 95%;
            }
        }

        .large-margin {
            background: #30303F !important;
            border-radius: 15px 15px 15px 15px;
            -webkit-box-shadow: 5px 30px 30px 5px rgba(0, 0, 0, 0.2);
            box-shadow: 5px 30px 30px 5px rgba(0, 0, 0, 0.2);
            margin-bottom: 4em !important;
        }

        .searchbar {
            margin-bottom: auto;
            margin-top: auto;
            height: 60px;
            background-color: rgba(0, 0, 0, 0.1);
            border-radius: 30px;
            padding: 10px;
        }

        .search_input {
            color: white;
            border: 0;
            outline: 0;
            background: none;
            width: 0;
            caret-color: transparent;
            line-height: 40px;
            transition: width 0.4s linear;
            border: none !important;
        }

        .descriptionText {
            font-size: 1.4rem;
            margin-top: 13%;
        }

        .searchbar:hover > .search_input {
            padding: 0 10px;
            width: 350px;
            caret-color: red;
            transition: width 0.4s linear;
        }

        .searchbar:hover > .search_icon {
            background: white;
            color: #e74c3c;
        }

        .search_icon {
            height: 40px;
            width: 40px;
            float: right;
            display: flex;
            justify-content: center;
            align-items: center;
            border-radius: 50%;
            color: white;
        }

        @import url(https://fonts.googleapis.com/css?family=Roboto:100,200,300,400,500);

    </style>

</head>
<body style="position: relative; background: #202033 !important;">
<!-- Loading Screen -->
<div id="loader-wrapper">
    <h1 class="loader-logo"><span class="colored">HUT</span>WINNER</h1>
    <h5 class="loader-logo text-center">BETA</h5>
    <div id="progress"></div>
    <h3 class="loader-text">LOADING</h3>
</div>

<!-- //// HEADER //// -->
<header id="main-header">
    <header id="main-header">
        @component('components.nav',['header'=> 'Market ', 'coloredHeader' => 'Place'])
        @endcomponent
    </header>
</header><!-- Header End -->

<!-- /// HERO SECTION /// -->
<!-- Hero Section End -->
<div id="container">
    <!-- /// Main Container /// -->
    <div class="content">
        <div class="main-container container pt-5 mt-3">
            <div id="hero-section" class="container">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100"
                                 src="/images/banner1.png"
                                 alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100"
                                 src="/images/banner3.png"
                                 alt="Third slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="/images/banner2.png"
                                 alt="Second slide">
                        </div>
                    </div>
                </div>
            </div>
            <!-- /// SECTION 1 /// -->
            <div id="about" class="large-margin p-5 mt-5">
                <div class="row heading tiny-margin">
                    <div class="col-md-auto">
                        <h1 class="">What
                            <span class="colored"> awaits</span> you</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <p class="small-margin descriptionText">HUTWinner offers <a href="{{route('packs.marketplace')}}">preset packs </a>, <a href="{{route('packs.custom.marketplace')}}">market-place packs</a> which
                            are created by site users and the ability to <a href="{{route('pack')}}">build your own pack</a>!
                    </div>
                    <div class="col-md-6">
                        <img id="" src="/images/customPacks.png" data-src="images/about.jpg"
                             class="img-fluid b-lazy imgValue " alt="digital collage">
                    </div>
                </div>
            </div>
            <div id="about" class="large-margin p-5">
                <div class="row heading tiny-margin">
                    <div class="col-12">
                        <h1 class="" style="float: right;">How it <span
                                    class="colored">works</span>
                        </h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <img id="" src="/images/myCards.png" data-src="images/about.jpg"
                             class="img-fluid b-lazy imgValue" alt="digital collage">
                    </div>
                    <div class="col-md-6">
                        <p class="small-margin descriptionText">Cards that you receive from packs can be sent to your
                            inbox for withdrawals or sold back to the website!</p>
                    </div>
                </div>
            </div>
            <div id="about" class="large-margin p-5">
                <div class="row heading tiny-margin">
                    <div class="col-12">
                        <h1 class="" style="float: right;">Withdrawal <span
                                    class="colored">Process</span>
                        </h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <img id="" src="/images/withdrawlRequest.png" data-src="images/about.jpg"
                             class="img-fluid b-lazy imgValue " alt="digital collage">
                    </div>
                    <div class="col-md-6">
                        <p class="small-margin descriptionText">Easily withdraw your cards to your Hockey Ultimate Team
                            by following our simple <a href="{{route('user.withdraw')}}">withdrawal process!</a> </p>
                    </div>
                </div>
            </div>
            <div id="about" class="large-margin p-5">
                <div class="row heading tiny-margin">
                    <div class="col-md-auto">
                        <h1 class="">Payment
                            <span class="colored">Methods</span></h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <p class="small-margin descriptionText">
                            Purchase points via PayPal or XSolla in order to open packs!
                        </p>
                    </div>
                    <div class="col-md-6">
                        <img id="" src="/images/myCoins.png" data-src="images/about.jpg"
                             class="img-fluid b-lazy imgValue" alt="digital collage">
                    </div>
                </div>
            </div>
            <div id="about" class="large-margin p-5">
                <div class="row heading tiny-margin">
                    <div class="col-md-auto">
                        <h1 class="">Who we
                            <span class="colored">are</span></h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <img id="" src="/images/logo.png" data-src="images/about.jpg"
                             class="img-fluid b-lazy imgValue " alt="digital collage">
                    </div>
                    <div class="col-md-6">
                        <p class="small-margin descriptionText">Our goal is to give consumers better odds of getting the
                            cards that they desire from packs. We take great pride in social responsibility; a minimum
                            of 3% of our monthly profits will be are donated to a charity which is voted upon by our
                            community. Our main priority is customer satisfaction.
                        </p>
                    </div>
                </div>
            </div>

        </div>
    </div><!-- Main Container End -->
@component('components.support')
@endcomponent
<!-- /// FOOTER /// --><!-- Footer End -->
@component('components.footer')
@endcomponent
<!-- Main Container End -->
</div>
<!-- //// SCRIPTS //// -->
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/blazy.min.js"></script>
<script src="js/isotope.pkgd.min.js"></script>
<script src="js/lightbox.min.js"></script>
<script src="js/jquery-modal-video.min.js"></script>
<script src="js/validator.min.js"></script>
<script src="js/strider.js"></script>
<script src="/js/search.js"></script>
<script src="/js/welcome.js"></script>
<script src="/js/floatingSupport.js"></script>
<script src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>
</body>
</html>
