<html xmlns="http://www.w3.org/1999/html">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>HUTWINNER</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <!-- OG meta tags that improve the look of your post on social media -->
    <meta property="og:site_name" content=""/><!--website name-->
    <meta property="og:site" content=""/> <!--website link-->
    <meta property="og:title" content=""/> <!--Post title-->
    <meta property="og:description" content=""/> <!--Post description-->
    <meta property="og:image" content=""/><!-- Image link (jpg only)-->
    <meta property="og:url" content=""/> <!--where do you want your post to link to-->
    <meta property="og:type" content="article"/>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/MarketPlace.css') }}">
    <link rel="stylesheet" href="{{asset('css/2019_HUT_Style.css')}}">
    <link rel="stylesheet" href="{{asset('css/nav.css')}}">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="/css/floatingSupport.css" rel="stylesheet">
    <link rel="apple-touch-icon" sizes="57x57" href="/images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <script src="https://cdn.rawgit.com/webcomponents/webcomponentsjs/v0.7.24/webcomponents-lite.js"></script>
    <style>
        .btn-light {
            background-color: #F6EA9C;
            color: #000;
            font-style: italic;
            border: unset;
        }

        h1 {
            font-family: unset;
        }

        a:visited {
            color: #F6EA9C;
        }

        .element-Container {
            background: #30303F !important;
            border-bottom-left-radius: unset !important;
            border-bottom-right-radius: unset !important;
            border-top-left-radius: 20px;
            border-top-right-radius: 20px;
        }

        /*ml-5 columnValue*/
        .element-ContainerSecond {
            border-top: 2px solid #F6EA9C;
            padding-left: 5%;
            background: #30303F !important;
            border-top-left-radius: unset !important;
            border-top-right-radius: unset !important;
            border-bottom-left-radius: 20px;
            border-bottom-right-radius: 20px;
        }

        a {
            color: #F6EA9C;
        }

        img {
            height: 100%;
            width: 100%;
        }

        .col-12 > .descriptionText {
            font-size: 1.5rem;
            margin-top: 5%;
            text-align: center;
        }

        @media (min-width: 1200px) {
            .container {
                max-width: 95%;
            }
        }

        body .containerResized {
            background: #222 !important;
        }

        .navbar {
            background: #20202A !important;
        }

        .main-container {
            background: #363647 !important;
            border-radius: 15px 15px 15px 15px;
            -webkit-box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.3);
            box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.3);
            margin-bottom: 120px !important;
        }

        .card-img-top {
            background-repeat: no-repeat;
            -webkit-background-size: cover;
            background-size: cover;
            width: 38vh;
            height: 42vh;
            display: inline-block;
        }

        .columnValue {
            min-height: 342px;
            min-width: 242px;
            padding: unset;
        }

        .probabilityCard {
            display: none;
            position: absolute;
            height: 100%;
            width: 100%;
            margin-left: auto;
            margin-right: auto;
            top: 0;
            z-index: 1000;
        }

        .cardContainer:hover .probabilityCard {
            display: block;
            background: rgba(0, 0, 0, 0.7);
            color: white;
            font-weight: 800;
        }

        .columnValue:hover .probability {
            display: block;
            background: rgba(0, 0, 0, 0.7);
            color: white;
            font-weight: 800;
        }

        .probability {
            display: none;
            position: absolute;
            height: 100%;
            width: 100%;
            margin-left: auto;
            margin-right: auto;
            top: 0;
            z-index: 1000;
        }

        /*END Pagination CSS*/

    </style>
</head>
<body style="position: relative; background: #202033 !important;">
<header id="main-header">
    @component('components.nav',['header'=> 'Market ', 'coloredHeader' => 'Place'])
    @endcomponent
</header>
<div id="container">
    <div class="main-container container pt-5 mt-3">
        <div class="row tiny-margin">
            <div class="col-12">
                <h1 class="marketPlaceTitle headerMarketplace animation-element slide-down"><span
                            class="colored">{{$pack->name}}</span></h1>
            </div>
            <div class="col-12 mt-2">
                <div class="container">
                    @if($errors->any())
                        <div id="failed" class="alert alert-danger">
                            <p>{{ $errors->first() }}</p>
                        </div>
                    @endif
                    <div class="row element-Container p-md-3 p-lg-5 p-sm-0">
                        <div class="col-5 col-md-5 align-content-center pl-5">
                            <div class="card-img-top"
                                 style=" background-image: url(/img/packCovers/{{strtoupper($pack->image)}});">

                            </div>

                        </div>
                        <div class="col-12 col-md-7 col-lg-7 text-md-center text-sm-center text-center">
                            @if(\Illuminate\Support\Facades\Auth::check())
                                @if(@\Illuminate\Support\Facades\Auth::user()->console == 'ps4')
                                    <h2>Price: <span class="colored">{{$pack->ps4_price}} Points</span></h2>
                                    <br>
                                @else
                                    <h2>Price: <span class="colored">{{$pack->xbox_price}} Points</span></h2>
                                    <br>
                                @endif
                            @else
                                <h2>Xbox Price: <span class="colored">{{$pack->xbox_price}} Points</span></h2>
                                <br>
                                <h2>PS4 Price: <span class="colored">{{$pack->ps4_price}} Points</span></h2>
                                <br>
                            @endif
                            <h2>Author: <span class="colored">{{$author}}</span></h2>
                            <br>
                            <h2>Number Of Cards: <span class="colored">{{$numberOfCards}}</span></h2>
                            @auth
                                <button type="button" class="btn btn-light select-button button-text mt-5"
                                        data-toggle="modal"
                                        data-target="#exampleModal">
                                    Buy Now
                                </button>
                            @else
                                <a href="{{ route('login') }}" class="btn btn-light select-button mt-5">
                                    <p class="button-text">Buy Now</p>
                                </a>
                            @endauth
                            <div style="font-size: 25px;margin-top: 5%;margin-bottom: 5%;">
                                @if($author == 'HUTWinner')
                                  You are guaranteed to get {{$pack->guaranteed_cards}} players from this pack

                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <?php $value = join(',', array_map(function ($val) {
                    return $val['id'];
                }, $cards)) ?>
                <?php $probability = join(',', array_map(function ($val) {
                    return $val['pivot']['probability'];
                }, $cards)) ?>

                <div class="container">
                    <div class="row element-ContainerSecond">
                        <!-- @TODO need to add array with all of the probability of the pack add field in cards-container web componenet -- Alpha -->
                        <cards-container ids="{{$value}}" packDisplay="true" probability="{{$probability}}"/>
                    </div>
                </div>

            </div>
        </div>
    </div>
    @auth
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Buy a pack</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        You are going to purchase the following pack: {{$pack->name}}
                    </div>
                    <div class="modal-footer">
                        <form action="{{ route('packs.purchase') }}" method="post">
                            @csrf
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Confirm Your Purchase</button>
                            <input type="hidden" value="{{$pack->id}}" name="pack">
                            <input type="hidden" value="{{$pack->name}}" name="name">
                        </form>

                    </div>
                </div>
            </div>
        </div>
    @endauth
    @component('components.support')
    @endcomponent
    @component('components.footer')
    @endcomponent
</div>
<!-- Footer End -->
<script src="../../js/app.js"></script>
<script src="/js/jquery-3.2.1.min.js"></script>
<script src="/js/sellPopup.js"></script>
<script src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>
<script src="/js/search.js"></script>
<script src="/js/floatingSupport.js"></script>
<script>
    var link = $('.pagination__link');
    link.on('click', function () {
        link.removeClass('is_active');
        $(this).addClass('is_active');
    });
    window.setTimeout(function () {
        $("#success").fadeTo(1000, 0).slideUp(1000, function () {
            $(this).remove();
        });
    }, 5000);
</script>
</body>
</html>