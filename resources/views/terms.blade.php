<html xmlns="http://www.w3.org/1999/html">
<head>

    <!-- META TAGS -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>HUTWINNER</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <!-- OG meta tags that improve the look of your post on social media -->
    <meta property="og:site_name" content=""/><!--website name-->
    <meta property="og:site" content=""/> <!--website link-->
    <meta property="og:title" content=""/> <!--Post title-->
    <meta property="og:description" content=""/> <!--Post description-->
    <meta property="og:image" content=""/><!-- Image link (jpg only)-->
    <meta property="og:url" content=""/> <!--where do you want your post to link to-->
    <meta property="og:type" content="article"/>
    <link rel="apple-touch-icon" sizes="57x57" href="images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/MarketPlace.css') }}">
    <link rel="stylesheet" href="{{ asset('css/2019_HUT_Style.css')}}">
    <link rel="stylesheet" href="{{ asset('css/nav.css')}}">
    <link href="/css/floatingSupport.css" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet">

    <style>
        @font-face {
            font-family: "steelfish";
            src: url("/fonts/steelfish/steelfis.ttf");
        }

        .link-MarketPlace {
            text-align: center;
            font-style: italic;
            font-size: 68px !important;
            letter-spacing: 2.05px;
            text-transform: uppercase;
            font-weight: 400;
            font-family: Steelfish;
        }

        img {
            vertical-align: middle;
            border-style: none;
            width: 100%;
        }

        .btn-light {
            background-color: #F6EA9C;
            color: #000;
            font-style: italic;
            border: unset;
        }

        h1 {
            font-family: unset;
        }

        a:visited {
            color: #F6EA9C;
        }

        .element-Container {
            background: #30303F !important;
            border-bottom-left-radius: unset !important;
            border-bottom-right-radius: unset !important;
            border-top-left-radius: 20px;
            border-top-right-radius: 20px;
        }

        input[type='submit'] {
            border: none !important;
        }

        /*ml-5 columnValue*/
        .element-ContainerSecond {
            padding-left: 5%;
            background: #30303F !important;
            -webkit-border-radius: 20px;
            -moz-border-radius: 20px;
            border-radius: 20px;
            -webkit-box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.3);
            box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.3);
        }

        a {
            color: #F6EA9C;
        }

        @media (min-width: 1200px) {
            .container {
                max-width: 95%;
            }
        }

        body .containerResized {
            background: #222 !important;
        }

        .navbar {
            background: #20202A !important;
        }

        .element-ContainerSecond p {
            font-size: 1.3em;
        }

        .main-container {
            background: #363647 !important;
            border-radius: 15px 15px 15px 15px;
            -webkit-box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.3);
            box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.3);
            margin-bottom: 120px !important;
        }

        .card-img-top {
            background: url(/img/hutbd-pack-icons/x7.jpg.pagespeed.ic.8-NtslfRji.jpg);
            background-repeat: no-repeat;
            -webkit-background-size: cover;
            background-size: cover;
            height: 300px;
            width: 300px;
            display: inline-block;
        }

        .columnValue {
            min-height: 342px;
            min-width: 242px;
            padding: unset;
        }

        .packBackground {
            width: 100%;
            height: 35%;
        }

        .headerPolicy {
            font-size: 42px !important;
            font-weight: 400;
            color: #ffffff;
            font-family: Steelfish;
            font-style: italic;
            letter-spacing: 2.05px;
            text-transform: uppercase;
        }
    </style>
</head>
<body style="position: relative; background: #202033 !important;">
<header id="main-header">
    @component('components.nav',['header'=> 'Market ', 'coloredHeader' => 'Place'])
    @endcomponent
</header>
<div id="container">
    <div class="main-container container pt-5 mt-5">
        <div class="row tiny-margin">
            <div class="col-12">
                <h1 class="marketPlaceTitle headerMarketplace animation-element slide-down"><span
                            class="colored">Terms & Conditions General</span>
                </h1>
            </div>
            <div class="col-12">
                <div class="container">
                    <div class="row element-ContainerSecond p-5 mb-5">
                        <h5 class="headerPolicy colored"><span
                                    class="colored">1. Acceptance of Terms of Use and Amendments.</span></h5>
                        <hr>
                        <p>
                            As a user, each time you enter this website, you are agreeing to abide by our terms of
                            use. We
                            reserve all rights to amend or change any of these terms when deemed necessary. You
                            agree that
                            by using any service through or on this website, you will be subject to the rules and
                            guidelines
                            set by us applicable to these services. By agreeing to these Terms & Conditions, you
                            confirm
                            that you are at least of the age of majority in your state or province of residence. If
                            you are
                            an adult, you have given your consent to allow any of your minor dependents to use this
                            site
                            under your login.
                        </p>
                        <h5 class="headerPolicy mt-5 colored"><span class="colored">2. Your Responsibilities and Registration Obligations.</span>
                        </h5>
                        <hr>
                        <p>You may be asked to register to our website in order to use it. By doing so, you are
                            agreeing to
                            provide us with 100% truthful and factual information when asked for it. Upon
                            registration of
                            your account with our website, you are automatically agreeing to our Terms of Use which
                            can be
                            modified at any time by us when deemed necessary.
                        </p>
                        <h5 class="headerPolicy mt-5"><span class="colored">4. Registration and Password</span></h5>
                        <hr>
                        <p>You are responsible to maintain the confidentiality of your password and shall be
                            responsible for
                            all uses via your registration and/or login, whether authorized or unauthorized by you.
                            You
                            agree to immediately notify us of any unauthorized use or registration linked to your
                            personal
                            information
                        </p>
                        <h5 class="headerPolicy mt-5"><span class="colored">5. Your Conduct</span></h5>
                        <hr>
                        <p>
                            You agree that our web site may indirectly expose you to Content that may be
                            objectionable or
                            offensive. We shall not be responsible to you in any way for the Content that appears on
                            this
                            web site nor for any error or omission.
                            You explicitly agree, in using this web site or any service provided, that you shall
                            not:
                            (a) Provide any Content or perform any conduct that may be unlawful, illegal,
                            threatening,
                            harmful,
                            abusive, harassing, stalking, tortious, defamatory, libelous, vulgar, obscene,
                            offensive,
                            objectionable, pornographic, designed to or does interfere or interrupt this web site or
                            any
                            service provided, infected with a virus or other destructive or deleterious programming
                            routine,
                            give rise to civil or criminal liability, or which may violate an applicable local,
                            national or
                            international law;
                            (b) Impersonate or misrepresent your association with any person or entity, or forge or
                            otherwise seek to conceal or misrepresent the origin of any Content provided by you;
                            (c) Collect or harvest any data about other users;
                            (d) Provide or use this website and any Content or service in any commercial manner or
                            in any
                            manner that would involve junk mail, spam, chain letters, pyramid schemes, or any other
                            form of
                            unauthorized advertising without our prior written consent;
                            (e) Provide any Content that may give rise to our civil or criminal liability or which
                            may
                            constitute or be considered a violation of any local, national or international law,
                            including
                            but not limited to laws relating to copyright, trademark, patent, or trade secrets.

                        </p>
                        <h5 class="headerPolicy mt-5"><span class="colored">6. Third Party Services</span></h5>
                        <hr>
                        <p>Goods and services of third parties may be advertised and/or made available on or through
                            this
                            website. Representations made regarding products and services provided by third parties
                            are
                            governed by the policies and representations made by these third parties. We shall not
                            be liable
                            for or responsible in any manner for any of your dealings or interaction with third
                            parties.
                        </p>
                        <h5 class="headerPolicy mt-5"><span class="colored">7. Indemnification</span></h5>
                        <hr>
                        <p>You agree to indemnify and hold us harmless, our subsidiaries, affiliates, related
                            parties,
                            officers, directors, employees, agents, independent contractors, advertisers, partners,
                            and
                            co-branders from any claim or demand, including reasonable legal fees, that may be made
                            by any
                            third party, that is due to or arising out of your conduct or connection with this web
                            site or
                            service, your violation of this Terms of Use or any other violation of the rights of
                            another
                            person or party.
                        </p>
                        <h5 class="headerPolicy mt-5"><span class="colored">8. DISCLAIMER OF WARRANTIES</span></h5>
                        <hr>
                        <p>YOU UNDERSTAND AND AGREE THAT YOUR USE OF THIS WEBSITE AND ANY SERVICES OR CONTENT
                            PROVIDED (THE
                            "SERVICE") IS MADE AVAILABLE AND PROVIDED TO YOU AT YOUR OWN RISK. IT IS PROVIDED TO YOU
                            "AS IS"
                            AND WE EXPRESSLY DISCLAIM ALL WARRANTIES OF ANY KIND, IMPLIED OR EXPRESS, INCLUDING BUT
                            NOT
                            LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
                            NON-INFRINGEMENT. WE MAKE NO WARRANTY, IMPLIED OR EXPRESS, THAT ANY PART OF THE SERVICE
                            WILL BE
                            UNINTERRUPTED, ERROR-FREE, VIRUS-FREE, TIMELY, SECURE, ACCURATE, RELIABLE, OF ANY
                            QUALITY, NOR
                            THAT ANY CONTENT IS SAFE IN ANY MANNER FOR DOWNLOAD. YOU UNDERSTAND AND AGREE THAT
                            NEITHER US
                            NOR ANY PARTICIPANT IN THE SERVICE PROVIDES PROFESSIONAL ADVICE OF ANY KIND AND THAT USE
                            OF SUCH
                            ADVICE OR ANY OTHER INFORMATION IS SOLELY AT YOUR OWN RISK AND WITHOUT OUR LIABILITY OF
                            ANY
                            KIND. Some jurisdictions may not allow disclaimers of implied warranties and the above
                            disclaimer may not apply to you only as it relates to implied warranties.
                        </p>
                        <h5 class="headerPolicy mt-5"><span class="colored">9. LIMITATION OF LIABILITY</span></h5>
                        <hr>
                        <p>YOU EXPRESSLY UNDERSTAND AND AGREE THAT WE SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT,
                            SPECIAL,
                            INCIDENTAL, CONSEQUENTIAL OR EXEMPLARY DAMAGES, INCLUDING BUT NOT LIMITED TO, DAMAGES
                            FOR LOSS
                            OF PROFITS, GOODWILL, USE, DATA OR OTHER INTANGIBLE LOSS (EVEN IF WE HAVE BEEN ADVISED
                            OF THE
                            POSSIBILITY OF SUCH DAMAGES), RESULTING FROM OR ARISING OUT OF (1) THE USE OF OR THE
                            INABILITY
                            TO USE THE SERVICE, (2) THE COST TO OBTAIN SUBSTITUTE GOODS AND/OR SERVICES RESULTING
                            FROM ANY
                            TRANSACTION ENTERED INTO ON THROUGH THE SERVICE, (3) UNAUTHORIZED ACCESS TO OR
                            ALTERATION OF
                            YOUR DATA TRANSMISSIONS, (4) STATEMENTS OR CONDUCT OF ANY THIRD PARTY ON THE SERVICE, OR
                            (5) ANY
                            OTHER MATTER RELATING TO THE SERVICE. In some jurisdictions, it is not permitted to
                            limit
                            liability and therefore such limitations may not apply to you.
                        </p>
                        <h5 class="headerPolicy mt-5"><span class="colored">10. Reservation of Rights</span></h5>
                        <hr>
                        <p>We reserve all of our rights, including but not limited to any and all copyrights,
                            trademarks,
                            patents, trade secrets, and any other proprietary right that we may have in our web
                            site, its
                            content, and the goods and services that may be provided. The use of our rights and
                            property
                            requires our prior written consent. We are not providing you with any implied or express
                            licenses or rights by making services available to you and you will have no rights to
                            make any
                            commercial uses of our web site or service without our prior written consent.
                        </p>
                        <h5 class="headerPolicy mt-5"><span
                                    class="colored">11. Notification of Copyright Infringement</span></h5>
                        <hr>
                        <p>If you believe that your property has been used in any way that would be considered
                            copyright
                            infringement or a violation of your intellectual property rights, our copyright agent
                            may be
                            contacted at the following address: admin@hutwinner.com
                        </p>
                        <h5 class="headerPolicy mt-5"><span class="colored">12. Applicable Law</span></h5>
                        <hr>
                        <p>You agree that this Terms of Use and any dispute arising out of your use of this web site
                            or our
                            products or services shall be governed by and construed in accordance with local laws
                            where the
                            headquarters of the owner of this website is located, without regards to its conflict of
                            law
                            provisions. By registering or using this website and service, you consent and submit to
                            the
                            exclusive jurisdiction and venue of the county or city where the headquarters of the
                            owner of
                            this website is located.
                        </p>
                        <h5 class="headerPolicy mt-5"><span class="colored">13. Miscellaneous Information</span>
                        </h5>
                        <hr>
                        <p>(i) In the event that this Terms of Use conflicts with any law under which any provision
                            may be
                            held invalid by a court with jurisdiction over the parties, such provision will be
                            interpreted
                            to reflect the original intentions of the parties in accordance with applicable law, and
                            the
                            remainder of this Terms of Use will remain valid and intact; <br>(ii) The failure of
                            either
                            party to
                            assert any right under this Terms of Use shall not be considered a waiver of any of that
                            party's
                            right and that right will remain in full force and effect;<br> (iii) You agree that
                            without
                            regard
                            to any statue or contrary law that any claim or cause arising out of this website or its
                            services must be filed within three months (3) months after such claim or cause arose or
                            the
                            claim shall be forever barred; <br>(iv) We may assign our rights and obligations under
                            this
                            Terms &
                            Conditions and we shall be relieved of any further obligation.
                        </p>
                        <h5 class="headerPolicy mt-5"><span class="colored">14. What We Are Providing</span></h5>
                        <hr>
                        <p>HUTWinner.com provides the users with an exclusive service which facilitates the process
                            of
                            acquiring star players in Hockey Ultimate Team. Our service saves the consumer’s time by
                            opening
                            packs to get the cards that they actually desire for their Ultimate Team. Once the
                            service has
                            been performed, it cannot be reversed under any circumstances.
                            We are not in any way associated with EA SPORTS. Card art & images displayed on this
                            website are
                            property of EA SPORTS and are used for entertainment purposes only.
                            You will be receiving the service of the players being transferred to your account to
                            enhance
                            your gaming experience in Hockey Ultimate Team.
                        </p>
                        <h5 class="headerPolicy mt-5"><span class="colored">15. Refund Policy</span></h5>
                        <hr>
                        <p>
                            We do accept refunds from customers under special circumstances. If you believe that you
                            deserve
                            a refund, please open a support ticket or email us at support@hutwinner.com<br>
                            By purchasing any services through our site, you are automatically agreeing to our Terms
                            &
                            Conditions, our Terms and Conditions are there to give all of our users an experience
                            that they
                            deserve, a service that will make their Hockey Ultimate Team experience a better one.
                        </p>
                        <h5 class="headerPolicy mt-5"><span class="colored">16. Identification</span></h5>
                        <hr>
                        <p>Please know HUTWinner.com holds the right to request for any government-issued ID image
                            to be
                            sent in order for to validate orders through the system to combat fraud. Please
                            understand that
                            these will be kept safe and only requested if deemed necessary.<br>
                            Also note: If you order with a card that does not belong to you, we will require ID from
                            that
                            person as well. We will need to conduct a recorded phone call with that person and ask
                            them if
                            they did provide you with consent to use their card. (Please do not order if you can not
                            provide
                            us with these)

                        </p>
                        <h5 class="headerPolicy mt-5"><span
                                    class="colored">17. Errors, Inaccuracies And Omissions</span>
                        </h5>
                        <hr>
                        <p>Occasionally, there may be information on our website or in the Terms & Conditions that
                            contains
                            typographical errors, inaccuracies or omissions that may relate to product descriptions,
                            pricing, promotions, offers, product shipping charges, transit times and availability.
                            We
                            reserve the right to correct any errors, inaccuracies or omissions, and to change or
                            update
                            information or cancel orders if any information in the Service or on any related website
                            is
                            inaccurate at any time without prior notice (including after you have submitted your
                            order).
                            We undertake no obligation to update, amend or clarify information in the Service or on
                            any
                            related website, including without limitation, pricing information, except as required
                            by law.
                            No specified update or refresh date applied in the Service or on any related website,
                            should be
                            taken to indicate that all information in the Service or on any related website has been
                            modified or updated.
                        </p>
                        <h5 class="headerPolicy mt-5"><span class="colored">19. Delivery</span></h5>
                        <hr>
                        <p>HUTWinner.com reserves the right to send you an item equal to or up to 20% greater in
                            value if
                            the original desired item that you request to withdraw cannot be found for you within
                            two (2)
                            business days.
                        </p>
                        <h5 class="headerPolicy mt-5"><span class="colored">Purchase</span></h5>
                        <hr>
                        <p>By Purchasing from us, you are automatically agreeing to the following Terms &
                            Conditions.
                            Please Read carefully and consider that non-compliance with these Terms & Conditions,
                            will lead
                            in court proceedings brought before you by HUTWinner.com and will be charged
                            appropriately.
                        <ol>
                            <li>
                                I agree that the card/bank being used to create any services from HUTWinner.com is
                                owned by
                                me
                                and in my name. Otherwise I have FULL permission from the card/bank owner to
                                undertake this
                                purchase.
                            </li>
                            <li>
                                I consent that the contact number on file is correct, as HUTWinner.com holds the
                                right to
                                call me
                                and ask for permission from the card holder in a recorded phone call, as well as
                                holding the
                                right
                                to ask me for further Identification checks/proof of address/ or proof of card
                                ownership (of
                                the
                                card holder
                            </li>
                            <li>
                                We will not be held liable for any type of ban imposed to your console or account by
                                EA,
                                Sony or
                                Microsoft. We take every precaution to ensure your cards are delivered safely, but
                                we assume
                                no
                                liability and will issue no refunds if your account were to get banned or suspended.
                            </li>
                            <li>
                                4. You are paying for a service for POINTS to use on our website, which will be
                                delivered to
                                your account. You will receive an email upon purchase, all transactions are logged.
                            </li>
                        </ol>
                        Breaking these Terms & Conditions will lead to the maximum penalty/prosecution.
                    </div>
                </div>
            </div>
        </div>
    </div>
    @component('components.support')
    @endcomponent
    @component('components.footer')
    @endcomponent
</div>
<!-- Footer End -->
<script src="../../js/app.js"></script>
<script src="/js/search.js"></script>
<script src="/js/sellPopup.js"></script>
<script src="/js/floatingSupport.js"></script>
</body>
</html>
