<html xmlns="http://www.w3.org/1999/html">
<head>

    <!-- META TAGS -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>HUTWINNER</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/MarketPlace.css') }}">
    <link rel="stylesheet" href="{{ asset('css/2019_HUT_Style.css')}}">
    <link rel="stylesheet" href="{{ asset('css/nav.css')}}">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="/css/floatingSupport.css" rel="stylesheet">
    <!-- OG meta tags that improve the look of your post on social media -->
    <meta property="og:site_name" content=""/><!--website name-->
    <meta property="og:site" content=""/> <!--website link-->
    <meta property="og:title" content=""/> <!--Post title-->
    <meta property="og:description" content=""/> <!--Post description-->
    <meta property="og:image" content=""/><!-- Image link (jpg only)-->
    <meta property="og:url" content=""/> <!--where do you want your post to link to-->
    <meta property="og:type" content="article"/>
    <link rel="apple-touch-icon" sizes="57x57" href="images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">


    <style>
        .link-MarketPlace {
            text-align: center;
            font-style: italic;
            font-size: 68px !important;
            letter-spacing: 2.05px;
            text-transform: uppercase;
            font-weight: 400;
            font-family: Steelfish;
        }

        img {
            vertical-align: middle;
            border-style: none;
            width: 100%;
        }

        .btn-light {
            background-color: #F6EA9C;
            color: #000;
            font-style: italic;
            border: unset;
        }

        h1 {
            font-family: unset;
        }

        a:visited {
            color: #F6EA9C;
        }

        .element-Container {
            background: #30303F !important;
            border-bottom-left-radius: unset !important;
            border-bottom-right-radius: unset !important;
            border-top-left-radius: 20px;
            border-top-right-radius: 20px;
        }

        input[type='submit'] {
            border: none !important;
        }

        /*ml-5 columnValue*/
        .element-ContainerSecond {
            padding-left: 5%;
            background: #30303F !important;
            -webkit-border-radius: 20px;
            -moz-border-radius: 20px;
            border-radius: 20px;
            -webkit-box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.3);
            box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.3);
        }

        a {
            color: #F6EA9C;
        }

        @media (min-width: 1200px) {
            .container {
                max-width: 95%;
            }
        }

        body .containerResized {
            background: #222 !important;
        }

        .navbar {
            background: #20202A !important;
        }

        .element-ContainerSecond p {
            font-size: 1.5em;
        }

        .main-container {
            background: #363647 !important;
            border-radius: 15px 15px 15px 15px;
            -webkit-box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.3);
            box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.3);
            margin-bottom: 120px !important;
        }

        .columnValue {
            min-height: 342px;
            min-width: 242px;
            padding: unset;
        }

        .packBackground {
            width: 100%;
            height: 35%;
        }

        /*END Pagination CSS*/

    </style>
</head>
<body style="position: relative; background: #202033 !important;">
<header id="main-header">
    @component('components.nav',['header'=> 'Market ', 'coloredHeader' => 'Place'])
    @endcomponent
</header>
<div id="container">
    <div class="main-container container pt-5 mt-5">
        <div class="row tiny-margin">
            <div class="col-12">
                <h1 class="marketPlaceTitle headerMarketplace animation-element slide-down"><span
                            class="colored">About Us</span></h1>
            </div>
            <div class="col-12">
                <div class="container">
                    <div class="row element-ContainerSecond p-5 mb-5">
                        <p>
                            HUTWinner was created by a Canadian, Hockey Ultimate Team enthusiast to facilitate the
                            process
                            for
                            HUT Players consumers to get the cards that they desire from packs.
                        </p>
                        <br>
                        <p>
                            We take great pride in social responsibility; a minimum of 3% of our monthly profits will be
                            are
                            donated to a charity which is voted upon by our community. Our main priority is customer
                            satisfaction;, we are always open to feedback and have multiple contact methods for quick
                            and,
                            efficient customer support.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @component('components.support')
    @endcomponent
    @component('components.footer')
    @endcomponent
</div>
<!-- Footer End -->
<script src="../../js/app.js"></script>
<script src="/js/search.js"></script>
<script src="/js/sellPopup.js"></script>
<script src="/js/floatingSupport.js"></script>
</body>
</html>
