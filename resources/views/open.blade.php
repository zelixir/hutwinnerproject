<html >
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <title>HUTWINNER</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <!-- OG meta tags that improve the look of your post on social media -->
    <meta property="og:site_name" content=""/><!--website name-->
    <meta property="og:site" content=""/> <!--website link-->
    <meta property="og:title" content=""/> <!--Post title-->
    <meta property="og:description" content=""/> <!--Post description-->
    <meta property="og:image" content=""/><!-- Image link (jpg only)-->
    <meta property="og:url" content=""/> <!--where do you want your post to link to-->
    <meta property="og:type" content="article"/>
    <link rel="apple-touch-icon" sizes="57x57" href="images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/MarketPlace.css') }}">
    <link rel="stylesheet" href="{{ asset('css/2019_HUT_Style.css')}}">
    <link rel="stylesheet" href="{{ asset('css/nav.css')}}">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/2019_HUT_Style.css') }}">
    <script src="https://cdn.rawgit.com/webcomponents/webcomponentsjs/v0.7.24/webcomponents-lite.js"></script>
    <style>
        @import url(https://fonts.googleapis.com/css?family=Roboto:400,500,700);
        @import url(https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic);

        * {
            box-sizing: border-box;
            margin: 0;
            padding: 0;
        }

        body .containerMain {
            background: #363647 !important;
            border-radius: 15px 15px 15px 15px;
            -webkit-box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.3);
            box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.3);
            margin-bottom: 120px !important;
            height: 70%;
        }

        #toggle {
            margin-left: auto;
            margin-right: auto;
            background: url(/img/hutbd-pack-icons/x7.jpg.pagespeed.ic.8-NtslfRji.jpg);
            background-repeat: no-repeat;
            -webkit-background-size: cover;
            background-size: cover;

        }

        .shake-bottom {
            animation: shake 1s cubic-bezier(.36, .07, .19, .97) both;
            animation-iteration-count: 4;
            transform: translate3d(0, 0, 0);
            backface-visibility: hidden;
            perspective: 1000px;
        }

        @keyframes shake {
            10%, 90% {
                transform: translate3d(-1px, 0, 0);
            }

            20%, 80% {
                transform: translate3d(2px, 0, 0);
            }

            30%, 50%, 70% {
                transform: translate3d(-4px, 0, 0);
            }

            40%, 60% {
                transform: translate3d(4px, 0, 0);
            }
        }

        .blog-card {
            width: 350px;
            height: 500px;
            position: absolute;
            top: 50%;
            left: 50%;
            margin: -250px 0 0 -175px;
            overflow: hidden;
            border-radius: 10px;
            box-shadow: 3px 3px 20px rgba(0, 0, 0, 0.5);
            text-align: center;
        }

        .card-img-top {
            background-repeat: no-repeat;
            -webkit-background-size: cover;
            background-size: cover;
            margin-top: 5%;
        }

        /** .color-overlay {
             background: rgba(0, 0, 0, 0.5);
             width: 100%;
             height: 100%;
             position: absolute;
             z-index: 10;

             left: 0;
             transition: background 0.3s cubic-bezier(0.33, 0.66, 0.66, 1);
         }
         **/

        .title-content {
            text-align: center;
            margin: 70px 0 0 0;
            z-index: 20;
            width: 100%;
        }

        h3 {
            font-size: 20px;
            font-weight: 500;
            letter-spacing: 2px;
            color: white;
            font-family: 'Roboto', sans-serif;
            margin-bottom: 0;
        }

        hr {
            width: 50px;
            height: 3px;
            margin: 20px auto;
            border: 0;
            background: #D0BB57;
        }

        .intro {
            width: 170px;
            margin: 0 auto;
            color: #DCE3E7;
            font-family: 'Droid Serif', serif;
            font-size: 13px;
            font-style: italic;
            line-height: 18px;
        }

        .utility-info {
            position: absolute;
            bottom: 0;
            left: 0;
            z-index: 20;
            width: 100%;
            height: 20%;
            background: rgba(84, 104, 110, 0.4) !important;
            -webkit-border-radius: 0 0 25px 25px;
            -moz-border-radius: 0 0 25px 25px;
            border-radius: 0 0 25px 25px;
        }

        .utility-list {
            list-style-type: none;
            margin: 0 0 30px 20px;
            padding: 0;
            width: 100%;
            top: 50%;
        }

        .coins {
            width: 24px;
            height: 24px;
        }

        .card {
            height: 50%;
            width: 18%;
            padding: 0;
            background-color: unset;
        }

        .priceText {
            color: white;
            top: 8;
            position: absolute;
            left: 40%;
        }

        .card-img-top {
            height: 40vh;
            width: 37vh;
            -webkit-border-radius: 25px 25px 0 0;
            -moz-border-radius: 25px 25px 0 0;
            border-radius: 25px 25px 0 0;
        }

        .card-body {
            height: 20%;
            -webkit-border-radius: 25px 25px 0 0;
            -moz-border-radius: 25px 25px 0 0;
            border-radius: 25px 25px 0 0;
        }

        [data-dis-container] {
            width: 100%;
            height: 100%;
            margin: unset !important;
            margin-left: 34% !important;
        }

        .buyDiv {
            position: absolute;
            width: 100%;
            height: 100%;
        }

        .btn-buy {
            bottom: 0;
            position: absolute;
            width: 100%;
            border-radius: 0 0 25px 25px;
            box-shadow: none;
            color: white;

        }

        .btn-buy:focus {
            outline: none;
            box-shadow: none;
        }

        .btn:focus {
            outline: none;
        }

        .searchbar {
            margin-bottom: auto;
            margin-top: auto;
            height: 60px;
            background-color: #353b48;
            border-radius: 30px;
            padding: 10px;
        }

        img {
            height: 100%;
            width: 100%;
        }

        .search_input {
            color: white;
            border: 0;
            outline: 0;
            background: none;
            width: 0;
            caret-color: transparent;
            line-height: 40px;
            transition: width 0.4s linear;
            border: none !important;
        }

        .searchbar:hover > .search_input {
            padding: 0 10px;
            width: 350px;
            caret-color: red;
            transition: width 0.4s linear;
        }

        .searchbar:hover > .search_icon {
            background: white;
            color: #e74c3c;
        }

        .search_icon {
            height: 40px;
            width: 40px;
            float: right;
            display: flex;
            justify-content: center;
            align-items: center;
            border-radius: 50%;
            color: white;
        }

        /* Add Animation */
        @-webkit-keyframes animatetop {
            from {
                top: -300px;
                opacity: 0
            }
            to {
                top: 0;
                opacity: 1
            }
        }

        @keyframes animatetop {
            from {
                top: -300px;
                opacity: 0
            }
            to {
                top: 0;
                opacity: 1
            }
        }

        /* The Close Button */
        .close {
            color: white;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .btn-success {
            border: unset !important;
        }

        .close:hover,
        .close:focus {
            text-decoration: none;
            cursor: pointer;
        }

        .modal-header {
            border-bottom: 1px solid #b48445;
            padding: 1rem;
            color: white;
        }

        .modal-body {
            padding: 2px 16px;
        }

        .modal-footer {
            border-top: 1px solid #b48445;
            padding: 2px 16px;
            color: white;
        }

        .container-group > .row {
            overflow-x: scroll;
            scrollbar-color: dark;
            white-space: nowrap;
        }

        .container-group > .row > .card {
            display: inline-block;
            float: none;
        }

        .container-group .row .card {
            display: inline-block;
            float: none;
        }

        .displayCard {
            display: none;
        }

        .card-wrapper {
            display: inline-block;
            perspective: 1000px;
        }

        .card-wrapper .card {
            position: relative;
            cursor: pointer;
            transition-duration: 0.6s;
            transition-timing-function: ease-in-out;
            transform-style: preserve-3d;
        }

        .card-wrapper .card .front,
        .card-wrapper .card .back {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            backface-visibility: hidden;
            transform: rotateX(0deg);
        }

        .card-wrapper .card .front {
            z-index: 2;
        }

        .card-wrapper .card .back,
        .card-wrapper.flip-right .card .back {
            transform: rotateY(180deg);
        }

        .card-cover {
            background-color: red;
            height: 100%;
            width: 100%;
        }

        [data-dis-container] {
            vertical-align: bottom;
        }

        button[data-dis-type] {
            margin: 0;
        }

        [data-dis-trigger-for] {
            cursor: pointer;
        }

        /* Generic click behavior */

        /* Start the animation we add a class */

        .card {
            border: unset;
            transition: transform 0.6s;
            transform-style: preserve-3d;
        }

        .columnValue {
            min-height: 380px;
            min-width: 200px;
        }

        .card {
            transition: transform 0.6s;
            transform-style: preserve-3d;
        }

        .valueFlip {
            transform: rotateY(180deg);
        }

        .front {
            background-image: url("/images/Card Cover.png");
            -webkit-background-size: cover;
            background-size: cover;
            min-height: 342px;
            min-width: 242px;
            z-index: 2;
            position: absolute;
        }

        .back {
            position: absolute;
            top: 0;
            min-height: 342px;
            min-width: 242px;
            z-index: 20;
            transform: rotateY(180deg);
        }

        .front .back {
            backface-visibility: hidden;
            position: absolute;
            top: 0;
        }

        .card {
            /* Set the transition effects */
            -webkit-transition: -webkit-transform 0.4s;
            -moz-transition: -moz-transform 0.4s;
            -o-transition: -o-transform 0.4s;
            transition: transform 0.4s;
            -webkit-transform-style: preserve-3d;
            -moz-transform-style: preserve-3d;
            -o-transform-style: preserve-3d;
            transform-style: preserve-3d;
        }
        ::-webkit-scrollbar {
            width: 1em;
        }

        ::-webkit-scrollbar-track {
           background: #404040;
            border-radius: 25px;
        }

        ::-webkit-scrollbar-thumb {
          -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.9);;
            border-radius: 25px;

        }
        .card .front,
        .card .back {
            -webkit-backface-visibility: hidden;
            -moz-backface-visibility: hidden;
            -o-backface-visibility: hidden;
            backface-visibility: hidden;

            box-shadow: 3px 5px 20px 2px rgba(0, 0, 0, 0.25);
        }

        .card .back {
            -webkit-transform: rotateY(180deg);
            -moz-transform: rotateY(180deg);
            -o-transform: rotateY(180deg);
            transform: rotateY(180deg);
        }

        .sellNow {
            display: none;
            position: absolute;
            height: 100%;
            width: 100%;

            margin-left: auto;
            margin-right: auto;
            top: 0;
            z-index: 1000;
        }

        .card-Container:hover .sellNow {
            display: block;
            background: rgba(0, 0, 0, 0.7);
            color: white;
            font-weight: 800;
        }

        #buttonTrigger {
            -webkit-border-radius: 100px;
            -moz-border-radius: 100px;
            border-radius: 100px;
        }


    </style>
</head>
<body style="position:relative; background: #202033 !important;">

<header id="main-header">
    @component('components.nav',['header'=> 'Market ', 'coloredHeader' => 'Place'])
    @endcomponent
</header>
<div class="alert alert-success" id="success-div" style="display: none">
    <p>Your Card has been sold!</p>
</div>
<div class="alert alert-danger" id="failed-div" style="display: none">
    <p>An error has occurred while selling your card</p>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2>
                    Confirmation
                </h2>
                <span class="close">&times;</span>
            </div>
            <div class="modal-body" id="header">

            </div>


            <div class="modal-footer">
                <form action="{{ route('user.quickSell') }}" method="POST" id="userForm">
                    @csrf
                    <input id="card" type="hidden" value="" name="card">
                    <input type="submit" value="Confirm" class="btn btn-success" id="Confirm">
                </form>

            </div>
        </div>
    </div>
</div>
<div id="container">
    <div class="container containerMain container-group mt-3">
        <a href="{{ route('home')}}" class="btn btn-primary">Go back to Inbox</a>
        <div class=" col-12 ">
            <div id="image" class="card-img-top mx-auto shake-bottom" data-dis-type="simultaneous"
                 data-dis-particle-type="ExplodingParticle"
                 data-dis-reduction-factor="111" style="background-image: url(/img/packCovers/{{strtoupper($pack->image)}});"></div>

        </div>
        <?php $value = join(',', array_map(function ($val) {
            return $val['id'];
        }, $cards)) ?>
        <div class="row text-center flex-nowrap m-2 pl-5 displayCard" id="mainContainer">
            <cards-container slider="true" ids="{{$value}}"/>
        </div>
    </div>

    @component('components.footer')
    @endcomponent
</div>
<script src="../../js/app.js"></script>
<script src="/js/jquery-3.2.1.min.js"></script>
<script src="/js/quickSell.js"></script>
<script type="text/javascript" src="/js/html2canvas.min.js"></script>
<script type="text/javascript" src="/js/disintegrate.js"></script>
<script src="/js/openAnimation.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<script>
    $('.card').click(function () {
        if (!$(this).hasClass('flippedCard')) {
            $(this).addClass('valueFlip');
            $(this).addClass('flippedCard')
        }
    });
</script>
</body>
</html>
