<html xmlns="http://www.w3.org/1999/html">
<head>

    <!-- META TAGS -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>HUTWINNER</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <!-- OG meta tags that improve the look of your post on social media -->
    <meta property="og:site_name" content=""/><!--website name-->
    <meta property="og:site" content=""/> <!--website link-->
    <meta property="og:title" content=""/> <!--Post title-->
    <meta property="og:description" content=""/> <!--Post description-->
    <meta property="og:image" content=""/><!-- Image link (jpg only)-->
    <meta property="og:url" content=""/> <!--where do you want your post to link to-->
    <meta property="og:type" content="article"/>
    <link rel="apple-touch-icon" sizes="57x57" href="images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/MarketPlace.css') }}">
    <link rel="stylesheet" href="{{ asset('css/nav.css')}}">
    <link href="/css/floatingSupport.css" rel="stylesheet">

    <script src="https://cdn.rawgit.com/webcomponents/webcomponentsjs/v0.7.24/webcomponents-lite.js"></script>
    <style>
        .dropdown-menu {
            background: #363647 !important;
        }
        .pagination{
            background: #30303F !important;
        }
        .page-link{
            background-color: unset !important;
            border: unset !important;
        }
        .page-link:hover{
            color: white !important;
        }
        .dropdown-item {
            color: #F6EA9C;
        }

        .dropdown-item:hover {
            color: #F6EA9C;
            background: unset !important;
        }

        @media (min-width: 1200px) {
            .container {
                max-width: 95%;
            }
        }

        .dropdown-item {
            color: #F6EA9C;
        }

        .dropdown-item:hover {
            color: #F6EA9C;
            background: unset !important;
        }

        a:visited {
            color: #F6EA9C;
        }

        .col-12 > .descriptionText {
            font-size: 1.5rem;
            margin-top: 5%;
            text-align: center;
        }

        body .containerResized {
            background: #222 !important;
        }

        .navbar {
            background: #20202A !important;
        }

        .main-container {
            background: #363647 !important;
            border-radius: 15px 15px 15px 15px;
            -webkit-box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.3);
            box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.3);
            margin-bottom: 120px !important;
        }

        .card-img-top {
            background-repeat: no-repeat;
            -webkit-background-size: cover;
            background-size: cover;
            width: 27vh;
            height: 24vh;
        }

        .card:hover {
            border: 2px solid #30303F;
            -webkit-box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.3);
            box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.3);
        }

        /*Pagination CSS*/
        html {
            box-sizing: border-box;
        }

        *, *:before, *:after {
            box-sizing: inherit;
        }

        a {
            text-decoration: none;
        }

        .wrapper {
            display: flex;
            align-items: center;
            justify-content: center;
            margin-bottom: 10px;
            width: 100%;
            position: absolute;
            bottom: 70;
        }

        .card > hr {
            margin-right: auto;
            margin-left: auto;
        }

        hr {
            width: 80%;
        }

        .pagination__item {
            display: inline-block;
            margin: 0 10px;
        }

        .pagination__link {
            position: relative;
            text-indent: -99em;
            overflow: hidden;
            display: block;
            width: 30px;
            height: 30px;
        }

        .pagination__link:before, .pagination__link:after {
            content: '';
            display: block;
            position: absolute;
            top: 0;
            width: 100%;
            height: 100%;
            border-radius: 50%;
            border: 3px solid orange;
            transition: all 600ms cubic-bezier(0.68, -0.55, 0.265, 1.55);
        }

        .pagination__link:before {
            background: orange;
            transform: scale(0.2);
        }

        .pagination__link:hover:after {
            transform: scale(1.1);
        }

        .pagination__link.is_active:before {
            transform: scale(0.5);
        }

        .navbar {
            background: #20202A !important;
        }

        .pagination__link.is_active:after {
            transform: scale(0.2);
        }

        .card {
            margin-bottom: 50px;
        }

        /*END Pagination CSS*/

    </style>
</head>
<body style="position: relative; background: #202033 !important;">
<header id="main-header">
    @component('components.nav',['header'=> 'Market ', 'coloredHeader' => 'Place'])
    @endcomponent
</header>
<div id="container">
    <div class="main-container container pt-5 mt-3">
        <div class="row heading tiny-margin">
            <div class="col-12">
                <h1 class="marketPlaceTitle headerMarketplace animation-element slide-down">Get what you <span
                            class="colored">actually</span>  want from packs!</h1>
                @if($type == 'custom')
                    <h5 class=" text-center animation-element slide-down">Custom Packs</h5>
                @else
                    <h5 class=" text-center animation-element slide-down">Preset Packs</h5>
                @endif
            </div>
        </div>
        <div class="container" style="position: relative; margin-bottom: 120px;">

            @if(Session::has('success'))
                <div id="success" class="alert alert-success">
                    {!!  Session::get('success') !!}
                </div>
            @endif
            <div class="row mx-auto card-Container">
                <?php $count = 0 ?>
                @foreach($packs as $pack)
                    @if($count > 2)
                        <div class="card packDesign col-lg-3 "
                             style="width: 20rem;">
                            <div class="card-img-top mx-auto"
                                 style=" background-image: url(/img/packCovers/{{strtoupper($pack->image)}});">
                            </div>
                            <hr>
                            <div class="card-body">
                                <div class="row pl-2">
                                    <p class="card-text col-12"> {{$pack->name}}</p>
                                    @if($console == 'none')
                                        <p class="card-text col-12">Xbox: {{$pack->ps4_price}} Points</p>
                                        <p class="card-text col-12">PS4: {{$pack->xbox_price}} Points</p>
                                    @else
                                        @if($console == 'ps4')
                                            <p class="card-text col-12"> {{$pack->ps4_price}} Points</p>
                                        @else
                                            <p class="card-text col-12"> {{$pack->xbox_price}} Points</p>
                                        @endif
                                    @endif
                                    <a id="show-more" href="{{ url('packs/display/'. $pack->id) }}"
                                       class="card-link select-button col-12 pb-2"
                                    >Show More</a>
                                </div>

                            </div>
                        </div>
                    @else
                        <div class="card packDesign col-lg-3"
                             style="width: 20rem;">
                            <div class="card-img-top mx-auto"
                                 style=" background-image: url(/img/packCovers/{{strtoupper($pack->image)}});">
                            </div>
                            <hr>
                            <div class="card-body">

                                <div class="row pl-2">
                                    <p class="card-text col-12"> {{$pack->name}}</p>
                                    @if($console == 'none')
                                        <p class="card-text col-12">Xbox: {{$pack->ps4_price}} Points</p>
                                        <p class="card-text col-12">PS4: {{$pack->xbox_price}} Points</p>
                                    @else
                                        @if($console == 'ps4')
                                            <p class="card-text col-12"> {{$pack->ps4_price}} Points</p>
                                        @else
                                            <p class="card-text col-12"> {{$pack->xbox_price}} Points</p>
                                        @endif
                                    @endif
                                    <a id="show-more" href="{{ url('packs/display/'. $pack->id) }}"
                                       class="card-link select-button col-12 pb-2"
                                    >Show More</a>
                                </div>


                            </div>
                        </div>
                    @endif
                    <?php $count++ ?>
                @endforeach
            </div>
            <div class="col-12 d-flex justify-content-center">
                {{ $packs->links() }}
            </div>
        </div>
        @component('components.support')
        @endcomponent
    </div>

    @component('components.footer')
    @endcomponent
</div>
<!-- Footer End -->
<script src="/js/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>
<script src="/js/marketplace.js"></script>
<script src="/js/floatingSupport.js"></script>
<script>
    var link = $('.pagination__link');
    link.on('click', function () {
        link.removeClass('is_active');
        $(this).addClass('is_active');
    });
    window.setTimeout(function () {
        $("#success").fadeTo(1000, 0).slideUp(1000, function () {
            $(this).remove();
        });
    }, 5000);
</script>
</body>
</html>
