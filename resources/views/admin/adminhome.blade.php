@extends('layouts.adminmaster')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Dashboard</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a class="custom-link" href="{{ route('admin.dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    @foreach(config('navbar') as $nav)
                        @can('admin-level' . $nav['adminlevel'], Auth::user())
                            <div class="col-lg-6">
                                <a href="{{ route($nav['route']) }}" class="custom-card">
                                    <div class="card">
                                        <div class="card-body custom-card-color">
                                            <h5 class="card-title">{{ $nav['name'] }}</h5>

                                            <p class="card-text">
                                                {{ $nav['description'] }}
                                            </p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endcan
                    @endforeach
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <script src="/js/adminhome.js"></script>
@endsection
