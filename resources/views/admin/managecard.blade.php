@extends('layouts.adminmaster')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Manage cards</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a class="custom-link" href="{{ route('admin.dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item active">Cards</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">

                <!-- Cards pack panel -->

                <?php $ids="" ?>
                @foreach($cards as $card)
                    <?php $ids .= $card->id . ','; ?>
                @endforeach
                <?php $values =substr($ids, 0,-1); ?>

                <cards-container ids={{$values}} manageCards="true" />


            </div>
            <div class="container">
                <div class="justify-content-center mx-auto">
                    {{ $cards->links() }}
                </div>
            </div>

        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <script src="/js/managecard.js"></script>
@endsection