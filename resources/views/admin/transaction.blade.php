@extends('layouts.adminmaster')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Transaction {{ $transaction->id }}</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a class="custom-link" href="{{ route('admin.transactions') }}">Transactions</a></li>
                            <li class="breadcrumb-item active">Transaction</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card custom-card-outline">
                            <div class="card-header">
                                <h5 class="m-0 card-title"># {{ $transaction->number }}</h5>
                            </div>
                            <div class="card-body">
                                <p>User: {{ $transaction->user()->first()->firstname . ' ' . $transaction->user()->first()->lastname }} ({{ $transaction->user()->first()->id }})</p>
                                <p>User status: {{ $transaction->user()->first()->status()->first()->type }}</p>
                                <p>Amount: {{ $transaction->amount }} $</p>
                                <p>Description: {{ $transaction->description }}</p>
                                <p>Payment method: {{ $transaction->payment_method()->first()->method }}</p>
                                <p>Date: {{ $transaction->created_at }}</p>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#noteModal">Leave a note</button>
                            </div>
                        </div>

                        @foreach($transaction->note()->get() as $note)
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="m-0">Written by {{ $note->admin()->first()->firstname . ' ' . $note->admin()->first()->lastname }} on {{ $note->created_at }}</h5>
                                </div>
                                <div class="card-body">
                                    <p class="card-text">{{ $note->note }}</p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Modal -->
    <div class="modal fade" id="noteModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Note</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="noteForm" role="form" method="post" action="{{ action('Admin\AdminTransactionController@addNote') }}">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <textarea form="noteForm" class="form-control" name="note" rows="3"></textarea>
                        <input type="hidden" value="{{ $transaction->id }}" name="transactionId" />
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="clearForm()">Cancel</button>
                        <button type="submit" class="btn btn-primary">Write</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        function clearForm() {
            document.getElementById("noteForm").reset();
        }
    </script>
@endsection
