@extends('layouts.adminmaster')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Admin Form</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a class="custom-link" href="{{ route('admin.dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item active">Form</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <form id="adminForm" class="needs-validation" role="form" method="post" action="{{ action('Admin\AdminController@createAdmin') }}" novalidate>
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" required>
                        <div class="invalid-feedback">
                            Invalid email address
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control mb-1" id="password" name="password" placeholder="Password" required>
                        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" data-pass-conf="password" placeholder="Confirm password" required>
                        <small class="form-text text-muted">It must be at least 8 characters in length.</small>
                        <div id="password-error" class="invalid-feedback">
                            Invalid password
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="firstname">First name</label>
                        <input type="text" class="form-control" id="firstname" name="firstname" placeholder="First name" required>
                        <div class="invalid-feedback">
                            Please enter a first name
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="lastname">Last name</label>
                        <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Last name" required>
                        <div class="invalid-feedback">
                            Please enter a last name
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="admintype">Admin level</label>
                        <select id="admintype" name="admintype" class="custom-select" required>
                            <option value="" selected>Open this select menu</option>
                            <option value="1">One (highest)</option>
                            <option value="2">Two</option>
                            <option value="3">Three (lowest)</option>
                        </select>
                        <div class="invalid-feedback">
                            Please select an admin level
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary mb-1">Create</button>
                </form>
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <script>
        @if($errors->any())
            @foreach ($errors->all() as $error)
                $.notify({
                    message: "{{$error}}"
                }, {
                    type: 'warning',
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutRight'
                    }
                }).show();
            @endforeach
        @endif
    </script>
    <script src="/js/adminform.js"></script>
@endsection