@extends('layouts.adminmaster')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Users</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a class="custom-link" href="{{ route('admin.dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item active">Users</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <!-- Search engine -->
                <div class="form-group">
                    <form class="form-inline input-group">
                        {{csrf_field()}}
                        <input class="form-control" type="search" placeholder="Search">
                        <div class="input-group-append">
                            <button class="input-group-btn btn btn-navbar" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </form>
                </div>

                <table class="table table-bordered table-striped table-dark">
                    <thead>
                    <tr>
                        <th scope="col">User #</th>
                        <th scope="col">Email</th>
                        <th scope="col">First name</th>
                        <th scope="col">Last name</th>
                        <th scope="col">Coins</th>
                        <th scope="col">Status</th>
                        <th scope="col">Created</th>
                        <th scope="col">Modify status</th>
                        <th scope="col">Details</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($users as $user)
                        <tr>
                            <th scope="row">{{ $user->id }}</th>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->firstname }}</td>
                            <td>{{ $user->lastname }}</td>
                            <td>{{ $user->coins }}</td>
                            <td id="user-{{ $user->id }}-status">{{ $user->status()->first()->type }}</td>
                            <td>{{ $user->created_at }}</td>
                            <td>
                                <div class="input-group w-100">
                                    <button type="button" class="btn btn-success mr-2 verify-user-btn" value="{{ $user->id }}">Verify</button>
                                    <button type="button" class="btn btn-danger unverify-user-btn" value="{{ $user->id }}">Unverify</button>
                                </div>
                            </td>
                            <td><a class="custom-link" href="{{ route('admin.user', ['id' => $user->id]) }}">Details</a></td>
                        </tr>
                    @empty
                        <tr class="no-data">
                            <td align="center" colspan="9">No data found.</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
                {{ $users->links() }}
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <script src="/js/users.js"></script>
@endsection
