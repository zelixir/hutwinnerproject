@extends('layouts.adminmaster')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Withdraws</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a class="custom-link" href="{{ route('admin.dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item active">Withdraws</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">

                <!-- Search engine -->
                <div class="form-group">
                    <form class="form-inline input-group">
                        {{csrf_field()}}
                        <input class="form-control" type="search" placeholder="Search">
                        <div class="input-group-append">
                            <button class="input-group-btn btn btn-navbar" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </form>
                </div>

                <table class="table table-bordered table-striped table-dark">
                    <thead>
                    <tr>
                        <th scope="col">Withdraw #</th>
                        <th scope="col">User</th>
                        <th scope="col">Card</th>
                        <th scope="col">Amount</th>
                        <th scope="col">Status</th>
                        <th scope="col">Date</th>
                        <th scope="col">Details</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($withdraws as $withdraw)
                        <tr>
                            <th scope="row">{{ $withdraw->id }}</th>
                            <td>{{ $withdraw->user_id }}</td>
                            <td>{{ $withdraw->card_id }}</td>
                            <td>{{ $withdraw->amount }}</td>
                            <td>{{ $withdraw->status()->first()->type }}</td>
                            <td>{{ $withdraw->created_at }}</td>
                            <td><a class="custom-link" href="{{ route('admin.withdraw', ['id' => $withdraw->id]) }}">Details</a></td>
                        </tr>
                    @empty
                        <tr class="no-data">
                            <td align="center" colspan="7">No data found.</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
                {{ $withdraws->links() }}
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
