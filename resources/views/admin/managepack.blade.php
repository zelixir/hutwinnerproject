@extends('layouts.adminmaster')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Manage packs</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a class="custom-link" href="{{ route('admin.dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item active">Packs</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">

                <!-- Cards pack panel -->
                <div class="row flex-wrap">
                    @foreach($packs as $pack)
                        <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                            <div id="{{ 'pack-' . $pack->id }}" class="card card-height">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active custom-link" id="home-tab" data-toggle="tab" href="{{ '#image-' . $pack->id }}" role="tab">Home</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link custom-link" id="profile-tab" data-toggle="tab" href="{{ '#details-' . $pack->id }}" role="tab">Details</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link custom-link" href="{{ route('admin.packview', ['id' => $pack->id]) }}" role="tab"><i class="fas fa-edit"></i></a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade show active" id="{{ 'image-' . $pack->id }}" role="tabpanel" >
                                        <div class="card-body">
                                            <img src="..." class="card-img-top" alt="...">
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="{{ 'details-' . $pack->id }}" role="tabpanel">
                                        <div class="card-body">
                                            <div class="d-flex justify-content-between">
                                                <p class="card-text m-0">Pack ID:</p>
                                                <p class="card-text m-0">{{ $pack->id }}</p>
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <p class="card-text m-0">Name:</p>
                                                <p class="card-text m-0">{{ $pack->name }}</p>
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <p class="card-text m-0">Author:</p>
                                                <p class="card-text m-0">{{ $pack->user()->count() > 0 ? $pack->user()->first()->firstname . ' ' . $pack->user()->first()->lastname : 'admin' }}</p>
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <p class="card-text m-0">Number of cards:</p>
                                                <p class="card-text m-0">{{ $pack->cards()->count() }}</p>
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <p class="card-text m-0">Guaranteed cards:</p>
                                                <p class="card-text m-0">{{ $pack->guaranteed_cards }}</p>
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <p class="card-text m-0">Created at:</p>
                                                <p class="card-text m-0">{{ $pack->created_at->format('Y-M-d') }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="row">
                    {{ $packs->links() }}
                </div>
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection