@extends('layouts.adminmaster')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Create card</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a class="custom-link" href="{{ route('admin.dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item active">Card</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <form id="cardForm" class="needs-validation" role="form" method="post" action="{{ action('Admin\CardPackController@createCard') }}" novalidate>
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="cardtype">Card type</label>
                        <select id="cardtype" name="cardtype" class="custom-select" required>
                            <option value="" selected>Open this to select a type</option>
                            @foreach($types as $type)
                                <option value="{{ $type->id }}">{{ $type->type }}</option>
                            @endforeach
                        </select>
                        <div class="invalid-feedback">
                            Please select a card type
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="league">League</label>
                        <select id="league" name="league" class="custom-select" required>
                            <option value="" selected>Open this to select a league</option>
                            @foreach(config('league') as $league)
                                <option value="{{ $league }}">{{ $league }}</option>
                            @endforeach
                        </select>
                        <div class="invalid-feedback">
                            Please select a league
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="team">Team</label>
                        <select id="team" name="team" class="custom-select" required>
                            <option value="" selected>Open this to select a team</option>
                            @foreach(config('team') as $team)
                                <option value="{{ $team }}">{{ $team }}</option>
                            @endforeach
                        </select>
                        <div class="invalid-feedback">
                            Please select a team
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="firstname">Player first name</label>
                        <input type="text" class="form-control" id="firstname" name="firstname" placeholder="First name" required>
                        <div class="invalid-feedback">
                            Please enter a first name
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="lastname">Player last name</label>
                        <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Last name" required>
                        <div class="invalid-feedback">
                            Please enter a last name
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="position">Player position</label>
                        <select id="position" name="position" class="custom-select" required>
                            <option value="" selected>Open this to select a position</option>
                            @foreach(config('position') as $position)
                                <option value="{{ $position }}">{{ $position }}</option>
                            @endforeach
                        </select>
                        <div class="invalid-feedback">
                            Please select a position
                        </div>
                    </div>
                    <div class="form-group" style="height: auto;">
                        <label for="synergy[]">Player synergy</label>
                        <div id="synergyGroup" class="form-control h-auto">
                            <div class="row">
                                @foreach(config('synergy') as $synergy)
                                    <div class="input-group col-6 col-sm-3 col-md-2 col-lg-2 m-1">
                                        <input class="input-control col-6 mr-1 pr-0" type="number" onKeyPress="if(this.value.length > 0) return false;" name="synergy[{{ $synergy }}]">
                                        <label class="form-check-label" for="synergy[{{ $synergy }}]">{{ $synergy }}</label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="invalid-feedback">
                            Please select enter at least one synergy value
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="overall">Player overall</label>
                        <input type="number" onKeyPress="if(this.value.length > 1) return false;" class="form-control" id="overall" name="overall" placeholder="Player overall" required>
                        <div class="invalid-feedback">
                            Please enter an overall value
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="xboxprice">Xbox price</label>
                        <input type="number" class="form-control" id="xboxprice" name="xboxprice" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" placeholder="Player xbox price" required>
                        <div class="invalid-feedback">
                            Please enter a price value
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="ps4price">PS4 price</label>
                        <input type="number" class="form-control" id="ps4price" name="ps4price" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" placeholder="Player ps4 price" required>
                        <div class="invalid-feedback">
                            Please enter a price value
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="year">Year</label>
                        <select id="year" name="year" class="custom-select" required>
                            <option value="{{ date("Y",strtotime("-1 year")) }}">{{ date("Y",strtotime("-1 year")) }}</option>
                            <option value="{{ date("Y") }}" selected>{{ date("Y") }}</option>
                            <option value="{{ date("Y",strtotime("+1 year")) }}">{{ date("Y",strtotime("+1 year")) }}</option>
                        </select>
                        <div class="invalid-feedback">
                            Please enter a year
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary mb-1">Create</button>
                </form>
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <script>
        @if($errors->any())
            @foreach ($errors->all() as $error)
                $.notify({
                    message: "{{$error}}"
                }, {
                    type: 'warning',
                    animate: {
                        enter: 'animated fadeInUp',
                        exit: 'animated fadeOutRight'
                    }
                }).show();
            @endforeach
        @endif
    </script>
    <script src="/js/createcard.js"></script>
@endsection