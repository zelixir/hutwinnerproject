@extends('layouts.adminmaster')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Create pack</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a class="custom-link" href="{{ route('admin.dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item active">Pack</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <form class="needs-validation" role="form" method="post" action="{{ action('Admin\CardPackController@packFind') }}" novalidate>
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="packname">Pack name</label>
                        <input type="text" class="form-control" id="packname" name="packname" placeholder="Pack name" required>
                        <div class="invalid-feedback">
                            Please enter a pack name
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="xboxpackprice">XBOX pack price</label>
                        <input type="number" class="form-control" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" id="xboxpackprice" name="xboxpackprice" placeholder="Pack price" required>
                        <div class="invalid-feedback">
                            Please enter a price
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="ps4packprice">PS4 pack price</label>
                        <input type="number" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" class="form-control" id="ps4packprice" name="ps4packprice" placeholder="Pack price" required>
                        <div class="invalid-feedback">
                            Please enter a price
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="guaranteedcard">Guaranteed number of cards</label>
                        <input type="number" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" class="form-control" id="guaranteedcard" name="guaranteedcard" placeholder="Number of cards" required>
                        <div class="invalid-feedback">
                            Please enter a price
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="team[]">Teams</label>
                        <div class="form-control h-auto">
                            <div class="row">
                                @foreach(config('team') as $team)
                                    <div class="form-check form-check-inline col-4 col-sm-3 col-md-2 col-lg-1 ml-2">
                                        <input class="form-check-input" type="checkbox" name="team[]" value="{{ $team }}">
                                        <label class="form-check-label" for="team[]">{{ $team }}</label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-check text-right">
                            <input id="allChkbox" class="form-check-input" type="checkbox" value="1" name="all">
                            <label class="form-check-label" for="all">
                                All
                            </label>
                        </div>
                        <div id="criteriaSetting">
                            @foreach(config('league') as $league)
                                <div class="form-group">
                                    <label for="league[][]">{{ $league }}</label>
                                    <div class="form-control h-auto">
                                        <div class="row">
                                        @foreach($types as $type)
                                            <div class="form-check form-check-inline col-4 col-sm-3 col-md-2 col-lg-1 ml-2">
                                                <input class="form-check-input" type="checkbox" name="league[{{ $league }}][]" value="{{ $type->id }}">
                                                <label class="form-check-label" for="league[{{$league}}][]">{{ $type->type }}</label>
                                            </div>
                                        @endforeach
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary mb-1">Create</button>
                </form>
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <script>
        @if($errors->any())
        @foreach ($errors->all() as $error)
        $.notify({
            message: "{{$error}}"
        }, {
            type: 'warning',
            animate: {
                enter: 'animated fadeInUp',
                exit: 'animated fadeOutRight'
            }
        }).show();
        @endforeach
        @endif
    </script>
    <script src="/js/createpackform.js"></script>
@endsection