@extends('layouts.adminmaster')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Transactions</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a class="custom-link" href="{{ route('admin.dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item active">Transactions</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">

                <!-- Search engine -->
                <div class="form-group">
                    <form class="form-inline input-group">
                        {{csrf_field()}}
                        <input class="form-control" type="search" placeholder="Search">
                        <div class="input-group-append">
                            <button class="input-group-btn btn btn-navbar" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </form>
                </div>

                <table id="transactions-table" class="table table-bordered table-striped table-dark">
                    <thead>
                        <tr>
                            <th scope="col">Transaction #</th>
                            <th scope="col">User</th>
                            <th scope="col">Amount</th>
                            <th scope="col">Description</th>
                            <th scope="col">Payment</th>
                            <th scope="col">Date</th>
                            <th scope="col">Details</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($transactions as $transaction)
                            <tr>
                                <th scope="row">{{ $transaction->number }}</th>
                                <td>{{ $transaction->user_id }}</td>
                                <td>{{ $transaction->amount }}</td>
                                <td>{{ $transaction->description }}</td>
                                <td>{{ $transaction->payment_method()->first()->method }}</td>
                                <td>{{ $transaction->created_at }}</td>
                                <td><a class="custom-link" href="{{ route('admin.transaction', ['id' => $transaction->id]) }}">Details</a></td>
                            </tr>
                        @empty
                            <tr class="no-data">
                                <td align="center" colspan="7">No data found.</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                {{ $transactions->links() }}
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
