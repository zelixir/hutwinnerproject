@extends('layouts.adminmaster')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Pack view</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a class="custom-link" href="{{ route('admin.displaypacks') }}">Packs</a></li>
                            <li class="breadcrumb-item active">Pack view</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <form id="cardList" class="needs-validation" role="form" method="post" action="{{ action('Admin\CardPackController@modifyPack') }}" novalidate>
                    @csrf
                    <div id="hidden-input-data" class="d-none">
                        <input type="hidden" name="packid" value="{{ $pack->id }}">
                    </div>
                    <div class="form-group">
                        <label for="packname">Pack name</label>
                        <input type="text" class="form-control" id="packname" name="packname" value="{{ $pack->name }}" required>
                        <div class="invalid-feedback">
                            Please enter a pack name
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="xboxprice">XBOX pack price</label>
                        <input id="xboxprice" name="xboxprice" type="number" class="form-control price-input" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" value="{{ $pack->xbox_price }}" required>
                        <div class="invalid-feedback">
                            Please enter a price
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="ps4price">PS4 pack price</label>
                        <input id="ps4price" name="ps4price" type="number" class="form-control price-input" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" value="{{ $pack->ps4_price }}" required>
                        <div class="invalid-feedback">
                            Please enter a price
                        </div>
                    </div>
                    <table id="cardViewTable" class="table table-bordered table-striped table-dark">
                        <thead>
                        <tr>
                            <th scope="col">Player #</th>
                            <th scope="col">Type</th>
                            <th scope="col">League</th>
                            <th scope="col">Team</th>
                            <th scope="col">Player name</th>
                            <th scope="col">Synergy</th>
                            <th scope="col">Overall</th>
                            <th scope="col">Position</th>
                            <th scope="col">Xbox price</th>
                            <th scope="col">PS4 price</th>
                            <th scope="col">Probability</th>
                            <th scope="col">Remove</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($pack->cards()->get() as $player)
                            <tr id="cardView-{{ $player->id }}">
                                <th scope="row">{{ $player->id }}</th>
                                <td>{{ $player->type()->first()->type }}</td>
                                <td>{{ $player->league }}</td>
                                <td>{{ $player->team }}</td>
                                <td>{{ $player->player_name }}</td>
                                <td>{{ $player->synergy }}</td>
                                <td>{{ $player->overall }}</td>
                                <td>{{ $player->position }}</td>
                                <td>{{ $player->xbox_price }}</td>
                                <td>{{ $player->ps4_price }}</td>
                                <td>
                                    <div class="input-group w-100">
                                        <input type="number" class="form-control w-75 probability-input" step=".0001" name="card[{{ $player->id }}]" value="{{ $card_pack->where('card_id', $player->id)->first()->probability }}" required>
                                        <div class="input-group-append w-25">
                                            <span class="input-group-text">%</span>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group w-100">
                                        <button type="button" class="btn btn-danger card-remove-btn">Remove</button>
                                    </div>
                                </td>
                            </tr>
                        @empty
                            <tr class="no-data">
                                <td align="center" colspan="10">No data found.</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-6 text-left">
                            <button type="button" class="btn btn-success mb-1" data-toggle="modal" data-target="#addCardModal">Add card(s)</button>
                        </div>
                        <div class="col-6 text-right">
                            <button type="submit" class="btn btn-primary mb-1">Save changes</button>
                        </div>
                    </div>
                </form>

                <!-- Modal -->
                <div class="modal fade" id="addCardModal" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Add card(s)</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form id="modal-add-form">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-search"></i></span>
                                    </div>
                                    <input id="nameInput" type="text" class="form-control" onkeyup="search()" placeholder="Search for player names,..">
                                </div>
                                <div class="custom-scrollable-table">
                                    <table id="cardsTable" class="table table-hover table-dark">
                                        <thead>
                                        <tr>
                                            <th scope="col">Select</th>
                                            <th scope="col">Player #</th>
                                            <th scope="col">Type</th>
                                            <th scope="col">League</th>
                                            <th scope="col">Team</th>
                                            <th scope="col">Player name</th>
                                            <th scope="col">Synergy</th>
                                            <th scope="col">Overall</th>
                                            <th scope="col">Position</th>
                                            <th scope="col">Xbox price</th>
                                            <th scope="col">PS4 price</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($cards as $card)
                                            <tr id="card-{{ $card->id }}">
                                                <td class="input-id"><input type="checkbox" class="select-card-chkbox" name="ids" value="{{ $card->id }}"></td>
                                                <th scope="row">{{ $card->id }}</th>
                                                <td>{{ $card->type()->first()->type }}</td>
                                                <td>{{ $card->league }}</td>
                                                <td>{{ $card->team }}</td>
                                                <td class="player-name">{{ $card->player_name }}</td>
                                                <td>{{ $card->synergy }}</td>
                                                <td>{{ $card->overall }}</td>
                                                <td>{{ $card->position }}</td>
                                                <td>{{ $card->xbox_price }}</td>
                                                <td>{{ $card->ps4_price }}</td>
                                            </tr>
                                        @endforeach
                                            <tr id="cards-search-notfound" class="no-data">
                                                <td align="center" colspan="11">No data found.</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="addRow()">Add<span id="card-select-count" class="badge badge-success ml-1"></span></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <script>
        @if($errors->any())
        @foreach ($errors->all() as $error)
        $.notify({
            message: "{{$error}}"
        }, {
            type: 'warning',
            animate: {
                enter: 'animated fadeInUp',
                exit: 'animated fadeOutRight'
            }
        }).show();
        @endforeach
        @endif
    </script>
    <script src="/js/singlepack.js"></script>
@endsection
