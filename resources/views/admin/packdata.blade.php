@extends('layouts.adminmaster')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Results</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a class="custom-link" href="{{ route('admin.createpack') }}">Create pack</a></li>
                            <li class="breadcrumb-item active">Results</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <form class="needs-validation" role="form" method="post" action="{{ action('Admin\CardPackController@createPack') }}" novalidate>
                    @csrf
                    <input type="hidden" name="packname" value="{{ $packname }}">
                    <input type="hidden" name="xboxprice" value="{{ $xboxprice }}">
                    <input type="hidden" name="ps4price" value="{{ $ps4price }}">
                    <input type="hidden" name="guaranteedcard" value="{{ $guaranteedcard }}">
                    <table class="table table-bordered table-striped table-dark">
                        <thead>
                        <tr>
                            <th scope="col">Player #</th>
                            <th scope="col">Type</th>
                            <th scope="col">League</th>
                            <th scope="col">Team</th>
                            <th scope="col">Player name</th>
                            <th scope="col">Overall</th>
                            <th scope="col">Position</th>
                            <th scope="col">Xbox price</th>
                            <th scope="col">PS4 price</th>
                            <th scope="col">Probability</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($players as $player)
                            <tr>
                                <th scope="row">{{ $player->id }}</th>
                                <td>{{ $player->type()->first()->type }}</td>
                                <td>{{ $player->league }}</td>
                                <td>{{ $player->team }}</td>
                                <td>{{ $player->player_name }}</td>
                                <td>{{ $player->overall }}</td>
                                <td>{{ $player->position }}</td>
                                <td>{{ $player->xbox_price }}</td>
                                <td>{{ $player->ps4_price }}</td>
                                <td>
                                    <div class="input-group w-100">
                                        <input type="number" class="form-control w-75 probability-input" name="card[{{ $player->id }}]" required>
                                        <div class="input-group-append w-25">
                                            <span class="input-group-text">%</span>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @empty
                            <tr class="no-data">
                                <td align="center" colspan="10">No data found.</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    <button type="submit" class="btn btn-primary mb-1">Create pack</button>
                </form>
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <script>
        @if($errors->any())
        @foreach ($errors->all() as $error)
        $.notify({
            message: "{{$error}}"
        }, {
            type: 'warning',
            animate: {
                enter: 'animated fadeInUp',
                exit: 'animated fadeOutRight'
            }
        }).show();
        @endforeach
        @endif
    </script>
    <script src="/js/packdata.js"></script>
@endsection
