<html xmlns="http://www.w3.org/1999/html">
<head>

    <!-- META TAGS -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>HUTWINNER</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <!-- OG meta tags that improve the look of your post on social media -->
    <meta property="og:site_name" content=""/><!--website name-->
    <meta property="og:site" content=""/> <!--website link-->
    <meta property="og:title" content=""/> <!--Post title-->
    <meta property="og:description" content=""/> <!--Post description-->
    <meta property="og:image" content=""/><!-- Image link (jpg only)-->
    <meta property="og:url" content=""/> <!--where do you want your post to link to-->
    <meta property="og:type" content="article"/>
    <link rel="apple-touch-icon" sizes="57x57" href="images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/MarketPlace.css') }}">
    <link rel="stylesheet" href="{{asset('css/2019_HUT_Style.css')}}">
    <link rel="stylesheet" href="{{asset('css/nav.css')}}">

    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="/css/floatingSupport.css" rel="stylesheet">
    <script src="https://cdn.rawgit.com/webcomponents/webcomponentsjs/v0.7.24/webcomponents-lite.js"></script>

    <style>
        .link-MarketPlace {
            text-align: center;
            font-style: italic;
            font-size: 68px !important;
            letter-spacing: 2.05px;
            text-transform: uppercase;
            font-weight: 400;
            font-family: Steelfish;
        }
        input[type='text'],input[type='number'], select {
            background: rgba(0, 0, 0, 0.5) !important;
            border-radius: 15px 15px 15px 15px;
            color: white !important;
            border: 1px solid #F6EA9C !important;
        }
        #nextPageBtn {
            width: 25%;
            border-radius: 30px;
        }

        img {
            vertical-align: middle;
            border-style: none;
            width: 100%;
        }

        .btn-light {
            background-color: #F6EA9C;
            color: #000;
            font-style: italic;
            border: unset;
        }

        h1 {
            font-family: unset;
        }

        a:visited {
            color: #F6EA9C;
        }

        img {
            width: 100%;
            height: 100%;
        }

        .element-Container {
            background: #30303F !important;
            border-bottom-left-radius: unset !important;
            border-bottom-right-radius: unset !important;
            border-top-left-radius: 20px;
            border-top-right-radius: 20px;
        }

        /*ml-5 columnValue*/
        .element-ContainerSecond {
            padding-left: 5%;
            background: #30303F !important;
            border: 2px solid #F6EA9C !important;;
            -webkit-border-radius: 20px;
            -moz-border-radius: 20px;
            border-radius: 20px;
        }

        a {
            color: #F6EA9C;
        }

        @media (min-width: 1200px) {
            .container {
                max-width: 95%;
            }
        }

        body .containerResized {
            background: #222 !important;
        }

        .navbar {
            background: #20202A !important;
        }

        .main-container {
            background: #363647 !important;
            border-radius: 15px 15px 15px 15px;
            -webkit-box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.3);
            box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.3);
            margin-bottom: 120px !important;
        }

        .card-img-top {
            background: url(/img/hutbd-pack-icons/x7.jpg.pagespeed.ic.8-NtslfRji.jpg);
            background-repeat: no-repeat;
            -webkit-background-size: cover;
            background-size: cover;
            height: 300px;
            width: 300px;
            display: inline-block;
        }

        .columnValue {
            min-height: 342px;
            min-width: 242px;
            padding: unset;
        }

        .clickedCard {
            border: 5px solid #F6EA9C;
            border-radius: 10px;
        }

        /*END Pagination CSS*/

    </style>
</head>
<body style="position: relative; background: #202033 !important;">
<header id="main-header">
    @component('components.nav',['header'=> 'Market ', 'coloredHeader' => 'Place'])
    @endcomponent
</header>
<div id="container">
    <div class="main-container container pt-5 mt-3">
        @if(Session::has('success'))
            <div class="alert alert-success">
                <p>{!! Session::get('success') !!}</p>
            </div>
        @endif
        @if($errors->any())
            <div class="alert alert-danger">
                <p>{{ $errors->first() }}</p>
            </div>
        @endif
        <div class="row tiny-margin">
            <div class="col-12 text-center">
                @if( ! empty($cards) && isset($cards))
                    <h1 class="marketPlaceTitle headerMarketplace animation-element slide-down"><span
                                class="colored">MAKE A REQUEST</span></h1>
                    <p style="font-size: 1.5em; padding-bottom: 3%">Note that you can't select more than 4 cards per
                        withdrawl</p>
                    <button id="nextPageBtn" type="button" class="btn btn-success btn-lg mb-5" disabled
                            data-toggle="modal"
                            data-target="#exampleModal">NEXT
                    </button>
                @else
                    <h1 class="marketPlaceTitle headerMarketplace animation-element slide-down text-center col-12"><span
                                class="colored text-center">You don't have any cards! <br><a
                                    href="{{route('packs.marketplace')}}" class="link-MarketPlace text-center col-12">Click Here to Buy a Pack!</a></span>
                    </h1>

                @endif
            </div>
            <div class="col-12">
                @if( isset($cards))
                    <?php $value = join(',', array_map(function ($val) {
                        return $val['id'];
                    }, $cards->toArray())) ?>
                    <div class="container">
                        <div class="row element-ContainerSecond">
                            <cards-container ids="{{$value}}" withdraw="true"/>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
    @component('components.support')
    @endcomponent
    @component('components.footer')
    @endcomponent
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Your Withdraw Request:</h4>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <form action="{{ route('user.post.withdraw')}}" method="POST">
                    @csrf
                    <h4 class="modal-title" id="exampleModalLabel">Enter you HUT information for the trade offer:</h4>
                    <div class="row p-3">
                        <label for="player_name" class="col-4 mb-2">Player Name</label>
                        <input type="text" class="col-8 mb-2" id="player_name" name="player_name" required> <br>
                        <label for="hutTeamName" class="col-4 mb-2">Team Name</label>
                        <input type="text" name="hutTeamName" class="col-8 mb-2" id="hutTeamName" value="{{ \Illuminate\Support\Facades\Auth::user()->hut_team }}" disabled required> <br>
                        <label for="start_price" class="col-4 mb-2">Start Price</label>
                        <input name="start_price"  class="col-8 mb-2"  type="number" id="start_price" required> <br>
                        <label for="buy_now" class="col-4 mb-2">Buy Now Price</label>
                        <input name="buy_now" type="number" id="buy_now" required class="col-8 mb-2"> <br>
                        <label for="transfer_duration" class="col-4 mb-2">Transfer Duration</label>
                        <input name="transfer_duration" id="transfer_duration" value="3 Days" type="text" disabled required class="col-8 mb-2">
                        <br>
                    </div>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Confirm</button>
                </form>

            </div>
        </div>
    </div>
</div>
<!-- Footer End -->
<script src="../../js/app.js"></script>
<script src="/js/search.js"></script>
<script src="/js/sellPopup.js"></script>
<script src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>
<script src="/js/withdraw.js"></script>
<script src="/js/floatingSupport.js"></script>
</body>
</html>
