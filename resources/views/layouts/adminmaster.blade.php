<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>CSE</title>

    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="{{ asset('css/2019_HUT_Style.css') }}">
</head>
<body class="hold-transition sidebar-mini">
<script src="/js/app.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/mouse0270-bootstrap-notify/3.1.7/bootstrap-notify.min.js"></script>
<script src="/js/adminmaster.js"></script>
<div class="wrapper">
    <!-- Navbar -->
    <nav id="mainNavbar" class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="{{ route('admin.dashboard') }}" class="nav-link">Home</a>
            </li>
        </ul>

        <!-- SEARCH FORM -->
        {{--<form class="form-inline ml-3">--}}
        {{--<div class="input-group input-group-sm">--}}
        {{--<input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">--}}
        {{--<div class="input-group-append">--}}
        {{--<button class="btn btn-navbar" type="submit">--}}
        {{--<i class="fa fa-search"></i>--}}
        {{--</button>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</form>--}}
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="{{ route('admin.dashboard') }}" class="brand-link">
            {{--TODO: add logo here--}}
            {{--<img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"--}}
            {{--style="opacity: .8">--}}
            <div class="span12 text-center">
                <span class="brand-text font-weight-light">ADMIN</span>
            </div>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                    data-accordion="false">
                    <li class="nav-item has-treeview menu">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-user"></i>
                            <p>
                                {{ Auth::guard('admin')->user()->firstname . ' ' . Auth::guard('admin')->user()->lastname }}
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            {{--<li class="nav-item">--}}
                                {{--<a href="#" class="nav-link">--}}
                                    {{--<i class="fas fa-info-circle"></i>--}}
                                    {{--<p>Profile</p>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            <li class="nav-item">
                                <a href="{{ route('admin.logout') }}" class="nav-link">
                                    <i class="fas fa-sign-out-alt"></i>
                                    <p>Logout</p>
                                </a>
                            </li>
                        </ul>
                    </li>

                </ul>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                    data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->

                    @foreach(config('navbar') as $nav)
                        @can('admin-level' . $nav['adminlevel'], Auth::user())
                            <li class="nav-item">
                                <a href="{{ route($nav['route']) }}" class="nav-link">
                                    <i class="nav-icon fas {{ $nav['icon'] }}"></i>
                                    <p>
                                        {{ $nav['name'] }}
                                    </p>
                                </a>
                            </li>
                        @endcan
                    @endforeach
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Notifications -->
    @include('layouts.notification')

    @yield('content')

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="float-right d-none d-sm-inline">
            Anything you want
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2014-2018 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
    </footer>
</div>
<!-- ./wrapper -->

</body>
</html>
