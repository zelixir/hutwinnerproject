<script>
    @if(Session::has('success'))
        $.notify({
            message: "{{ Session::get('success') }}"
        }, {
            type: 'success',
            placement: {
                from: "bottom"
            }
        }).show();
        @php
            Session::forget('success');
        @endphp
    @endif

    @if(Session::has('info'))
        $.notify({
            message: "{{ Session::get('info') }}"
        }, {
            type: 'info',
            placement: {
                from: "bottom"
            }
        }).show();
        @php
            Session::forget('info');
        @endphp
    @endif

    @if(Session::has('warning'))
        $.notify({
            message: "{{ Session::get('warning') }}"
        }, {
            type: 'warning',
            placement: {
                from: "bottom"
            }
        }).show();
        @php
            Session::forget('warning');
        @endphp
    @endif

    @if(Session::has('error'))
        $.notify({
            message: "{{ Session::get('error') }}"
        }, {
            type: 'danger',
            placement: {
                from: "bottom"
            }
        }).show();
        @php
            Session::forget('error');
        @endphp
    @endif
</script>