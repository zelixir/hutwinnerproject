<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <!-- META TAGS -->
    <meta charset="utf-8">
    <meta name="author" content="AtypicalThemes">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- WEBSITE TITLE & DESCRIPTION -->
    <meta name="description" content="">
    <meta name="keywords" content="">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="{{ asset('css/2019_HUT_Style.css') }}" rel="stylesheet">

    <!-- OG meta tags that improve the look of your post on social media -->
    <meta property="og:site_name" content=""/><!--website name-->
    <meta property="og:site" content=""/> <!--website link-->
    <meta property="og:title" content=""/> <!--Post title-->
    <meta property="og:description" content=""/> <!--Post description-->
    <meta property="og:image" content=""/><!-- Image link (jpg only)-->
    <meta property="og:url" content=""/> <!--where do you want your post to link to-->
    <meta property="og:type" content="article"/>
    <link rel="apple-touch-icon" sizes="57x57" href="images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <link href="{{ asset('css/nav.css') }}" rel="stylesheet">


    <style>

        .card {
            background: unset !important;
            border: unset !important;
        }

        .button-submit {
            background-color: #F6EA9C !important;
            border-radius: 5px;
            width: 82% !important;
            border: unset;
            color: black !important;
        }

        html {
            background: #202033 !important;
        }

        .forgetPassword {
            text-align: left !important;
            color: white;
            width: 100%;
            margin-left: 10% !important;
            padding-left: unset !important;
        }

        body {
            background: unset !important;
            background: #202033 !important;
        }
        .card {
            box-shadow: unset !important;
        }
        @font-face {
            font-family: 'bebas';
            src: url("/fonts/bebas/BebasNeue.otf");
        }

        @media (min-width: 768px) {
            .login-form {
                border-top: 1px solid #F6EA9C;
                border-left: unset;
            }

            .card {
                margin-top: 0px;
            }

            .login-header {
                margin-top: 100px;
            }
        }

        @media (min-width: 992px) {
            .card {
                margin-top: 10px;
                background: #363647 !important;
                border: unset !important;
                border-radius: unset !important;
            }

            .login-form {
                border-top: unset !IMPORTANT;
                border-left: 1px solid #F6EA9C !important;
            }

            .login-main-text h2 {
                font-weight: 300;
            }
        }

        @media (min-width: 576px) {
            .login-header {
                margin-top: 100px;
            }

            .login-form {
                border-top: 1px solid #F6EA9C;
                border-left: unset;
            }
        }

        @media (min-width: 992px) {
            .login-header {
                margin-top: 150px;
            }
        }

        .login-header {
            margin-top: 100px;
        }

        .container {
            height: 650px;
            max-width: 975px;
            margin-top: 10px;
        }

        input:focus {
            -webkit-box-shadow: none !important;
            box-shadow: none !important;
        }

        input[type="email"] {
            border: unset !important;
            border-bottom: 2px solid #F6EA9C !important;
            color: white !important;
            background: #30303F !important;
        }

        input[type="text"] {
            border: unset !important;
            border-bottom: 2px solid #F6EA9C !important;
            color: white !important;
            background: #30303F !important;
        }

        input[type="password"] {
            border: unset !important;
            border-bottom: 2px solid #F6EA9C !important;
            color: white !important;
            background: #30303F !important;
        }

        input[type='checkbox'] {
            width: 13px !important;
            height: 13px !important;
            padding: 0 !important;
            margin: 0 !important;
            vertical-align: bottom !important;
            position: relative !important;
            top: -1px !important;
            *overflow: hidden !important;
        }

        input {
            margin-left: 10% !important;
            width: 80% !important;
        }

        #remember {
            position: relative;
            margin-bottom: 4px !important;
        }

        #register-container {
            border-radius: 25px;
            background: #363647 !important;
        }

        @media (min-width: 576px) {
            .col-sm-12 {
                -ms-flex: 0 0 100%;
                flex: 0 0 100%;
                max-width: 100%;
            }
        }
        .marketPlaceTitle{
            font-size: 82px !important;
            font-weight: 400;
            height: 120px;
            color: #ffffff;
            font-family: Steelfish;
            font-size: 40px !important;
            font-style: italic;
            letter-spacing: 2.05px;
            text-transform: uppercase;
        }
    </style>

    <!-- STYLES -->
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- FontAwesome -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <!-- Animations -->
    <link href="css/animations.css" rel="stylesheet">
    <!-- Lightbox -->
    <link href="css/lightbox.min.css" rel="stylesheet">
    <!-- Video Lightbox -->
    <link href="css/modal-video.min.css" rel="stylesheet">
    <!-- Main Stylesheet -->
    <link href="css/style.css" rel="stylesheet">
    <link href="/css/floatingSupport.css" rel="stylesheet">


</head>
<body>
@component('components.nav')
@endcomponent
<div class="container">
    <div id="register-container" class="row mt-3 mb-5">
        <div class="col-12 col-lg-6 col-md-12 col-sm-12 col-xs-12">
            <div class="col-12 d-flex align-items-center h-75 justify-content-center">
                <img src="/images/logo.png" alt="" style="height: 248px;">
            </div>
            <h3 class="col-12  h-25 marketPlaceTitle text-center">Get what you <span class="colored">actually</span> want from packs</h3>
        </div>
        <div class="col-12 col-lg-6 h-100 col-md-12  col-sm-12 col-xs-12 login-form">
            <div class=" h-100">
                <div class="card text-center h-75">
                    <div class="mt-3">
                        <h2>Register</h2>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <!-- First name -->
                            <div class="form-group">
                                <label hidden for="firstname"
                                       class="col-md-4 col-form-label text-md-right">First Name</label>

                                <div class="transparent-input">
                                    <input id="firstname" placeholder="First Name" type="text"
                                           class="form-control input-lg input-container"
                                           name="firstname" value="{{ old('firstname') }}" required autofocus>

                                    @if ($errors->has('firstname'))
                                        <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <!-- Last name -->
                            <div class="form-group">
                                <label hidden for="lastname"
                                       class="col-md-4 col-form-label text-md-right">Last Name</label>

                                <div class="transparent-input">
                                    <input id="lastname" placeholder="Last Name" type="text"
                                           class="form-control input-lg input-container"
                                           name="lastname" value="{{ old('lastname') }}" required autofocus>
                                    @if ($errors->has('lastname'))
                                        <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label hidden for="username"
                                       class="col-md-4 col-form-label text-md-right">Enter your username</label>

                                <div class="transparent-input">
                                    <input id="username" placeholder="Username" type="text"
                                           class="form-control input-lg input-container"
                                           name="username" value="{{ old('username') }}" required>

                                    @if ($errors->has('username'))
                                        <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <!-- Email Address -->
                            <div class="form-group">
                                <label hidden for="email"
                                       class="col-md-4 col-form-label text-md-right">E-Mail
                                    Address</label>

                                <div class="transparent-input">
                                    <input id="email" placeholder="Email Address" type="email"
                                           class="form-control input-lg input-container"
                                           name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label hidden for="password"
                                       class="col-md-4 col-form-label text-md-right">Password</label>

                                <div class="transparent-input">
                                    <input id="password" placeholder="Password" type="password"
                                           class="form-control input-lg input-container"
                                           name="password" required>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label hidden for="password-confirm"
                                       class="col-md-4 col-form-label text-md-right">Confirm Password</label>

                                <div class="transparent-input">
                                    <input id="password-confirm" placeholder="Password Confirm" type="password"
                                           class="form-control input-lg input-container"
                                           name="password_confirmation" required>

                                </div>
                            </div>

                            <div class="form-group">
                                <label hidden for="phone"
                                       class="col-md-4 col-form-label text-md-right">Phone Number</label>

                                <div class="transparent-input">
                                    <input id="phone" type="text"
                                           class="form-control input-lg input-container input-medium bfh-phone"
                                           name="phone" data-country="US" placeholder="Phone Number" required>
                                    @if ($errors->has('phone'))
                                        <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-12">
                                    <div class="form-check">
                                        <p style="margin-bottom: 10px">Preferred Console</p>
                                        <label style="width: 150px; padding: 0" class="radio-inline" for="ps4">
                                            <input id="ps4" style="width: 15px !important; height: 15px; margin-right: 10px" type="radio" name="console" value="ps4" checked>PS4
                                        </label>
                                        <label style="width: 150px; padding: 0;" class="radio-inline" for="xbox">
                                            <input id="ps4" style="width: 15px !important; height: 15px; margin-right: 10px" type="radio" name="console" value="xbox">Xbox One
                                        </label>
                                    </div>
                                </div>
                                @if ($errors->has('console'))
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $errors->first('console') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label hidden for="hut_team"
                                       class="col-md-4 col-form-label text-md-right">Enter your HUT team name</label>

                                <div class="transparent-input">
                                    <input id="hut_team" placeholder="HUT Team Name" type="text"
                                           class="form-control input-lg input-container"
                                           name="hut_team" value="{{ old('hut_team') }}" required>

                                    @if ($errors->has('hut_team'))
                                        <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $errors->first('hut_team') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>



                            <div class="form-group row">
                                <div class="col-12">
                                    <div class="form-check">
                                        <input style="margin-bottom: 4px !important;" class="form-check-input" type="checkbox" name="agree"
                                               id="agree">

                                        <label class="form-check-label" for="agree">
                                            I agree to the <a style="color: orange" href="{{route('terms')}}">terms of service</a>
                                        </label>
                                    </div>
                                    @if ($errors->has('agree'))
                                        <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $errors->first('agree') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-12">
                                    <button type="submit" class="button-submit btn-primary">
                                        Register
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@component('components.support')
@endcomponent

<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/blazy.min.js"></script>
<script src="js/isotope.pkgd.min.js"></script>
<script src="js/lightbox.min.js"></script>
<script src="js/jquery-modal-video.min.js"></script>
<script src="js/validator.min.js"></script>
<script src="js/strider.js"></script>
<script src="/js/floatingSupport.js"></script>
<script src="/js/register.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-formhelpers/2.3.0/js/bootstrap-formhelpers.min.js"></script>
</body>
</html>



