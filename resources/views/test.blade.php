<html>
<head lang="en">
    <meta charset="UTF-8">

</head>
<body>
<?php
use Xsolla\SDK\API\XsollaClient;
use Xsolla\SDK\API\PaymentUI\TokenRequest;

$tokenRequest = new TokenRequest((int)env('PROJECT_ID'), '1');
$tokenRequest
    ->setSandboxMode(true);

$xsollaClient = XsollaClient::factory(array(
    'merchant_id' => (int)env('MERCHANT_ID'),
    'api_key' => env('API_KEY')
));
$response = $xsollaClient->ListPaymentAccounts(array(
    'project_id' =>(int)env('PROJECT_ID'),
    'user_id' => '1'
));
$token = $xsollaClient->createPaymentUITokenFromRequest($tokenRequest);
echo $token;
\Xsolla\SDK\API\PaymentUI\PaymentUIScriptRenderer::send($token, $isSandbox = true)
?>

<button data-xpaystation-widget-open>Buy Credits</button>
<?php \Xsolla\SDK\API\PaymentUI\PaymentUIScriptRenderer::send($token, $isSandbox = true); ?>

</body>
</html>
