<html xmlns="http://www.w3.org/1999/html">
<head>
    <title>HUTWINNER</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <!-- OG meta tags that improve the look of your post on social media -->
    <meta property="og:site_name" content=""/><!--website name-->
    <meta property="og:site" content=""/> <!--website link-->
    <meta property="og:title" content=""/> <!--Post title-->
    <meta property="og:description" content=""/> <!--Post description-->
    <meta property="og:image" content=""/><!-- Image link (jpg only)-->
    <meta property="og:url" content=""/> <!--where do you want your post to link to-->
    <meta property="og:type" content="article"/>
    <link rel="apple-touch-icon" sizes="57x57" href="images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">


    <!-- STYLES -->

    <link rel="stylesheet" href="{{ asset('css/packDisplay.css') }}">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/nav.css')}}">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="/css/floatingSupport.css" rel="stylesheet">
    <script src="https://cdn.rawgit.com/webcomponents/webcomponentsjs/v0.7.24/webcomponents-lite.js"></script>
    <style>
        @import url(https://fonts.googleapis.com/css?family=Roboto:400,500,700);
        @import url(https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic);

        .navbar, #footer {
            background: #20202A !important;
        }

        .buttonValue {
            background: #F6EA9C !important;
            color: #000 !important;
        }

        @media (min-width: 1200px) {
            .container {
                max-width: 90%;
            }
        }

        input, select {
            background: rgba(0, 0, 0, 0.5) !important;
            color: white !important;
            border: 1px solid #F6EA9C !important;
        }

        #formContainer {
            background: #30303F !important;
            border-radius: 15px 15px 15px 15px;
            -webkit-box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.3);
            box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.3);
            margin-bottom: 8%;
        }

        .main-container {
            background: #363647 !important;
            border-radius: 15px 15px 15px 15px;
            -webkit-box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.3);
            box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.3);
            margin-bottom: 120px !important;
        }

        @media (min-width: 1200px) {
            .container {
                max-width: 85%;
            }
        }

        .columnValue {
            min-height: 342px;
            min-width: 242px;
            padding: unset;
        }

        form{
            margin: 0;
        }

    </style>
</head>
<body style="position:relative; background: #202033 !important;">
<header id="main-header">
    @component('components.nav',['header'=> 'Profile', 'coloredHeader' => ''])
    @endcomponent
</header>
<div id="container">
    <div class="main-container container pt-5 mt-3">
        <div class="row">
            <div class="col-12">
                <h2 class="marketPlaceTitle headerMarketplace animation-element slide-down text-center"><span
                            class="colored">General Account Settings</span></h2>
            </div>
        </div>
        @if(Session::has('success'))
            <div id="success" class="alert alert-success">
                {!!  Session::get('success') !!}
            </div>
        @endif
        @if($errors->any())
            <div id="failed" class="alert alert-danger">
                <p>{{ $errors->first() }}</p>
            </div>
        @endif
        <div class="row">
            <div class="container">
                <div class="row justify-content-center">
                    <form class="p-4  w-75 " id="formContainer">
                        <div class="form-group">
                            @if(Auth::user()->status_id == Config::get('statuses.user.unverify'))
                                <h3><span class="colored">Status: </span>Unverified</h3>
                            @else
                                <h3><span class="colored">Status: </span>Verified</h3>
                            @endif
                        </div>
                        <div class="form-group">
                            <h3><span class="colored">Email: </span>{{Auth::user()->email}}</h3>
                        </div>
                        <div class="form-group">
                            @if(Auth::user()->status_id != Config::get('statuses.user.unverify'))
                                <h3><span class="colored">Daily Deposit: </span>Unlimited</h3>
                            @else
                                <h3><span class="colored">Daily Deposit:</span> 50$ USD <br> <a style="color: white"
                                          class="btn btn-link forgetPassword" href="{{route('remove.deposit')}}">Remove Daily Deposit Limit</a></h3>
                            @endif

                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1" class="colored">First name</label>
                            <input type="text" class="form-control" id="exampleFormControlInput1"
                                   placeholder="First Name"
                                   value="{{Auth::user()->firstname}}">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1" class="colored">Last name</label>
                            <input type="text" class="form-control" id="exampleFormControlInput1"
                                   placeholder="First Name"
                                   value="{{Auth::user()->lastname}}">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1" class="colored">Phone Number</label>
                            <input type="text" class="form-control bfh-phone" id="exampleFormControlInput1"
                                   data-country="US" placeholder="Phone Number"
                                   value="{{Auth::user()->phone}}">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1" class="colored">Preferred Console</label>
                            <select class="form-control" id="exampleFormControlSelect1">
                                <option value="ps4">PS4</option>
                                <option value="xbox">XBOX</option>
                            </select>
                        </div>
                        <button type="button" class="btn buttonValue" data-toggle="modal" data-target="#confirmation"
                                id="validation">Save Changes
                        </button>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="confirmation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Confirm Changes</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <form action="{{ route('user.update')}}" id="containerValues" method="post">
                            @csrf
                            <input type="submit" class="btn btn-primary" value="Confirm">
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    @component('components.support')
    @endcomponent
    @component('components.footer')
    @endcomponent
</div>
<script src="/js/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>
<script src="/js/profile.js"></script>
<script src="/js/floatingSupport.js"></script>
<script>
    window.setTimeout(function() {
        $("#success").fadeTo(1000, 0).slideUp(1000, function(){
            $(this).remove();
        });
    }, 5000);
    window.setTimeout(function() {
        $("#failed").fadeTo(1000, 0).slideUp(1000, function(){
            $(this).remove();
        });
    }, 5000);
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-formhelpers/2.3.0/js/bootstrap-formhelpers.min.js"></script>
</body>
</html>
