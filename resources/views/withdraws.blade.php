<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>HUTWINNER</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <!-- OG meta tags that improve the look of your post on social media -->
    <meta property="og:site_name" content=""/><!--website name-->
    <meta property="og:site" content=""/> <!--website link-->
    <meta property="og:title" content=""/> <!--Post title-->
    <meta property="og:description" content=""/> <!--Post description-->
    <meta property="og:image" content=""/><!-- Image link (jpg only)-->
    <meta property="og:url" content=""/> <!--where do you want your post to link to-->
    <meta property="og:type" content="article"/>
    <link rel="apple-touch-icon" sizes="57x57" href="images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">


    <!-- STYLES -->

    <link rel="stylesheet" href="{{ asset('css/packDisplay.css') }}">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/nav.css')}}">

    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <script src="https://cdn.rawgit.com/webcomponents/webcomponentsjs/v0.7.24/webcomponents-lite.js"></script>
    <style>
        @import url(https://fonts.googleapis.com/css?family=Roboto:400,500,700);
        @import url(https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic);

        .navbar, #footer {
            background: #20202A !important;
        }

        .buttonValue {
            background: #F6EA9C !important;
            color: #000 !important;
        }



        .table td {
            border-top: 1px solid #F6EA9C !important;;
        }

        .table thead th {
            border-bottom: unset !important;
        }

        @media (min-width: 1200px) {
            .container {
                max-width: 90%;
            }
        }

        input, select {
            background: rgba(0, 0, 0, 0.5) !important;
            color: white !important;
            border: 1px solid #F6EA9C !important;
        }

        .tableContainer {
            background: #30303F !important;
            border-radius: 15px 15px 15px 15px;
            -webkit-box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.3);
            box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.3);
            margin-bottom: 8%;
            overflow-y: scroll;
        }

        .table th {
            border-top: unset !important;
        }

        .main-container {
            background: #363647 !important;
            border-radius: 15px 15px 15px 15px;
            -webkit-box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.3);
            box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.3);
            margin-bottom: 120px !important;
        }

        @media (min-width: 1200px) {
            .container {
                max-width: 85%;
            }
        }

        .columnValue {
            min-height: 342px;
            min-width: 242px;
            padding: unset;
        }


    </style>
    <link href="/css/floatingSupport.css" rel="stylesheet">
</head>
<body style="position:relative; background: #202033 !important;">
<header id="main-header">
    @component('components.nav',['header'=> 'Profile', 'coloredHeader' => ''])
    @endcomponent
</header>
<div id="container">
<div class="main-container container pt-5 mt-3">
    <div class="row">
        <div class="col-12">
            <h2 class="marketPlaceTitle headerMarketplace animation-element slide-down text-center"><span
                        class="colored">Withdraw Request</span></h2>
        </div>
    </div>
    <div class="row">
        <div class="container">
            <div class="row justify-content-center">
                @if( $withdraws->isEmpty())
                    <h3>
                        You have yet to withdraw any cards.
                        Go to the <a href="{{ route('user.withdraw') }}" style="color: #b48445;">Withdraw page</a> and
                        make a request.
                    </h3>
                @else
                    <table class="table tableContainer h-50" style="">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Status</th>
                            <th scope="col">Number of Cards</th>
                            <th scope="col">Time</th>
                            <th scope="col">Cards Request</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($withdraws as $withdraw)
                            <tr>
                                <td>
                                    {{ $withdraw->id }}
                                </td>
                                <td>
                                    <?php
                                    $val = $withdraw->status->first()->type;
                                    ?>
                                    @if($val =='Pending')
                                        <span class="colored">{{ $val }}</span>
                                    @elseif($val == 'Accepted')
                                        <span class="text-success">{{ $val }}</span>
                                    @else
                                        <span class="text-danger">{{ $val }}</span>
                                    @endif
                                </td>
                                <td>
                                    {{ count($withdraw->cards()->get()) }}
                                </td>
                                <td>
                                    {{ $withdraw->created_at }}
                                </td>
                                <td>

                                    @foreach($withdraw->cards()->get() as $card)
                                        <ul>
                                            <li>
                                                Player: {{$card->player_name}}
                                            </li>
                                            <li>
                                                Overall: {{$card->overall}}
                                            </li>
                                            <li>
                                                Type: {{ $card->cardType()->first()->type }}
                                            </li>
                                            <li>
                                                Synergy: {{ $card->synergy }}
                                            </li>
                                        </ul>
                                    @endforeach
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
</div>
    @component('components.support')
    @endcomponent
@component('components.footer')
@endcomponent
</div>
<script src="/js/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>
<script src="/js/floatingSupport.js"></script>
</body>
</html>
