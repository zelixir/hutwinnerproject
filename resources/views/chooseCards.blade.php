<html xmlns="http://www.w3.org/1999/html">
<head>

    <!-- META TAGS -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>HUTWINNER</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <!-- OG meta tags that improve the look of your post on social media -->
    <meta property="og:site_name" content=""/><!--website name-->
    <meta property="og:site" content=""/> <!--website link-->
    <meta property="og:title" content=""/> <!--Post title-->
    <meta property="og:description" content=""/> <!--Post description-->
    <meta property="og:image" content=""/><!-- Image link (jpg only)-->
    <meta property="og:url" content=""/> <!--where do you want your post to link to-->
    <meta property="og:type" content="article"/>
    <link rel="apple-touch-icon" sizes="57x57" href="images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/MarketPlace.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/chosen-cards.css') }}">
    <link rel="stylesheet" href="{{ asset('css/2019_HUT_Style.css') }}">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="/css/floatingSupport.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/nav.css')}}">

    <script src="https://cdn.rawgit.com/webcomponents/webcomponentsjs/v0.7.24/webcomponents-lite.js"></script>
    <style>
        .navbar, #footer {
            background: #20202A !important;
        }

        body {
            height: 100%;
        }

        @media (min-width: 1200px) {
            .container {
                max-width: 90%;
            }
        }

        .mainContainer {
            background: #363647 !important;
            border-radius: 15px 15px 15px 15px;
        }

        input[type='submit'] {
            border: none !important;
        }

        .columnValue {
            min-height: 342px;
            min-width: 242px;
            padding: unset;
        }
        img{
            width: 100%;
            height: 100%;
        }

    </style>

</head>
<body style="position: relative;  background: #202033 !important;">
<input id="packSize" type="hidden" value="{{$packSize}}">
<header id="main-header">
    @component('components.nav', ['header'=> 'Custom ', 'coloredHeader' => 'Pack'])
    @endcomponent
</header>
<div id="container">
    <div class="fixed" style="margin-top: 10px">
        <div id="myChosenSidenav" class="ChosenSidenav">
            <a style="margin-top: 100px" href="javascript:void(0)" class="closeChosenbtn"
               onclick="closeChosenNav()">&times;</a>
            <div class="container mt-5">
                <div class="row pl-5 pr-5" id="chosen-cards">

                </div>

            </div>
        </div>
    </div>
    <div class="container mainContainer mt-5" style="position: relative; padding-bottom: 110px;    margin-bottom: 110px;">
        <h1 class="animation-element slide-down marketPlaceTitle headerMarketplace mt-5"><span class="colored">SELECT YOUR CARDS</span>
        </h1>
        <p style="font-size: 1.5em; text-align:center; margin-bottom: 3%">Note that you have to select exactly {{$packSize}} cards</p>
        @if(Session::has('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        @if($errors->any())
            <div class="alert alert-danger">
                <p>{{ $errors->first() }}</p>
            </div>
        @endif
        <div class="row d-flex justify-content-center">
            <div class="col-6">
                <button class="btn btn-light float-right" onclick="openChosenNav()"
                        style="width: 60%; border-radius: 30px">Chosen Cards
                </button>
            </div>
            <div class="col-6">
                <form id="submitForm" action="{{ route('pack.probability', $packSize)}}" method="post">
                    @csrf
                    <input type="submit" class="btn btn-success" id="nextBtn" value="Next"
                           style="width: 60%;border-radius: 30px" disabled>
                </form>
            </div>
        </div>
        <div class="row d-flex justify-content-md-center">
            <input id="search-player" type="text" placeholder="Search Player...">
        </div>
        <div class="container mt-5" id="search-container">

        </div>
    </div>

    @component('components.support')
    @endcomponent

    @component('components.footer')
    @endcomponent
</div>
<script src="../../js/app.js"></script>

<script src="/js/chosenCards.js"></script>

<!-- Footer End -->
<script src="/js/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="/js/marketplace.js"></script>
<script src="/js/floatingSupport.js"></script>
<script src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>
<script>
    function openChosenNav() {
        document.getElementById("myChosenSidenav").style.width = "100%";
    }

    function closeChosenNav() {
        document.getElementById("myChosenSidenav").style.width = "0";
    }
</script>
</body>
</html>
