<nav class="floating-container">
    <div class="support-container container" style="overflow-y: auto;">
        <form class="contact-form row" action="{{ route('contact.message') }}" method="post" data-toggle="validator" style="display: none">
            <h2 class="text-center" style="width: 100%; text-align: center !important; margin-top: 10px">Submit A Ticket</h2>
            <div class="form-group" style="width: 100%; text-align: center">
                <label class="contact-label" for="name">Full Name</label>
                <input id="contact-name" class="contact-input col-10 mx-auto" type="text" name="name" required data-error="Name cannot be empty">
                <div class="help-block with-errors"></div>
            </div>

            <div class="form-group" style="width: 100%; text-align: center">
                <label class="contact-label" for="email">Email</label>
                <input id="contact-email" class="contact-input col-10 mx-auto" type="email" name="email" required data-error="Email cannot be empty">
                <div class="help-block with-errors"></div>
            </div>

            <div class="form-group" style="width: 100%; text-align: center">
                <label class="contact-label" for="message">Message</label>
                <textarea id="contact-message" name="message" rows="4" cols="50" class="col-10 mx-auto" required data-error="Message cannot be empty"></textarea>
                <div class="help-block with-errors"></div>
            </div>
            <button  id="submit-message" type="submit" class="col-10 mx-auto btn-success mb-3">Send Message</button>
            <!-- Success Message -->
            <div id="response" class="text-center" style="color: white">
            </div>
        </form>
    </div>
    <a class="buttons" tooltip="Support" href=""></a>
</nav>