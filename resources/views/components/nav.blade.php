<nav class="navbar navbar-expand-xl navbar-dark">
    <a class="navbar-brand" href="{{route('welcome')}}">
        <h2 style="margin-bottom:unset !important;">HUT<span class="colored">WINNER</span></h2>
        <p class="text-right" style="font-size: 0.5375rem !important; padding:  unset">BETA</p>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03"
            aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse pr-5" id="navbarColor03">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item dropdown">
                <a id="navbarDropdown1" class="nav-link dropdown-toggle" href="#" role="button"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    Marketplace
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('packs.marketplace')}}">
                       Preset Packs
                    </a>

                    <a class="dropdown-item" href="{{ route('packs.custom.marketplace')}}">
                        Custom Packs
                    </a>
                </div>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{ route('pack')}}">Create your own pack</a>
            </li>
            <li class="nav-item">
                @if(Auth::check())
                    <a class="nav-link" href="{{ route('user.packs') }}">My Packs <span class="badge badge-dark">{{\Illuminate\Support\Facades\Auth::user()->unopenedPacks_number()}}</span></a>
                @else
                    <a class="nav-link" href="{{ route('user.packs') }}">My Packs</a>
                @endif
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('user.withdraw')}}">Withdrawals</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('payment')}}">Get Points</a>
            </li>
        </ul>
        @auth
            <ul class="navbar-nav">
                <li class="nav-item">
                    <p class="nav-link"><span id="points">{{Auth::user()->coins}}</span> Points</p>
                </li>
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        My Account
                    </a>

                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('home')}}">
                            My Cards
                        </a>

                        <a class="dropdown-item" href='{{ route('profile')}}'>
                            Settings
                        </a>
                        <a class="dropdown-item" href='{{ route('user.transactions')}}'>
                            My Transactions
                        </a>
                        <a class="dropdown-item" href='{{ route('user.withdraws')}}'>
                           My Withdraws
                        </a>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            </ul>
        @else
            <ul class="navbar-nav my-2 my-lg-0">
                <li class="nav-item active">
                    <a class="nav-link" href="{{route('login')}}">Login <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('register')}}">Register</a>
                </li>
            </ul>
        @endauth
    </div>
</nav>