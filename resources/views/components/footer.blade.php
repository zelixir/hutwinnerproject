<footer id="footer"/>
        <div class="container">
            <div class="row">
                <div class="col-md-4 text-left">
                    <p id="copyright">2019 HUTWinner. All rights<span id="year"> </span></p><!-- Copyright Text -->
                </div>
                <div class="col-md-4 text-center">
                    <ul class="social-links"> <!-- Social Media Icons -->
                        <li>
                            <a href="{{ route('about')}}">About</a>
                        </li>
                        <li>
                            <a href="{{ route('terms')}}">Terms & Conditions</a>
                        </li>
                        <li>
                            <a  href="{{ route('privacy')}}">Privacy</a>
                        </li>
                        <li>
                            <a href="{{ route('faq')}}">FAQ</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <p id="copyright" class="text-right">Powered By <a href="http://www.zelixir.net/">Zelixir</a></p><!-- Copyright Text -->
                </div>
            </div>
        </div><!-- Container End -->
</footer><!-- Footer End -->