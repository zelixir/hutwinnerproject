<html xmlns="http://www.w3.org/1999/html">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>HUTWINNER</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <!-- OG meta tags that improve the look of your post on social media -->
    <meta property="og:site_name" content=""/><!--website name-->
    <meta property="og:site" content=""/> <!--website link-->
    <meta property="og:title" content=""/> <!--Post title-->
    <meta property="og:description" content=""/> <!--Post description-->
    <meta property="og:image" content=""/><!-- Image link (jpg only)-->
    <meta property="og:url" content=""/> <!--where do you want your post to link to-->
    <meta property="og:type" content="article"/>
    <link rel="apple-touch-icon" sizes="57x57" href="images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/MarketPlace.css') }}">
    <link rel="stylesheet" href="{{asset('css/2019_HUT_Style.css')}}">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/nav.css')}}">
    <link href="/css/floatingSupport.css" rel="stylesheet">
    <script src="https://cdn.rawgit.com/webcomponents/webcomponentsjs/v0.7.24/webcomponents-lite.js"></script>
    <style>
        .btn-light {
            background-color: #F6EA9C;
            color: #000;
            font-style: italic;
            border: unset;
        }

        h1 {
            font-family: unset;
        }

        a:visited {
            color: #F6EA9C;
        }

        .element-Container {
            background: #30303F !important;
            border-bottom-left-radius: unset !important;
            border-bottom-right-radius: unset !important;
            border-top-left-radius: 20px;
            border-top-right-radius: 20px;
        }

        /*ml-5 columnValue*/
        .element-ContainerSecond {
            border-top: 2px solid #F6EA9C;
            padding-left: 5%;
            background: #30303F !important;
            border-top-left-radius: unset !important;
            border-top-right-radius: unset !important;
            border-bottom-left-radius: 20px;
            border-bottom-right-radius: 20px;
        }
        .col-12 > .descriptionText {
            font-size: 1.5rem;
            margin-top: 5%;
            text-align: center;
        }

        @media (min-width: 1200px) {
            .container {
                max-width: 95%;
            }
        }

        body .containerResized {
            background: #222 !important;
        }

        .navbar {
            background: #20202A !important;
        }

        .main-container {
            background: #363647 !important;
            border-radius: 15px 15px 15px 15px;
            -webkit-box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.3);
            box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.3);
            margin-bottom: 120px !important;
        }

        .card-img-top {
            background: url(/img/hutbd-pack-icons/x7.jpg.pagespeed.ic.8-NtslfRji.jpg);
            background-repeat: no-repeat;
            -webkit-background-size: cover;
            background-size: cover;
            height: 300px;
            width: 300px;
            display: inline-block;
        }

        .columnValue {
            min-height: 342px;
            min-width: 242px;
            padding: unset;
        }
        .panel-heading{
            height: 15%;
        }

        .panel-default {
            margin-bottom: unset;
        }

        .panel-group {
            -webkit-border-radius: 10px;
            -moz-border-radius: 10px;
            border-radius: 10px;
            background: #30303F !important;
        }

        h2 {
            margin-bottom: unset;
            cursor: pointer;
        }

        .panel-body {
            background: #444452 !important;
        }
        /*END Pagination CSS*/

    </style>
</head>
<body style="position: relative; background: #202033 !important;">
<header id="main-header">
    @component('components.nav',['header'=> 'Market ', 'coloredHeader' => 'Place'])
    @endcomponent
</header>
<div id="container">
<div class="main-container container pt-5 mt-3">
    <div class="row tiny-margin">
        <div class="col-12">
            <h1 class="marketPlaceTitle headerMarketplace animation-element slide-down"><span
                        class="colored">Frequently Asked Question (FAQ)</span></h1>
        </div>
        <div class="col-12 pb-5">
            <div class="panel-group" id="faqAccordion">
                <div class="panel panel-default ">
                    <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse"
                         data-parent="#faqAccordion" data-target="#question0">
                        <h2 class="panel-title pt-5 text-center">
                           <span class="colored"> How long does it take for me to receive my cards?
                            </span>
                        </h2>

                    </div>
                    <div id="question0" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body p-5">
                            <h5><span class="label label-primary">Answer</span></h5>

                            <p>Usually, cards should take no more than 12-24 hours in order to be received. However,
                                depending on a card’s rarity, some might take up to three (3) business days. If you
                                still haven’t received your card and it has been more than three business days, contact
                                us immediately by submitting a ticket, and we’ll get back to you as soon as
                                possible.</p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default ">
                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse"
                         data-parent="#faqAccordion" data-target="#question1">
                        <h2 class="panel-title pt-5 text-center">
                           <span class="colored">  I received a card but it’s not the card that I withdrew?
                           </span>
                        </h2>

                    </div>
                    <div id="question1" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body p-5">
                            <h5><span class="label label-primary">Answer</span></h5>
                            <p>If we are unable to locate the card that you attempted to withdraw within 1-2 business
                                days, we will send you another card which is equal to or up to 20% greater in value.</p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default ">
                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse"
                         data-parent="#faqAccordion" data-target="#question2">
                        <h2 class="panel-title pt-5 text-center">
                           <span class="colored">  Help! Where are all the payment options that you’ve advertised?
                           </span>

                        </h2>

                    </div>
                    <div id="question2" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body p-5">
                            <h5><span class="label label-primary">Answer</span></h5>
                            <p>Upon checkout, you will be prompted to select either PayPal or XSolla. PayPal is the
                                preferred payment method, however XSolla has more than 600 different payment options;
                                use the search bar upon XSolla checkout in order to view them all!
                            </p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default ">
                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse"
                         data-parent="#faqAccordion" data-target="#question3">
                        <h2 class="panel-title pt-5 text-center">
                        <span class="colored">   Can I sell my coins to you guys?
                        </span>
                        </h2>

                    </div>
                    <div id="question3" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body p-5">
                            <h5><span class="label label-primary">Answer</span></h5>
                            <p>Definitely! Please contact us via support ticket or e-mail in order to discuss our rates.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse"
                         data-parent="#faqAccordion" data-target="#question4">
                        <h2 class="panel-title pt-5 text-center">
                          <span class="colored">   A card that I want isn’t in the database. Why isn’t it there?
                          </span>
                        </h2>

                    </div>
                    <div id="question4" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body p-5">
                            <h5><span class="label label-primary">Answer</span></h5>
                            <p>When a new card is released, it may take up to 1-2 business days for it to be added into
                                our system. If a card that you desire is still not in the database after this timeframe,
                                please open a support ticket or e-mail us!
                            </p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default ">
                    <div class="panel-heading accordion-toggle collapsed question-toggle" data-toggle="collapse"
                         data-parent="#faqAccordion" data-target="#question5">
                        <h2 class="panel-title pt-5 text-center">
                           <span class="colored">  I found an exploit on the site, who do I report it to?
                           </span>
                        </h2>

                    </div>
                    <div id="question5" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body p-5">
                            <h5><span class="label label-primary">Answer</span></h5>
                            <p>
                                If you find an exploit or a bug on the website, please open a support ticket or contact
                                an Administrator immediately. Depending on the severity of the exploit, you may be
                                compensated.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@component('components.support')
@endcomponent
@component('components.footer')
@endcomponent
<!-- Footer End -->
</div>
<script src="../../js/app.js"></script>
<script src="/js/jquery-3.2.1.min.js"></script>
<script src="/js/sellPopup.js"></script>
<script src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>
<script src="/js/search.js"></script>
<script src="/js/floatingSupport.js"></script>
</body>
</html>