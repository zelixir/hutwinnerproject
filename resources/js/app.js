
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

require('./components/EvolutionCard');
require('./components/XmasCard');
require('./components/ThanksgivingCard');
require('./components/NormalCard');
require('./components/PrimeTimeCard');
require('./components/TOTWSCard');
require('./components/TOTWGCard');
require('./components/AlumniCard');
require('./components/RareGoldCard');
require('./components/CompetitiveSeasonsCard');
require('./components/GatoradeCard');
require('./components/MilestoneCard');
require('./components/HutChampionsCard');
require('./components/GSCSICard');
require('./components/GSCGECard');
require('./components/GSCFICard');
require('./components/GSCSECard');
require('./components/HalloweenCard');
require('./components/LegendCard');
require('./components/MSPCard');
require('./components/DiamondCard');
require('./components/CardsContainer');
require('./components/ExampleComponent');
require('./components/BronzeCard');