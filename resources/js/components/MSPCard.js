import React, {Component} from 'react';
import ReactiveElements from 'reactive-elements';
import axios from "axios";

export default class MSPCard extends Component{

    render(){
        let styling = '';
        if(this.props.applyborder)
            styling = this.props.applyborder;
        return(
            <div className={`cardContainer ${styling}`}>
                <img src={this.props.img}/>
            </div>
        )
    }
}

document.registerReact('msp-card', MSPCard);