import React, {Component} from 'react';
import ReactiveElements from 'reactive-elements';
import axios from "axios";

export default class CompetitiveSeasonsCard extends Component{

    /*constructor(props){
        super(props);
        this.state = {
            info: []
        }
    }

    componentDidMount(){
        axios({
            method: 'post',
            url: '/cardinfo',
            data: {'id' : this.props.id}
        }).then(res => {
            this.setState({info: res.data})
        })
    }*/

    render(){
        return(
            <div className="cardContainer" style={{background: '#c5e321'}}>
                <div className="containerTop">
                    <div className="competitiveseasons_cardType cardType">

                    </div>
                    <div className="logoBackground competitiveseasons_LogoBackground"
                         style={{backgroundImage: 'url(/img/cards/19/backdrop/nhl/lak.png)'}}>

                    </div>
                    <div style={{backgroundImage: 'url(/img/headshots/active/14569.png)'}}
                         className="playerFaceNormalCard">

                    </div>
                    <div className="competitiveseasons_tag overall">
                        <div className="overallContainer">
                            <div className="type competitiveseasons_type">
                                OVR
                            </div>
                            <div className="overallText competitiveseasons_overallText">
                                93
                            </div>

                        </div>
                    </div>
                </div>
                <div className="playerInfoContainer">
                    <div className="playerPosition">

                        <div className="playerTextContainer competitiveseasons_playerTextContainer">
                            <strong>
                                RD - TWD
                            </strong>
                        </div>
                    </div>
                    <div className="playerName" style={{lineHeight: 1}}>
                        <div className="playerNameMainContainer competitiveseasons_playerNameMainContainer">
                            <div className="competitiveseasons_firstName">
                                ROB
                            </div>
                            <div className="competitiveseasons_lastName">
                                BLAKE
                            </div>
                        </div>

                    </div>
                    <div className="teamNameContainer">
                        <div className="primeTimeTeamNameContainer competitiveseasons_primeTimeTeamNameContainer">
                            KINGS
                        </div>
                    </div>
                    <div className="bottomContainer">
                        <div className="logoPosition">
                            <div className="logoPosition competitiveseasons_logoPosition"
                                 style={{backgroundImage: 'url(/img/logos/nhl/lak.png)'}}>
                            </div>
                        </div>
                        <div className="statsContainer">
                            <div className="competitiveseasons_synergyHeader synergyHeader">
                                <div className="synergyContainerTitle">
                                    <strong>Synergy</strong>
                                </div>
                            </div>
                            <div className="statsTable">
                                <div className="singleSynergy">
                                    <div className="synergyHeaderContainer competitiveseasons_synergyHeaderContainer">
                                        <div className="synergyHeaderText competitiveseasons_synergyHeaderText">
                                            BU
                                        </div>
                                    </div>
                                    <div className="statsValues competitiveseasons_statsValues">
                                        1
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

document.registerReact('competitiveseasons-card', CompetitiveSeasonsCard);