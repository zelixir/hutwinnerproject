import React, {Component} from 'react';
import ReactiveElements from 'reactive-elements';
import axios from "axios";

export default class DiamondCard extends Component{

    constructor(props){
        super(props);
        this.state = {
            info: []
        }
    }

    componentDidMount(){
        axios({
            method: 'post',
            url: '/cardinfo',
            data: {'id' : this.props.id, 'diamond' : true}
        }).then(res => {
            this.setState({info: res.data})
        })
    }

    render(){
        let styling = '';
        if(this.props.applyborder)
            styling = this.props.applyborder;
        return(
            <div className={`cardContainer ${styling}`}>
                <img src={this.props.img}/>
            </div>
        )
    }
}

document.registerReact('diamond-card', DiamondCard);