import React, { Component } from 'react';
import axios from 'axios';
import ReactiveElements from 'reactive-elements';

export default class XmasCard extends Component {

    constructor(props){
        super(props);
        this.state = {
            synergyArray: []
        }
    }

    componentDidMount(){
        {this.setSynergyArray(this.props.synergySymbols)}
    }

    setSynergyArray(synergyString){
        let originalSynergyArray = synergyString.split(',');
        let finalSynergyArray = [];
        for(let i = 0; i < originalSynergyArray.length; i++){
            if(i%2 == 1)
                continue;
            let tempArray = [originalSynergyArray[i], originalSynergyArray[i+1]];
            finalSynergyArray.push(tempArray);
        }
        this.setState({
            synergyArray: finalSynergyArray
        });
    }

    renderSynergySymbols(){
        let numOfSynergySymbols = this.state.synergyArray.length;

        if(numOfSynergySymbols == 1){
            return (
                <div className="singleSynergy">
                    <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                        <div className="synergyHeaderText primeTimeSynergy">
                            {this.state.synergyArray[0][0]}
                        </div>
                    </div>
                    <div className="statsValues primeTimeSynergyValue">
                        {this.state.synergyArray[0][1]}
                    </div>
                </div>
            );
        }
        else if(numOfSynergySymbols == 2){
            return (
                <div>
                    <div className="singleSynergy" style={{left: '35%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[0][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[0][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '55%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[1][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[1][1]}
                        </div>
                    </div>
                </div>
            );
        }
        else if(numOfSynergySymbols == 3){
            return (
                <div>
                    <div className="singleSynergy" style={{left: '30%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[0][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[0][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '50%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[1][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[1][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '70%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[2][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[2][1]}
                        </div>
                    </div>
                </div>
            );
        }
        else if(numOfSynergySymbols == 4){
            return (
                <div>
                    <div className="singleSynergy" style={{left: '27%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[0][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[0][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '45%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[1][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[1][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '65%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[2][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[2][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '83%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[3][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[3][1]}
                        </div>
                    </div>
                </div>
            );
        }
        else if(numOfSynergySymbols == 5){
            return (
                <div>
                    <div className="singleSynergy" style={{left: '25%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[0][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[0][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '41%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[1][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[1][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '56%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[2][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[2][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '71%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[3][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[3][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '86%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[4][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[4][1]}
                        </div>
                    </div>
                </div>
            );
        }

    }

    render(){
        let styling = '';
        if(this.props.applyborder)
            styling = this.props.applyborder;
        return (
            <div className={`cardContainer ${styling}`}>
                <div className="containerTop">
                    <div className="cardType" style={{backgroundImage: this.props.topBg}}>

                    </div>
                    <div className="cardType" style={{backgroundImage: this.props.bottomBg}}>

                    </div>
                    <div className="logoBackground xmas_logoBackground"
                         style={{backgroundImage: this.props.teamBackdrop}}>

                    </div>
                    <div className="xmas_tag cardType">

                    </div>
                    <div style={{backgroundImage: this.props.headshot}}
                         className="playerFaceNormalCard xmas_headShot">

                    </div>
                    <div className="overall">
                        <div className="overallContainer xmas_overallContainer">
                            <div className="type xmas_overallType">
                                RARE
                            </div>
                            <div className="overallText xmas_overallText">
                                {this.props.overall}
                            </div>

                        </div>
                    </div>
                </div>
                <div className="playerInfoContainer">
                    <div className="playerPosition">

                        <div className="playerTextContainer">
                            <strong>
                                {this.props.position}
                            </strong>
                        </div>
                    </div>
                    <div className="playerName xmas_playerNameContainer" style={{lineHeight: 1}}>
                        <div className="playerNameMainContainer xmas_playerNameMainContainer">
                            {this.props.firstName}
                            <br/>
                            {this.props.lastName}
                        </div>

                    </div>
                    <div className="teamNameContainer xmas_teamNameContainer">
                        <div className=" teamNameNormal">
                            {this.props.teamName}
                        </div>
                        <div className="logoPosition xmas_logoPosition"
                             style={{backgroundImage: this.props.teamLogo}}>

                        </div>
                    </div>
                    <div className="bottomContainer">
                        <div className="statsContainer">
                            <div className="synergyHeader">
                                <div className="synergyContainerTitle xmas_synergyHeader">
                                    <strong>Synergy</strong>
                                </div>
                            </div>
                            <div className="statsTable">
                                {this.renderSynergySymbols()}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

document.registerReact('xmas-card', XmasCard);