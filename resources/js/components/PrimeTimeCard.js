import React, {Component} from 'react';
import ReactiveElements from 'reactive-elements';
import axios from "axios";

export default class PrimeTimeCard extends Component{

    constructor(props){
        super(props);
        this.state = {
            synergyArray: []
        }
    }

    componentDidMount(){
        {this.setSynergyArray(this.props.synergySymbols)}
    }

    setSynergyArray(synergyString){
        let originalSynergyArray = synergyString.split(',');
        let finalSynergyArray = [];
        for(let i = 0; i < originalSynergyArray.length; i++){
            if(i%2 == 1)
                continue;
            let tempArray = [originalSynergyArray[i], originalSynergyArray[i+1]];
            finalSynergyArray.push(tempArray);
        }
        this.setState({
            synergyArray: finalSynergyArray
        });
    }

    renderSynergySymbols(){
        let numOfSynergySymbols = this.state.synergyArray.length;

        if(numOfSynergySymbols == 1){
            return (
                <div className="singleSynergy">
                    <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                        <div className="synergyHeaderText primeTimeSynergy">
                            {this.state.synergyArray[0][0]}
                        </div>
                    </div>
                    <div className="statsValues primeTimeSynergyValue">
                        {this.state.synergyArray[0][1]}
                    </div>
                </div>
            );
        }
        else if(numOfSynergySymbols == 2){
            return (
                <div>
                    <div className="singleSynergy" style={{left: '35%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[0][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[0][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '55%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[1][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[1][1]}
                        </div>
                    </div>
                </div>
            );
        }
        else if(numOfSynergySymbols == 3){
            return (
                <div>
                    <div className="singleSynergy" style={{left: '30%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[0][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[0][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '50%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[1][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[1][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '70%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[2][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[2][1]}
                        </div>
                    </div>
                </div>
            );
        }
        else if(numOfSynergySymbols == 4){
            return (
                <div>
                    <div className="singleSynergy" style={{left: '27%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[0][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[0][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '45%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[1][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[1][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '65%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[2][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[2][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '83%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[3][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[3][1]}
                        </div>
                    </div>
                </div>
            );
        }
        else if(numOfSynergySymbols == 5){
            return (
                <div>
                    <div className="singleSynergy" style={{left: '25%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[0][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[0][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '41%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[1][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[1][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '56%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[2][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[2][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '71%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[3][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[3][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '86%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[4][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[4][1]}
                        </div>
                    </div>
                </div>
            );
        }

    }

    render(){
        let styling = '';
        if(this.props.applyborder)
            styling = this.props.applyborder;
        return(
            <div className={`cardContainer ${styling}`} style={{background: '#be2f37'}}>
                <div className="containerTop">
                    <div className="primeTime cardType">

                    </div>
                    <div className="logoBackground primeTimeLogoBackground"
                         style={{backgroundImage: this.props.teamBackdrop}}>

                    </div>
                    <div style={{backgroundImage: this.props.headshot}}
                         className="playerFaceNormalCard">

                    </div>
                    <div className="primeTimeTag overall">
                        <div className="overallContainer">
                            <div className="type">
                                OVR
                            </div>
                            <div className="overallText primeTimeOverall">
                                {this.props.overall}
                            </div>

                        </div>
                    </div>
                </div>
                <div className="playerInfoContainer" style={{color: '#ffffff !important'}}>
                    <div className="playerPosition">

                        <div className="playerTextContainer primeTimePosition">
                            <strong>
                                {this.props.position}
                            </strong>
                        </div>
                    </div>
                    <div className="playerName" style={{lineHeight: 1}}>
                        <div className="playerNameMainContainer">
                            <div className="primeTimeFirstName">
                                {this.props.firstName}
                            </div>
                            <div className="primeTimeLastName">
                                {this.props.lastName}
                            </div>
                        </div>

                    </div>
                    <div className="teamNameContainer">
                        <div className="primeTimeTeamNameContainer">
                            {this.props.teamName}
                        </div>
                    </div>
                    <div className="bottomContainer primeTimeBottomContainer">
                        <div className="logoPosition primeTimeLogoPosition"
                             style={{backgroundImage: this.props.teamLogo}}>

                        </div>
                        <div className="statsContainer">
                            <div className="synergyHeader primeTimeSynergyHeader">
                                <div className="synergyContainerTitle">
                                    <strong>Synergy</strong>
                                </div>
                            </div>
                            <div className="statsTable primeTimeStatsTable">
                                {this.renderSynergySymbols()}

                            </div>
                        </div>
                    </div>

                </div>
            </div>

        )
    }
}

document.registerReact('primetime-card', PrimeTimeCard);