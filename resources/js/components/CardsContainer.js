import React, {Component} from 'react';
import axios from "axios";

export default class CardsContainer extends Component{
    constructor(props){
        super(props);
        this.ids = this.props.ids.split(',');
        this.idsSelected = [];
        if(this.props.idsselected)
            this.idsSelected = this.props.idsselected.split(',');
        if(this.props.slider){
            this.state = {
                info: [],
                notFlipped: "card col-md-3 mr-5 card-container columnValue ml-5",
                flipped: "card col-md-3 mr-5 card-container columnValue valueFlip flippedCard ml-5",
                cardsFlipped: []
            }
        }
        else{
            this.state = {
                info: []
            }
        }

        if(this.props.choosepercentage){
            let foo = new Array(25);
            for(let i = 0; i < 25; i++){
                foo[i] = i+1;
            }
            this.optionsValues = foo;
        }

        this.probabilities = [];
        if(this.props.probability){
            this.probabilities = this.props.probability.split(',');
        }
    }

    componentDidMount(){
        if(this.props.managecards){
            axios({
                method: 'post',
                url: '/manycardsinfo',
                data: {'ids' : this.ids, 'manageCards' : true}
            }).then(res => {
                this.setState({info: res.data})
            })
        }
        else{
            axios({
                method: 'post',
                url: '/manycardsinfo',
                data: {'ids' : this.ids}
            }).then(res => {
                this.setState({info: res.data})
            })
        }
    }

    getCard(card_type, card_info, selected){
        let card;

        switch (card_type) {
            case 'LGD':
                if(!selected)
                    card = <legend-card {...card_info}/>;
                else
                    card = <legend-card applyborder="clicked-card" {...card_info}/>;
                break;
            case 'ALM':
                if(!selected)
                    card = <alumni-card {...card_info}/>;
                else
                    card = <alumni-card applyborder="clicked-card" {...card_info}/>;
                break;
            case 'MSP':

                if(!selected)
                    card = <msp-card {...card_info}/>;
                else
                    card = <msp-card applyborder="clicked-card" {...card_info}/>;
                break;
            case 'MS':
                if(!selected)
                    card = <milestone-card {...card_info}/>;
                else
                    card = <milestone-card applyborder="clicked-card" {...card_info}/>;
                break;
            case 'HC':
                if(!selected)
                    card = <hutchampions-card {...card_info}/>;
                else
                    card = <hutchampions-card applyborder="clicked-card" {...card_info}/>;
                break;
            case 'PT':
                if(!selected)
                    card = <primetime-card {...card_info}/>;
                else
                    card = <primetime-card applyborder="clicked-card" {...card_info}/>;
                break;
            case 'EVO':
                if(!selected)
                    card = <evolution-card {...card_info}/>;
                else
                    card = <evolution-card applyborder="clicked-card" {...card_info}/>;
                break;
            case 'TOTW':
                if(!selected)
                    card = <totwg-card {...card_info}/>;
                else
                    card = <totwg-card applyborder="clicked-card" {...card_info}/>;
                break;
            case 'XMAS':
                if(!selected)
                    card = <xmas-card {...card_info}/>;
                else
                    card = <xmas-card applyborder="clicked-card" {...card_info}/>;
                break;
            case '19':
                if(!selected)
                    card = <thanksgiving-card {...card_info}/>;
                else
                    card = <thanksgiving-card applyborder="clicked-card" {...card_info}/>;
                break;
            case 'HWN':
                if(!selected)
                    card = <halloween-card {...card_info}/>;
                else
                    card = <halloween-card applyborder="clicked-card" {...card_info}/>;
                break;
            case 'GAT':
                if(!selected)
                    card = <gatorade-card {...card_info}/>;
                else
                    card = <gatorade-card applyborder="clicked-card" {...card_info}/>;
                break;
            case 'GSCSI':
                if(!selected)
                    card = <gscsi-card {...card_info}/>;
                else
                    card = <gscsi-card applyborder="clicked-card" {...card_info}/>;
                break;
            case 'GSCGE':
                if(!selected)
                    card = <gscge-card {...card_info}/>;
                else
                    card = <gscge-card applyborder="clicked-card" {...card_info}/>;
                break;
            case 'sGold':
                if(!selected)
                    card = <raregold-card {...card_info}/>;
                else
                    card = <raregold-card applyborder="clicked-card" {...card_info}/>;
                break;
            case 'GSCFI':
                if(!selected)
                    card = <gscfi-card {...card_info}/>;
                else
                    card = <gscfi-card applyborder="clicked-card" {...card_info}/>;
                break;
            case 'DIAM':
                if(!selected)
                    card = <diamond-card {...card_info}/>;
                else
                    card = <diamond-card applyborder="clicked-card" {...card_info}/>;
                break;
            case 'GSCSE':
                if(!selected)
                    card = <gscse-card {...card_info}/>;
                else
                    card = <gscse-card applyborder="clicked-card" {...card_info}/>;
                break;
            case 'Gold':
                if(!selected)
                    card = <normal-card {...card_info}/>;
                else
                    card = <normal-card applyborder="clicked-card" {...card_info}/>;
                break;
            case 'STOTW':
                if(!selected)
                    card = <totws-card {...card_info}/>;
                else
                    card = <totws-card applyborder="clicked-card" {...card_info}/>;
                break;
            default:
                if(!selected)
                    card = <bronze-card {...card_info}/>;
                else
                    card = <bronze-card applyborder="clicked-card" {...card_info}/>;
        }

        return card;
    }

    flipCard(index, e){
        this.setState({cardsFlipped: [...this.state.cardsFlipped, index]});
    }

    renderNormal(){
        let _this = this;
        let cards;
        let percentageSelect = <span></span>;

        if(this.props.choosepercentage){
            let allOptions = this.optionsValues.map(function (value, index) {
                return (
                    <option key={index} style={{width: '50px'}} value={value}>{value}</option>
                );
            });
            percentageSelect = <div><p style={{display: 'inline',marginRight: '10px'}}>Choose percentage:</p><select name="probability" style={{width: '50px'}}>{allOptions}</select></div>;
        }

        if(!this.state.info[0]){
            return null
        }
        else{
            cards = this.state.info.map(function(response, index){
                let selected = false;
                if(_this.idsSelected.includes(response.original.playerid))
                    selected = true;
                let displayedPercentageArray;
                if(_this.props.displayedpercentage){
                    displayedPercentageArray = _this.props.displayedpercentage.split(',');
                    return(
                        <div key={index} className="mr-3 col-3 col-md-3 col-lg-3 col-sm-12 ml-5 mt-5 columnValue"
                             style={{position: 'relative'}}>
                            {_this.getCard(response.original.card_type, response.original, selected)}
                            <div style={{marginTop: '360px'}}>
                                {percentageSelect}
                            </div>
                            <div style={{marginTop: '360px', marginLeft: '60px'}}>
                                <p style={{display: 'inline'}}>Probability: </p>
                                {displayedPercentageArray[index]} %
                            </div>
                        </div>
                    );
                }
                return(
                    <div key={index} className="mr-3 col-3 col-md-3 col-lg-3 col-sm-12 ml-5 mt-5 columnValue"
                         style={{position: 'relative'}}>
                        {_this.getCard(response.original.card_type, response.original, selected)}
                        <div style={{marginTop: '360px'}}>
                            {percentageSelect}
                        </div>
                    </div>
                );
            });
            return(
                <div className="row pl-5 pr-5">
                    {cards}
                </div>
            );
        }
    }

    renderSlider(){
        let _this = this;
        let cards = this.state.info.map(function (response, index) {
            if(_this.state.cardsFlipped.includes(index)){
                return(
                    <div key={index} className={_this.state.flipped}>
                        <div className="front">
                        </div>
                        <div className="back">
                            {_this.getCard(response.original.card_type, response.original)}
                            <button className="sellNow ">Quick Sell</button>
                            <div className="sellContainer">
                                <input type="hidden" name="info[]" value={response.original.playerid}/>
                                <input type="hidden" name="info[]" value={`${response.original.first_name} ${response.original.last_name}`}/>
                                <input type="hidden" name="info[]" value={response.original.price}/>
                            </div>
                        </div>
                    </div>
                );
            }
            else{
                return(
                    <div key={index} className={_this.state.notFlipped} onClick={_this.flipCard.bind(_this, index)}>
                        <div className="front">
                        </div>
                        <div className="back">
                            {_this.getCard(response.original.card_type, response.original)}
                            <button className="sellNow ">Quick Sell</button>
                        </div>
                    </div>
                );
            }
        });

        return(
            <div>
                {cards}
            </div>
        );
    }

    renderPackDisplay(){
        let _this = this;
        let cards;

        if(!this.state.info[0]){
            return null
        }
        else{
            cards = this.state.info.map(function(response, index){
                if(_this.props.sellbtn){
                    return(
                        <div key={index}>
                            <div className="ml-5 columnValue mt-5"
                                 style={{position: 'relative'}}>
                                {_this.getCard(response.original.card_type, response.original)}
                            </div>
                            <button className="btn-success sellNow" style={{marginLeft: '30%', marginTop: '10%',
                                width: '60%', borderRadius: '20px'}}>Sell Card</button>
                            <div className="sellContainer">
                                <input type="hidden" name="info[]" value={response.original.playerid} />
                                <input type="hidden" name="info[]" value={`${response.original.first_name} ${response.original.last_name}`} />
                                <input type="hidden" name="info[]" value={response.original.price} />
                            </div>

                        </div>
                    );
                }
                else {
                    let probability;
                    if(_this.props.probability){
                        let prob = (_this.probabilities[index] * 100).toFixed(2);
                        probability = <div className="probability" style={{paddingTop: '60%', paddingLeft: '18%'}}>Probability: {prob}%</div>
                    }
                    return (
                        <div key={index} className="ml-5 columnValue mt-5"
                             style={{position: 'relative'}}>
                            {_this.getCard(response.original.card_type, response.original)}
                            {probability}
                        </div>
                    );
                }
            });
            return(
                <div className="row">
                    {cards}
                </div>
            );
        }
    }

    renderWithdraw(){
        let _this = this;
        let cards;

        if(!this.state.info[0]){
            return null
        }
        else{
            cards = this.state.info.map(function(response, index){
                let fullName = `${response.original.first_name} ${response.original.last_name}`;
                return(
                    <div key={index} className="mr-3 col-3 col-md-3 col-lg-3 col-sm-12 ml-5 mt-5 columnValue"
                         style={{position: 'relative'}}>
                        {_this.getCard(response.original.card_type, response.original)}
                        <input type="hidden" name="id" value={response.original.playerid}/>
                        <input type="hidden" name="name" value={fullName}/>
                    </div>
                );
            });
            return(
                <div className="row pl-5 pr-5">
                    {cards}
                </div>
            );
        }
    }

    renderManageCards(){
        let _this = this;
        let cards;

        if(!this.state.info[0]){
            return null;
        }
        else{
            cards = this.state.info.map(function (response, index) {
                let fullName = `${response.original.first_name} ${response.original.last_name}`;
                return(
                    <div className='col-lg-5 col-md-6 col-sm-6 col-12 col-xl-4 columnValue' key={index} style={{height: '500px', width: '350px'}}>
                        <div id={`card-${response.original.playerid}`} className="card card-height" style={{height: '450px', width: '350px'}}>
                            <ul className="nav nav-tabs" role="tablist">
                                <li className="nav-item">
                                    <a className="nav-link active custom-link" id="home-tab" data-toggle="tab"
                                       href={`#image-${response.original.playerid}`} role="tab">{fullName}</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link custom-link" id="profile-tab" data-toggle="tab"
                                       href={`#details-${response.original.playerid}`} role="tab">Details</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link custom-link" data-toggle="tab"
                                       href={`#prices-${response.original.playerid}`} role="tab"><i className="fas fa-edit"/></a>
                                </li>
                            </ul>
                            <div className="tab-content">
                                <div className="tab-pane fade show active" id={`image-${response.original.playerid}`} role="tabpanel">
                                    <div className="card-body" style={{paddingLeft: '16%'}}>
                                        {_this.getCard(response.original.card_type, response.original)}
                                    </div>
                                </div>
                                <div className="tab-pane fade" id={`details-${response.original.playerid}`} role="tabpanel">
                                    <div className="card-body">
                                        <div className="d-flex justify-content-between">
                                            <p className="card-text m-0">Card ID:</p>
                                            <p className="card-text m-0">{response.original.playerid}</p>
                                        </div>
                                        <div className="d-flex justify-content-between">
                                            <p className="card-text m-0">League:</p>
                                            <p className="card-text m-0">{response.original.league}</p>
                                        </div>
                                        <div className="d-flex justify-content-between">
                                            <p className="card-text m-0">Team:</p>
                                            <p className="card-text m-0">{`${response.original.team_name} (${response.original.team_abbr})`}</p>
                                        </div>
                                        <div className="d-flex justify-content-between">
                                            <p className="card-text m-0">Overall:</p>
                                            <p className="card-text m-0">{response.original.overall}</p>
                                        </div>
                                        <div className="d-flex justify-content-between">
                                            <p className="card-text m-0">Player:</p>
                                            <p className="card-text m-0">{fullName}</p>
                                        </div>
                                        <div className="d-flex justify-content-between">
                                            <p className="card-text m-0">Position:</p>
                                            <p className="card-text m-0">{response.original.position}</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="tab-pane fade" id={`prices-${response.original.playerid}`} role="tabpanel">
                                    <div className="card-body">
                                        <div className="input-group mb-3">
                                            <div className="input-group-prepend">
                                                <span className="input-group-text">XBOX</span>
                                            </div>
                                            <input id={`xboxVal${response.original.playerid}`} type="number" step="0.01"
                                                   className="form-control pr-0" name="xboxprice"
                                                   defaultValue={response.original.xbox_price}/>
                                        </div>
                                        <div className="input-group mb-3">
                                            <div className="input-group-prepend">
                                                <span className="input-group-text">PS4</span>
                                            </div>
                                            <input id={`ps4Val${response.original.playerid}`}s type="number" step="0.01"
                                                   className="form-control pr-0" name="ps4price"
                                                   defaultValue={response.original.ps4_price}/>
                                        </div>
                                        <button type="button" className="btn btn-secondary btn-sm price-submit"
                                                value={response.original.playerid} value={response.original.playerid}>Save changes
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                );
            });
            return(
                <div className='row flex-wrap'>
                    {cards}
                </div>
            );
        }
    }

    render(){
        if(this.props.slider){
            return this.renderSlider();
        }
        if(this.props.packdisplay){
            return this.renderPackDisplay();
        }
        if(this.props.withdraw){
            return this.renderWithdraw();
        }
        if(this.props.managecards){
            return this.renderManageCards();
        }

        return this.renderNormal();
    }
}

document.registerReact('cards-container', CardsContainer);