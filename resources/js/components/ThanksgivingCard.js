import React, {Component} from 'react';
import ReactiveElements from 'reactive-elements';
import axios from "axios";

export default class ThanksgivingCard extends Component{

    constructor(props){
        super(props);
        this.state = {
            synergyArray: []
        }
    }

    componentDidMount(){
        {this.setSynergyArray(this.props.synergySymbols)}
    }

    setSynergyArray(synergyString){
        let originalSynergyArray = synergyString.split(',');
        let finalSynergyArray = [];
        for(let i = 0; i < originalSynergyArray.length; i++){
            if(i%2 == 1)
                continue;
            let tempArray = [originalSynergyArray[i], originalSynergyArray[i+1]];
            finalSynergyArray.push(tempArray);
        }
        this.setState({
            synergyArray: finalSynergyArray
        });
    }

    renderSynergySymbols(){
        let numOfSynergySymbols = this.state.synergyArray.length;

        if(numOfSynergySymbols == 1){
            return (
                <div className="singleSynergy">
                    <div className="synergyHeaderContainer">
                        <div className="synergyHeaderText">
                            {this.state.synergyArray[0][0]}
                        </div>
                    </div>
                    <div className="statsValues">
                        {this.state.synergyArray[0][1]}
                    </div>
                </div>
            );
        }
        else if(numOfSynergySymbols == 2){
            return (
                <div>
                    <div className="singleSynergy" style={{left: '35%'}}>
                        <div className="synergyHeaderContainer">
                            <div className="synergyHeaderText">
                                {this.state.synergyArray[0][0]}
                            </div>
                        </div>
                        <div className="statsValues">
                            {this.state.synergyArray[0][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '55%'}}>
                        <div className="synergyHeaderContainer">
                            <div className="synergyHeaderText">
                                {this.state.synergyArray[1][0]}
                            </div>
                        </div>
                        <div className="statsValues">
                            {this.state.synergyArray[1][1]}
                        </div>
                    </div>
                </div>
            );
        }
        else if(numOfSynergySymbols == 3){
            return (
                <div>
                    <div className="singleSynergy" style={{left: '30%'}}>
                        <div className="synergyHeaderContainer">
                            <div className="synergyHeaderText">
                                {this.state.synergyArray[0][0]}
                            </div>
                        </div>
                        <div className="statsValues">
                            {this.state.synergyArray[0][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '50%'}}>
                        <div className="synergyHeaderContainer">
                            <div className="synergyHeaderText">
                                {this.state.synergyArray[1][0]}
                            </div>
                        </div>
                        <div className="statsValues">
                            {this.state.synergyArray[1][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '70%'}}>
                        <div className="synergyHeaderContainer">
                            <div className="synergyHeaderText">
                                {this.state.synergyArray[2][0]}
                            </div>
                        </div>
                        <div className="statsValues">
                            {this.state.synergyArray[2][1]}
                        </div>
                    </div>
                </div>
            );
        }
        else if(numOfSynergySymbols == 4){
            return (
                <div>
                    <div className="singleSynergy" style={{left: '27%'}}>
                        <div className="synergyHeaderContainer">
                            <div className="synergyHeaderText">
                                {this.state.synergyArray[0][0]}
                            </div>
                        </div>
                        <div className="statsValues">
                            {this.state.synergyArray[0][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '45%'}}>
                        <div className="synergyHeaderContainer">
                            <div className="synergyHeaderText">
                                {this.state.synergyArray[1][0]}
                            </div>
                        </div>
                        <div className="statsValues">
                            {this.state.synergyArray[1][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '65%'}}>
                        <div className="synergyHeaderContainer">
                            <div className="synergyHeaderText">
                                {this.state.synergyArray[2][0]}
                            </div>
                        </div>
                        <div className="statsValues">
                            {this.state.synergyArray[2][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '83%'}}>
                        <div className="synergyHeaderContainer">
                            <div className="synergyHeaderText">
                                {this.state.synergyArray[3][0]}
                            </div>
                        </div>
                        <div className="statsValues">
                            {this.state.synergyArray[3][1]}
                        </div>
                    </div>
                </div>
            );
        }
        else if(numOfSynergySymbols == 5){
            return (
                <div>
                    <div className="singleSynergy" style={{left: '25%'}}>
                        <div className="synergyHeaderContainer">
                            <div className="synergyHeaderText">
                                {this.state.synergyArray[0][0]}
                            </div>
                        </div>
                        <div className="statsValues">
                            {this.state.synergyArray[0][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '41%'}}>
                        <div className="synergyHeaderContainer">
                            <div className="synergyHeaderText">
                                {this.state.synergyArray[1][0]}
                            </div>
                        </div>
                        <div className="statsValues">
                            {this.state.synergyArray[1][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '56%'}}>
                        <div className="synergyHeaderContainer">
                            <div className="synergyHeaderText">
                                {this.state.synergyArray[2][0]}
                            </div>
                        </div>
                        <div className="statsValues">
                            {this.state.synergyArray[2][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '71%'}}>
                        <div className="synergyHeaderContainer">
                            <div className="synergyHeaderText">
                                {this.state.synergyArray[3][0]}
                            </div>
                        </div>
                        <div className="statsValues">
                            {this.state.synergyArray[3][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '86%'}}>
                        <div className="synergyHeaderContainer">
                            <div className="synergyHeaderText">
                                {this.state.synergyArray[4][0]}
                            </div>
                        </div>
                        <div className="statsValues">
                            {this.state.synergyArray[4][1]}
                        </div>
                    </div>
                </div>
            );
        }

    }

    render(){
        let styling = '';
        if(this.props.applyborder)
            styling = this.props.applyborder;
        return(
            <div className={`cardContainer ${styling}`} style={{background: '#be2f37'}}>
                <div className="containerTop thanksGiving_topContainer">
                    <div className="thanksGiving_cardtype cardType">

                    </div>
                    <div className="logoBackground primeTimeLogoBackground"
                         style={{backgroundImage: this.props.teamBackdrop}}>

                    </div>
                    <div style={{backgroundImage: this.props.headshot}}
                         className="playerFaceNormalCard thanksGiving_HeadshotContainer">

                    </div>
                    <div className="thanksGiving_overlay">

                    </div>
                    <div className="thanksGiving_tag overall">
                        <div className="overallContainer">
                            <div className="type">

                            </div>
                            <div className="overallText thanksGiving_OverallText">
                                {this.props.overall}
                            </div>

                        </div>
                    </div>
                </div>
                <div className="playerInfoContainer" style={{color: '#ffffff !important'}}>

                    <div className="playerName" style={{lineHeight: 1}}>
                        <div className="playerNameMainContainer thanksGiving_playerNameMainContainer">
                            <div className="thanksGiving_FirstName">
                                {this.props.firstName}
                            </div>
                            <div className="thanksGiving_LastName">
                                {this.props.lastName}
                            </div>
                        </div>

                    </div>
                    <div className="logoPosition thanksGiving_logoPosition" style={{backgroundImage: this.props.teamLogo,
                        width: '4.5em', height: '4.5em'}}>
                    </div>
                    <div className="teamNameContainer thanksGiving_teamNameContainer">
                        <div className="primeTimeTeamNameContainer thanksGiving_primeTimeTeamNameContainer"
                             style={{left: '8%',right: 'unset',top: '0.83em',fontSize: '.6em',fontWeight: 'bold'}}>
                            {this.props.teamName}
                        </div>
                        <div className="playerPosition">

                        </div>
                        <div className="playerTextContainer" style={{top:'20%',fontFamily:'Open Sans, sans-serif'}}>
                            <strong>{this.props.position}</strong>
                        </div>
                    </div>
                    <div className="bottomContainer">

                        <div className="statsContainer">
                            <div className="synergyHeader">
                                <div className="synergyContainerTitle">
                                    <strong>Synergy</strong>
                                </div>
                            </div>
                            <div className="statsTable">
                                {this.renderSynergySymbols()}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

document.registerReact('thanksgiving-card', ThanksgivingCard);