import React, {Component} from 'react';
import ReactiveElements from 'reactive-elements';
import axios from "axios";

export default class AlumniCard extends Component{

    /*constructor(props){
        super(props);
        this.state = {
            info: []
        }
    }

    componentDidMount(){
        axios({
            method: 'post',
            url: '/cardinfo',
            data: {'id' : this.props.id, 'alumni' : true}
        }).then(res => {
            this.setState({info: res.data})
        })
    }

    renderSynergySymbols(){
        let numOfSynergySymbols = this.state.info.synergy_symbols.length;

        if(numOfSynergySymbols == 1){
            return (
                <div className="singleSynergy">
                    <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                        <div className="synergyHeaderText xmas_synergyTextValue">
                            {this.state.info.synergy_symbols[0][0]}
                        </div>
                    </div>
                    <div className="statsValues xmas_synergyValue">
                        {this.state.info.synergy_symbols[0][1]}
                    </div>
                </div>
            );
        }
        else if(numOfSynergySymbols == 2){
            return (
                <div>
                    <div className="singleSynergy" style={{left: '30%'}}>
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.info.synergy_symbols[0][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.info.synergy_symbols[0][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '60%'}}>
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.info.synergy_symbols[1][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.info.synergy_symbols[1][1]}
                        </div>
                    </div>
                </div>
            );
        }
        else if(numOfSynergySymbols == 3){
            return (
                <div>
                    <div className="singleSynergy" style={{left: '20%'}}>
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.info.synergy_symbols[0][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.info.synergy_symbols[0][1]}
                        </div>
                    </div>
                    <div className="singleSynergy">
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.info.synergy_symbols[1][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.info.synergy_symbols[1][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '70%'}}>
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.info.synergy_symbols[2][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.info.synergy_symbols[2][1]}
                        </div>
                    </div>
                </div>
            );
        }
        else if(numOfSynergySymbols == 4){
            return (
                <div>
                    <div className="singleSynergy" style={{left: '17%'}}>
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.info.synergy_symbols[0][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.info.synergy_symbols[0][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '35%'}}>
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.info.synergy_symbols[1][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.info.synergy_symbols[1][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '55%'}}>
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.info.synergy_symbols[2][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.info.synergy_symbols[2][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '72%'}}>
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.info.synergy_symbols[3][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.info.synergy_symbols[3][1]}
                        </div>
                    </div>
                </div>
            );
        }
        else if(numOfSynergySymbols == 5){
            return (
                <div>
                    <div className="singleSynergy" style={{left: '7%'}}>
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.info.synergy_symbols[0][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.info.synergy_symbols[0][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '25%'}}>
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.info.synergy_symbols[1][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.info.synergy_symbols[1][1]}
                        </div>
                    </div>
                    <div className="singleSynergy">
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.info.synergy_symbols[2][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.info.synergy_symbols[2][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '65%'}}>
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.info.synergy_symbols[3][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.info.synergy_symbols[3][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '83%'}}>
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.info.synergy_symbols[4][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.info.synergy_symbols[4][1]}
                        </div>
                    </div>
                </div>
            );
        }

    }

    render(){
        if(!this.state.info.synergy_symbols){
            return null;
        }
        else{
            return(

                <div className="cardContainer" style={{background: '#be2f37'}}>
                    <div className="containerTop">
                        <div className="playerPosition ">
                            <div className="playerTextContainer alumni_playerPosition">
                                <strong>
                                    {this.state.info.position}
                                </strong>
                            </div>
                        </div>
                        <div className="alumni_backgroundColor cardType">

                        </div>
                        <div className="logoBackground primeTimeLogoBackground"
                             style={{backgroundImage: this.state.info.team_backdrop}}>

                        </div>
                        <div style={{backgroundImage: this.state.info.headshot}}
                             className="playerFaceNormalCard alumni_Headshots">

                        </div>
                        <div className="alumni_overlay cardType">

                        </div>
                        <div className="overall">
                            <div className="overallContainer">
                                <div className="overallText alumni_OverallText">
                                    {this.state.info.overall}
                                </div>

                            </div>
                        </div>
                    </div>
                    <div className="playerInfoContainer" style={{color: '#ffffff !important'}}>
                        <div className="teamNameContainer alumni_TeamNameContainer">
                            <div className="alumni_TeamNameText">
                                {this.state.info.team_name}{' '} ALUMNI
                            </div>
                            <div className="logoPosition  alumni_logoPosition"
                                 style={{backgroundImage: this.state.info.team_logo}}>
                            </div>
                        </div>
                        <div className="playerName" style={{lineHeight: 1}}>
                            <div className="playerNameMainContainer alumni_PlayerNameContainer">
                                <div className="primeTimeFirstName">
                                    {this.state.info.first_name}
                                </div>
                                <div className="primeTimeLastName">
                                    {this.state.info.last_name}
                                </div>
                            </div>

                        </div>
                        <div className="bottomContainer alumni_bottomContainer">
                            <div className="statsContainer alumni_StatsContainer">
                                <div className="synergyHeader ">
                                    <div className="synergyContainerTitle alumni_SynergyHeader">
                                        <strong>Synergy</strong>
                                    </div>
                                </div>
                                <div className="statsTable alumni_statsTable">
                                    {this.renderSynergySymbols()}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            )
        }
    }*/

    constructor(props){
        super(props);
        this.state = {
            synergyArray: []
        }
    }

    componentDidMount(){
        {this.setSynergyArray(this.props.synergySymbols)}
    }

    setSynergyArray(synergyString){
        let originalSynergyArray = synergyString.split(',');
        let finalSynergyArray = [];
        for(let i = 0; i < originalSynergyArray.length; i++){
            if(i%2 == 1)
                continue;
            let tempArray = [originalSynergyArray[i], originalSynergyArray[i+1]];
            finalSynergyArray.push(tempArray);
        }
        this.setState({
            synergyArray: finalSynergyArray
        });
    }

    renderSynergySymbols(){
        let numOfSynergySymbols = this.state.synergyArray.length;

        if(numOfSynergySymbols == 1){
            return (
                <div className="singleSynergy">
                    <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                        <div className="synergyHeaderText xmas_synergyTextValue">
                            {this.state.synergyArray[0][0]}
                        </div>
                    </div>
                    <div className="statsValues xmas_synergyValue">
                        {this.state.synergyArray[0][1]}
                    </div>
                </div>
            );
        }
        else if(numOfSynergySymbols == 2){
            return (
                <div>
                    <div className="singleSynergy" style={{left: '30%'}}>
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.synergyArray[0][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.synergyArray[0][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '60%'}}>
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.synergyArray[1][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.synergyArray[1][1]}
                        </div>
                    </div>
                </div>
            );
        }
        else if(numOfSynergySymbols == 3){
            return (
                <div>
                    <div className="singleSynergy" style={{left: '20%'}}>
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.synergyArray[0][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.synergyArray[0][1]}
                        </div>
                    </div>
                    <div className="singleSynergy">
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.synergyArray[1][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.synergyArray[1][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '70%'}}>
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.synergyArray[2][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.synergyArray[2][1]}
                        </div>
                    </div>
                </div>
            );
        }
        else if(numOfSynergySymbols == 4){
            return (
                <div>
                    <div className="singleSynergy" style={{left: '17%'}}>
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.synergyArray[0][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.synergyArray[0][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '35%'}}>
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.synergyArray[1][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.synergyArray[1][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '55%'}}>
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.synergyArray[2][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.synergyArray[2][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '72%'}}>
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.synergyArray[3][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.synergyArray[3][1]}
                        </div>
                    </div>
                </div>
            );
        }
        else if(numOfSynergySymbols == 5){
            return (
                <div>
                    <div className="singleSynergy" style={{left: '7%'}}>
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.synergyArray[0][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.synergyArray[0][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '25%'}}>
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.synergyArray[1][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.synergyArray[1][1]}
                        </div>
                    </div>
                    <div className="singleSynergy">
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.synergyArray[2][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.synergyArray[2][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '65%'}}>
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.synergyArray[3][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.synergyArray[3][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '83%'}}>
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.synergyArray[4][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.synergyArray[4][1]}
                        </div>
                    </div>
                </div>
            );
        }

    }

    render(){
        let styling = '';
        if(this.props.applyborder)
            styling = this.props.applyborder;

        return(
            <div className={`cardContainer ${styling}`} style={{background: '#be2f37'}}>
                <div className="containerTop">
                    <div className="playerPosition ">
                        <div className="playerTextContainer alumni_playerPosition">
                            <strong>
                                {this.props.position}
                            </strong>
                        </div>
                    </div>
                    <div className="alumni_backgroundColor cardType">

                    </div>
                    <div className="logoBackground primeTimeLogoBackground"
                         style={{backgroundImage: this.props.teamBackdrop}}>

                    </div>
                    <div style={{backgroundImage: this.props.headshot}}
                         className="playerFaceNormalCard alumni_Headshots">

                    </div>
                    <div className="alumni_overlay cardType">

                    </div>
                    <div className="overall">
                        <div className="overallContainer">
                            <div className="overallText alumni_OverallText">
                                {this.props.overall}
                            </div>

                        </div>
                    </div>
                </div>
                <div className="playerInfoContainer" style={{color: '#ffffff !important'}}>
                    <div className="teamNameContainer alumni_TeamNameContainer">
                        <div className="alumni_TeamNameText">
                            {this.props.teamName}{' '} ALUMNI
                        </div>
                        <div className="logoPosition  alumni_logoPosition"
                             style={{backgroundImage: this.props.teamLogo}}>
                        </div>
                    </div>
                    <div className="playerName" style={{lineHeight: 1}}>
                        <div className="playerNameMainContainer alumni_PlayerNameContainer">
                            <div className="primeTimeFirstName">
                                {this.props.firstName}
                            </div>
                            <div className="primeTimeLastName">
                                {this.props.lastName}
                            </div>
                        </div>

                    </div>
                    <div className="bottomContainer alumni_bottomContainer">
                        <div className="statsContainer alumni_StatsContainer">
                            <div className="synergyHeader ">
                                <div className="synergyContainerTitle alumni_SynergyHeader">
                                    <strong>Synergy</strong>
                                </div>
                            </div>
                            <div className="statsTable alumni_statsTable">
                                {this.renderSynergySymbols()}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        );
    }

}

document.registerReact('alumni-card', AlumniCard);