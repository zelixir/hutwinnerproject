import React, {Component} from 'react';
import ReactiveElements from 'reactive-elements';
import axios from "axios";

export default class TOTWGCard extends Component{

    constructor(props){
        super(props);
        this.state = {
            synergyArray: []
        }
    }

    componentDidMount(){
        {this.setSynergyArray(this.props.synergySymbols)}
    }

    setSynergyArray(synergyString){
        let originalSynergyArray = synergyString.split(',');
        let finalSynergyArray = [];
        for(let i = 0; i < originalSynergyArray.length; i++){
            if(i%2 == 1)
                continue;
            let tempArray = [originalSynergyArray[i], originalSynergyArray[i+1]];
            finalSynergyArray.push(tempArray);
        }
        this.setState({
            synergyArray: finalSynergyArray
        });
    }

    renderSynergySymbols(){
        let numOfSynergySymbols = this.state.synergyArray.length;

        if(numOfSynergySymbols == 1){
            return (
                <div className="singleSynergy">
                    <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                        <div className="synergyHeaderText primeTimeSynergy">
                            {this.state.synergyArray[0][0]}
                        </div>
                    </div>
                    <div className="statsValues primeTimeSynergyValue">
                        {this.state.synergyArray[0][1]}
                    </div>
                </div>
            );
        }
        else if(numOfSynergySymbols == 2){
            return (
                <div>
                    <div className="singleSynergy" style={{left: '35%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[0][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[0][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '55%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[1][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[1][1]}
                        </div>
                    </div>
                </div>
            );
        }
        else if(numOfSynergySymbols == 3){
            return (
                <div>
                    <div className="singleSynergy" style={{left: '30%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[0][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[0][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '50%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[1][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[1][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '70%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[2][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[2][1]}
                        </div>
                    </div>
                </div>
            );
        }
        else if(numOfSynergySymbols == 4){
            return (
                <div>
                    <div className="singleSynergy" style={{left: '27%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[0][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[0][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '45%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[1][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[1][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '65%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[2][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[2][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '83%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[3][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[3][1]}
                        </div>
                    </div>
                </div>
            );
        }
        else if(numOfSynergySymbols == 5){
            return (
                <div>
                    <div className="singleSynergy" style={{left: '25%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[0][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[0][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '41%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[1][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[1][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '56%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[2][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[2][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '71%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[3][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[3][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '86%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[4][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[4][1]}
                        </div>
                    </div>
                </div>
            );
        }

    }

    render(){
        let styling = '';
        if(this.props.applyborder)
            styling = this.props.applyborder;
        return(
            <div className={`cardContainer ${styling}`} style={{background: '#333'}}>
                <div className="containerTop">
                    <div className="teamOfTheWeek_backgroundGold cardType">
                    </div>
                    <div className="teamOfTheWeek_footerGold cardType">
                    </div>
                    <div className="teamOfTheWeek_lineGold cardType teamOfTheWeek_line">

                    </div>
                    <div style={{backgroundImage: this.props.headshot}}
                         className="playerFaceNormalCard">

                    </div>
                    <div className="teamOfTheWeek_textGold cardType">
                    </div>
                    <div className="teamOfTheWeek_tagGold cardType">
                        <div className="overallContainer teamOfTheWeekOverallContainer">
                            <div
                                className="type teamOfTheWeek_OverallType teamOfTheWeek_OverallTypeGold teamOfTheWeek_Degrees goldColor">
                                OVR
                            </div>
                            <div className="overallText teamOfTheWeek_OverallText teamOfTheWeek_Degrees">
                                {this.props.overall}
                            </div>

                        </div>
                    </div>
                </div>
                <div className="playerInfoContainer" style={{color: '#ffffff !important'}}>
                    <div className="playerPosition teamOfTheWeek_Degrees teamOfTheWeek_PositionContainer">

                        <div className="playerTextContainer teamOfTheWeek_PositionText goldColor">
                            <strong>
                                {this.props.position}
                            </strong>
                        </div>
                    </div>
                    <div className="playerName teamOfTheWeek_PlayerNameContainer teamOfTheWeek_Degrees"
                         style={{lineHeight: 1}}>
                        <div className="playerNameMainContainer teamOfTheWeek_PlayerNameMainContainer goldColor">
                            {this.props.firstName}
                            {' '}
                            {this.props.lastName}
                        </div>
                    </div>
                    <div className="teamNameContainer teamOfTheWeek_TeamNameContainer teamOfTheWeek_Degrees">
                        <div className="primeTimeTeamNameContainer goldColor">
                            {this.props.teamName}
                        </div>
                    </div>
                    <div className="bottomContainer teamOfTheWeek_bottomContainer teamOfTheWeek_Degrees ">
                        <div className="logoPosition teamOfTheWeek_LogoPosition"
                             style={{backgroundImage: this.props.teamLogo}}>

                        </div>
                        <div className="statsContainer teamOfTheWeek_SynergyContainer ">
                            <div className="synergyHeader teamOfTheWeek_SynergyHeader ">
                                <div className="synergyContainerTitle goldColor">
                                    <strong>Synergy</strong>
                                </div>
                            </div>
                            <div className="statsTable ">
                                {this.renderSynergySymbols()}
                            </div>
                        </div>
                    </div>
                </div>


            </div>

        )
    }
}

document.registerReact('totwg-card', TOTWGCard);