import React, {Component} from 'react';
import ReactiveElements from 'reactive-elements';
import axios from "axios";

export default class MilestoneCard extends Component{

    constructor(props){
        super(props);
        this.state = {
            synergyArray: []
        }
    }

    componentDidMount(){
        {this.setSynergyArray(this.props.synergySymbols)}
    }

    setSynergyArray(synergyString){
        let originalSynergyArray = synergyString.split(',');
        let finalSynergyArray = [];
        for(let i = 0; i < originalSynergyArray.length; i++){
            if(i%2 == 1)
                continue;
            let tempArray = [originalSynergyArray[i], originalSynergyArray[i+1]];
            finalSynergyArray.push(tempArray);
        }
        this.setState({
            synergyArray: finalSynergyArray
        });
    }

    renderSynergySymbols(){
        let numOfSynergySymbols = this.state.synergyArray.length;

        if(numOfSynergySymbols == 1){
            return (
                <div className="singleSynergy">
                    <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                        <div className="synergyHeaderText xmas_synergyTextValue">
                            {this.state.synergyArray[0][0]}
                        </div>
                    </div>
                    <div className="statsValues xmas_synergyValue">
                        {this.state.synergyArray[0][1]}
                    </div>
                </div>
            );
        }
        else if(numOfSynergySymbols == 2){
            return (
                <div>
                    <div className="singleSynergy" style={{left: '30%'}}>
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.synergyArray[0][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.synergyArray[0][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '60%'}}>
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.synergyArray[1][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.synergyArray[1][1]}
                        </div>
                    </div>
                </div>
            );
        }
        else if(numOfSynergySymbols == 3){
            return (
                <div>
                    <div className="singleSynergy" style={{left: '20%'}}>
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.synergyArray[0][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.synergyArray[0][1]}
                        </div>
                    </div>
                    <div className="singleSynergy">
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.synergyArray[1][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.synergyArray[1][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '70%'}}>
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.synergyArray[2][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.synergyArray[2][1]}
                        </div>
                    </div>
                </div>
            );
        }
        else if(numOfSynergySymbols == 4){
            return (
                <div>
                    <div className="singleSynergy" style={{left: '17%'}}>
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.synergyArray[0][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.synergyArray[0][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '35%'}}>
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.synergyArray[1][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.synergyArray[1][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '55%'}}>
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.synergyArray[2][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.synergyArray[2][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '72%'}}>
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.synergyArray[3][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.synergyArray[3][1]}
                        </div>
                    </div>
                </div>
            );
        }
        else if(numOfSynergySymbols == 5){
            return (
                <div>
                    <div className="singleSynergy" style={{left: '7%'}}>
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.synergyArray[0][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.synergyArray[0][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '25%'}}>
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.synergyArray[1][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.synergyArray[1][1]}
                        </div>
                    </div>
                    <div className="singleSynergy">
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.synergyArray[2][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.synergyArray[2][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '65%'}}>
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.synergyArray[3][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.synergyArray[3][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '83%'}}>
                        <div className="synergyHeaderContainer xmas_synergyHeaderContainer">
                            <div className="synergyHeaderText xmas_synergyTextValue">
                                {this.state.synergyArray[4][0]}
                            </div>
                        </div>
                        <div className="statsValues xmas_synergyValue">
                            {this.state.synergyArray[4][1]}
                        </div>
                    </div>
                </div>
            );
        }

    }

    render(){
        let styling = '';
        if(this.props.applyborder)
            styling = this.props.applyborder;
        return(
            <div className={`cardContainer ${styling}`} style={{background: '#e8a328'}}>
                <div className="containerTop">
                    <div className="milestone_cardType cardType">

                    </div>
                    <div className="cardType" style={{backgroundImage: this.props.flair}}>

                    </div>
                    <div className="logoBackground milestone_logoBackground"
                         style={{backgroundImage: this.props.teamBackdrop}}>

                    </div>
                    <div style={{backgroundImage: this.props.headshot}}
                         className="playerFaceNormalCard milestone_playerFaceNormalCard">

                    </div>
                    <div className="milestone_tag overall">
                        <div className="overallContainer">
                            <div className="type milestone_type">
                                GOLD
                            </div>
                            <div className="overallText milestone_overallText">
                                {this.props.overall}
                            </div>

                        </div>
                    </div>
                </div>
                <div className="playerInfoContainer">
                    <div className="playerPosition">

                        <div className="playerTextContainer milestone_playerTextContainer">
                            <strong>
                                {this.props.position}
                            </strong>
                        </div>
                    </div>
                    <div className="playerName" style={{lineHeight: 1}}>
                        <div className="playerNameMainContainer milestone_playerNameMainContainer">
                            <div className="milestone_firstName">
                                {this.props.firstName}
                            </div>
                            <div className="milestone_lastName">
                                {this.props.lastName}
                            </div>
                        </div>

                    </div>
                    <div className="teamNameContainer">
                        <div className="logoPosition milestone_logoPosition"
                             style={{backgroundImage: this.props.teamLogo}}>
                        </div>
                        <div className="primeTimeTeamNameContainer milestone_primeTimeTeamNameContainer">
                            {this.props.teamName}
                        </div>
                    </div>
                    <div className="bottomContainer milestone_bottomContainer">
                        <div className="statsContainer">
                            <div className="synergyHeader milestone_synergyHeader">
                                <div className="synergyContainerTitle">
                                    <strong>Synergy</strong>
                                </div>
                            </div>
                            <div className="statsTable">
                                {this.renderSynergySymbols()}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}

document.registerReact('milestone-card', MilestoneCard);