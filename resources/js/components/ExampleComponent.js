import React, {Component} from 'react';
import axios from "axios";

export default class ExampleComponent extends Component{

    renderNormal(){
        return (<div>
            <h1>not this not this not this</h1>
        </div>);
    }

    render(){
        if(this.props.slider){
            return (<div><h1>this this this</h1></div>);
        }
        return this.renderNormal();
    }
}

document.registerReact('example-component', ExampleComponent);