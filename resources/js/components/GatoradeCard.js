import React, {Component} from 'react';
import ReactiveElements from 'reactive-elements';
import axios from "axios";

export default class GatoradeCard extends Component{

    constructor(props){
        super(props);
        this.state = {
            synergyArray: []
        }
    }

    componentDidMount(){
        {this.setSynergyArray(this.props.synergySymbols)}
    }

    setSynergyArray(synergyString){
        let originalSynergyArray = synergyString.split(',');
        let finalSynergyArray = [];
        for(let i = 0; i < originalSynergyArray.length; i++){
            if(i%2 == 1)
                continue;
            let tempArray = [originalSynergyArray[i], originalSynergyArray[i+1]];
            finalSynergyArray.push(tempArray);
        }
        this.setState({
            synergyArray: finalSynergyArray
        });
    }

    renderSynergySymbols(){
        let numOfSynergySymbols = this.state.synergyArray.length;

        if(numOfSynergySymbols == 1){
            return (
                <div className="singleSynergy">
                    <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                        <div className="synergyHeaderText primeTimeSynergy">
                            {this.state.synergyArray[0][0]}
                        </div>
                    </div>
                    <div className="statsValues primeTimeSynergyValue">
                        {this.state.synergyArray[0][1]}
                    </div>
                </div>
            );
        }
        else if(numOfSynergySymbols == 2){
            return (
                <div>
                    <div className="singleSynergy" style={{left: '35%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[0][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[0][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '55%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[1][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[1][1]}
                        </div>
                    </div>
                </div>
            );
        }
        else if(numOfSynergySymbols == 3){
            return (
                <div>
                    <div className="singleSynergy" style={{left: '30%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[0][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[0][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '50%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[1][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[1][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '70%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[2][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[2][1]}
                        </div>
                    </div>
                </div>
            );
        }
        else if(numOfSynergySymbols == 4){
            return (
                <div>
                    <div className="singleSynergy" style={{left: '27%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[0][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[0][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '45%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[1][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[1][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '65%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[2][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[2][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '83%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[3][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[3][1]}
                        </div>
                    </div>
                </div>
            );
        }
        else if(numOfSynergySymbols == 5){
            return (
                <div>
                    <div className="singleSynergy" style={{left: '25%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[0][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[0][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '41%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[1][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[1][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '56%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[2][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[2][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '71%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[3][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[3][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '86%'}}>
                        <div className="synergyHeaderContainer primeTimeSynergyHeaderContainer">
                            <div className="synergyHeaderText primeTimeSynergy">
                                {this.state.synergyArray[4][0]}
                            </div>
                        </div>
                        <div className="statsValues primeTimeSynergyValue">
                            {this.state.synergyArray[4][1]}
                        </div>
                    </div>
                </div>
            );
        }

    }

    render(){
        let styling = '';
        if(this.props.applyborder)
            styling = this.props.applyborder;
        return(
            <div className={`cardContainer ${styling}`} style={{background: this.props.teamColor}}>
                <div className="containerTop">
                    <div className="gatorade_cardType cardType">

                    </div>
                    <div style={{backgroundImage: this.props.headshot}}
                         className="playerFaceNormalCard gatorade_playerFaceNormalCard">

                    </div>
                    <div className="gatorade_overlay">

                    </div>
                    <div className="Gatorade_tag overall">
                        <div className="overallContainer">
                            <div className="overallText gatorade_overallText">
                                {this.props.overall}
                            </div>

                        </div>

                    </div>
                    <div className="playerPosition gatorade_playerPosition">

                        <div className="playerTextContainer gatorade_playerTextContainer">
                            <strong>
                                {this.props.position}
                            </strong>
                        </div>
                    </div>
                </div>
                <div className="playerInfoContainer">
                    <div className="playerName" style={{lineHeight: 1}}>
                        <div className="playerNameMainContainer gatorade_playerNameMainContainer">
                            <div className="gatorade_firstName">
                                {this.props.firstName}
                            </div>
                            <div className="gatorade_lastName">
                                {this.props.lastName}
                            </div>
                        </div>

                    </div>
                    <div className="teamNameContainer">
                        <div className="primeTimeTeamNameContainer gatorade_primeTimeTeamNameContainer">
                            {this.props.teamName}
                        </div>
                    </div>
                    <div className="bottomContainer milestone_bottomContainer">
                        <div className="statsContainer">
                            <div className="synergyHeader gatorade_synergyHeader">
                                <div className="synergyContainerTitle">
                                    <strong>Synergy</strong>
                                </div>
                            </div>
                            <div className="statsTable">
                                {this.renderSynergySymbols()}

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}

document.registerReact('gatorade-card', GatoradeCard);