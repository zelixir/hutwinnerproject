import React, {Component} from 'react';
import ReactiveElements from 'reactive-elements';
import axios from "axios";

export default class NormalCard extends Component{

    constructor(props){
        super(props);
        this.state = {
            synergyArray: []
        }
    }

    componentDidMount(){
        {this.setSynergyArray(this.props.synergySymbols)}
    }

    setSynergyArray(synergyString){
        let originalSynergyArray = synergyString.split(',');
        let finalSynergyArray = [];
        for(let i = 0; i < originalSynergyArray.length; i++){
            if(i%2 == 1)
                continue;
            let tempArray = [originalSynergyArray[i], originalSynergyArray[i+1]];
            finalSynergyArray.push(tempArray);
        }
        this.setState({
            synergyArray: finalSynergyArray
        });
    }

    renderSynergySymbols(){
        let numOfSynergySymbols = this.state.synergyArray.length;

        if(numOfSynergySymbols == 1){
            return (
                <div className="singleSynergy">
                    <div className="synergyHeaderContainer">
                        <div className="synergyHeaderText">
                            {this.state.synergyArray[0][0]}
                        </div>
                    </div>
                    <div className="statsValues">
                        {this.state.synergyArray[0][1]}
                    </div>
                </div>
            );
        }
        else if(numOfSynergySymbols == 2){
            return (
                <div>
                    <div className="singleSynergy" style={{left: '35%'}}>
                        <div className="synergyHeaderContainer">
                            <div className="synergyHeaderText">
                                {this.state.synergyArray[0][0]}
                            </div>
                        </div>
                        <div className="statsValues">
                            {this.state.synergyArray[0][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '55%'}}>
                        <div className="synergyHeaderContainer">
                            <div className="synergyHeaderText">
                                {this.state.synergyArray[1][0]}
                            </div>
                        </div>
                        <div className="statsValues">
                            {this.state.synergyArray[1][1]}
                        </div>
                    </div>
                </div>
            );
        }
        else if(numOfSynergySymbols == 3){
            return (
                <div>
                    <div className="singleSynergy" style={{left: '30%'}}>
                        <div className="synergyHeaderContainer">
                            <div className="synergyHeaderText">
                                {this.state.synergyArray[0][0]}
                            </div>
                        </div>
                        <div className="statsValues">
                            {this.state.synergyArray[0][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '50%'}}>
                        <div className="synergyHeaderContainer">
                            <div className="synergyHeaderText">
                                {this.state.synergyArray[1][0]}
                            </div>
                        </div>
                        <div className="statsValues">
                            {this.state.synergyArray[1][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '70%'}}>
                        <div className="synergyHeaderContainer">
                            <div className="synergyHeaderText">
                                {this.state.synergyArray[2][0]}
                            </div>
                        </div>
                        <div className="statsValues">
                            {this.state.synergyArray[2][1]}
                        </div>
                    </div>
                </div>
            );
        }
        else if(numOfSynergySymbols == 4){
            return (
                <div>
                    <div className="singleSynergy" style={{left: '27%'}}>
                        <div className="synergyHeaderContainer">
                            <div className="synergyHeaderText">
                                {this.state.synergyArray[0][0]}
                            </div>
                        </div>
                        <div className="statsValues">
                            {this.state.synergyArray[0][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '45%'}}>
                        <div className="synergyHeaderContainer">
                            <div className="synergyHeaderText">
                                {this.state.synergyArray[1][0]}
                            </div>
                        </div>
                        <div className="statsValues">
                            {this.state.synergyArray[1][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '65%'}}>
                        <div className="synergyHeaderContainer">
                            <div className="synergyHeaderText">
                                {this.state.synergyArray[2][0]}
                            </div>
                        </div>
                        <div className="statsValues">
                            {this.state.synergyArray[2][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '83%'}}>
                        <div className="synergyHeaderContainer">
                            <div className="synergyHeaderText">
                                {this.state.synergyArray[3][0]}
                            </div>
                        </div>
                        <div className="statsValues">
                            {this.state.synergyArray[3][1]}
                        </div>
                    </div>
                </div>
            );
        }
        else if(numOfSynergySymbols == 5){
            return (
                <div>
                    <div className="singleSynergy" style={{left: '25%'}}>
                        <div className="synergyHeaderContainer">
                            <div className="synergyHeaderText">
                                {this.state.synergyArray[0][0]}
                            </div>
                        </div>
                        <div className="statsValues">
                            {this.state.synergyArray[0][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '41%'}}>
                        <div className="synergyHeaderContainer">
                            <div className="synergyHeaderText">
                                {this.state.synergyArray[1][0]}
                            </div>
                        </div>
                        <div className="statsValues">
                            {this.state.synergyArray[1][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '56%'}}>
                        <div className="synergyHeaderContainer">
                            <div className="synergyHeaderText">
                                {this.state.synergyArray[2][0]}
                            </div>
                        </div>
                        <div className="statsValues">
                            {this.state.synergyArray[2][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '71%'}}>
                        <div className="synergyHeaderContainer">
                            <div className="synergyHeaderText">
                                {this.state.synergyArray[3][0]}
                            </div>
                        </div>
                        <div className="statsValues">
                            {this.state.synergyArray[3][1]}
                        </div>
                    </div>
                    <div className="singleSynergy" style={{left: '86%'}}>
                        <div className="synergyHeaderContainer">
                            <div className="synergyHeaderText">
                                {this.state.synergyArray[4][0]}
                            </div>
                        </div>
                        <div className="statsValues">
                            {this.state.synergyArray[4][1]}
                        </div>
                    </div>
                </div>
            );
        }

    }

    render(){
        let styling = '';
        if(this.props.applyborder)
            styling = this.props.applyborder;
        return(
            <div className={`cardContainer ${styling}`} style={{background: this.props.teamColor}}>
                <div className="containerTop">
                    <div className="gld_cardType cardType">

                    </div>
                    <div className="logoBackground"
                         style={{backgroundImage: this.props.teamBackdrop}}>

                    </div>
                    <div style={{backgroundImage: this.props.headshot}}
                         className="playerFaceNormalCard">

                    </div>
                    <div className="gld_overall overall">
                        <div className="overallContainer">
                            <div className="type">
                                GOLD
                            </div>
                            <div className="overallText">
                                {this.props.overall}
                            </div>

                        </div>
                    </div>
                </div>
                <div className="playerInfoContainer">
                    <div className="playerPosition">

                        <div className="playerTextContainer">
                            <strong>
                                {this.props.position}
                            </strong>
                        </div>
                    </div>
                    <div className="playerName" style={{lineHeight: 1}}>
                        <div className="playerNameMainContainer">
                            {this.props.firstName}
                            <br/>
                            {this.props.lastName}
                        </div>

                    </div>
                    <div className="teamNameContainer">
                        <div className="teamName teamNameNormal">
                            {this.props.teamName}
                        </div>
                    </div>
                    <div className="bottomContainer">
                        <div className="logoPosition" style={{backgroundImage: this.props.teamLogo}}>

                        </div>
                        <div className="statsContainer">
                            <div className="synergyHeader">
                                <div className="synergyContainerTitle">
                                    <strong>Synergy</strong>
                                </div>
                            </div>
                            <div className="statsTable">
                                {this.renderSynergySymbols()}
                            </div>
                        </div>
                    </div>

                </div>
                <div className="gld_footer footer">

                </div>
            </div>
        )
    }
}

document.registerReact('normal-card', NormalCard);