<?php
/**
 * Created by PhpStorm.
 * User: samoumbarek
 * Date: 2018-12-25
 * Time: 9:52 PM
 */
use Illuminate\Database\Seeder;
use App\PaymentMethod;
class PaymentMethodSeeder extends Seeder
{

    public function run()
    {
        PaymentMethod::create([
            'method' => 'PayPal'
        ]);
        PaymentMethod::create(
            [
                'method'=>'Xsolla'
            ]
        );
    }
}