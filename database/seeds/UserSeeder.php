<?php
/**
 * Created by PhpStorm.
 * User: samoumbarek
 * Date: 2018-12-25
 * Time: 9:49 PM
 */

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    public function run()
    {
        User::create([
            'firstname' => 'test',
            'lastname' => 'tester',
            'email' => 'test@example.com',
            'password' => Hash::make('helloworld'),
            'console' => 'ps4'
        ]);

        User::create(
            [
                'firstname'=>'sammy',
                'lastname' => 'chaouki',
                'email'=>'helloworld@gmail.com',
                'password' =>Hash::make('helloworld'),
                'console' => 'xbox'
            ]
        );
    }

}