<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(PaymentMethodSeeder::class);
        //$this->call(StatusSeeder::class);
        //$this->call(AdminSeeder::class);
        //$this->call(CardSeeder::class);
        $this->call(PackSeeder::class);

        //$this->call(PaymentMethodSeeder::class);
        //$this->call(StatusSeeder::class);
       //$this->call(CardSeeder::class);
        //$this->call(UserSeeder::class);
        //$this->call(AdminSeeder::class);


        //$this->call(TransactionSeeder::class);
        //$this->call(CardPacksSeeder::class);
        //$this->call(InventorySeeder::class);
        //$this->call(PackCoverSeeder::class);

    }
}
