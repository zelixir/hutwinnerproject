<?php
/**
 * Created by PhpStorm.
 * User: samoumbarek
 * Date: 2018-12-25
 * Time: 9:51 PM
 */
use Illuminate\Database\Seeder;
use \App\Card;
use App\Pack;
class CardPacksSeeder extends Seeder
{
    public function run()
    {
       $card = Card::find(1);
       $card2 = Card::find(2);
       $pack = Pack::find(1);
       $card->packs()->attach($pack->id, [ 'probability' => 8]);
       $card2->packs()->attach($pack->id, ['probability' => 10]);
    }
}