<?php
/**
 * Created by PhpStorm.
 * User: samoumbarek
 * Date: 2018-12-25
 * Time: 9:50 PM
 */

use Illuminate\Database\Seeder;
use App\Card;
use App\CardType;
use Rap2hpoutre\FastExcel\FastExcel;

class CardSeeder extends Seeder

{
    public function run()
    {
        $collection = (new FastExcel)->import('storage/app/Team.csv', function ($line) {
            $keys = array_keys($line);
            \App\Team::firstOrCreate([
                'team_abr' => $line[$keys[0]],
                'team_name' => $line[$keys[1]],
                'color' => $line[$keys[2]]
            ]);
        });
        $collection = (new FastExcel)->import('storage/app/Datasheet.xlsx', function ($line) {
            $keys = array_keys($line);
            $cardType = CardType::firstOrCreate(['type' => $line[$keys[0]]]);
            $team = \App\Team::firstOrCreate(['team_abr' => $line[$keys[2]]]);
            if (trim($line[$keys[7]]) != 'N/A' && trim($line[$keys[7]]) != 'n/a') {
                Card::create(
                    [
                        'type_id' => $cardType->id,
                        'league' => $line[$keys[1]],
                        'team' => $team->team_abr,
                        'player_name' => $line[$keys[3]],
                        'position' => $line[$keys[4]],
                        'synergy' => $line[$keys[5]],
                        'overall' => $line[$keys[6]],
                        'xbox_price' => $line[$keys[8]],
                        'ps4_price' => $line[$keys[7]],
                        'year' => \Illuminate\Support\Facades\Config::get('gameversion.currentVersion')
                    ]
                );
            }
        });

    }

}