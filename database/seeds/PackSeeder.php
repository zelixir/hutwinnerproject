<?php
/**
 * Created by PhpStorm.
 * User: samoumbarek
 * Date: 2018-12-25
 * Time: 9:49 PM
 */

use Illuminate\Database\Seeder;
use App\Pack;
use App\Card_pack;
use App\Card;
use App\CardType;
use Rap2hpoutre\FastExcel\FastExcel;

class PackSeeder extends Seeder
{

    public function run()
    {
        $currentTeam = '';
        $createPackObject = true;
        $typeTeam = false;

        $currentPackId = 1;
        $collection = (new FastExcel)->import('storage/app/Packs.xlsx', function ($line) use (&$currentTeam, &$createPackObject, &$currentPackId,&$typeTeam) {
            $keys = array_keys($line);
            var_dump($keys);
            var_dump($line);
            if($line[$keys[0]] =='TYPE PACK'){
                $typeTeam= true;
            }
            else if (empty(trim($line[$keys[0]]))) {
                $this->getXboxPackSize($currentPackId );
                $this->getPS4PackPrice($currentPackId);
                $createPackObject = true;
            } else {
                if ($createPackObject == true) {
                    if($typeTeam==true){
                        $pack = Pack::create(
                            [
                                'name' => 'All-' . $line[$keys[0]] . ' Pack',
                                'xbox_price' => 0.0,
                                'ps4_price' => 0.0,
                                'author_id' => null,
                                'image' => 'Pack_Cover.png',
                                'guaranteed_cards' => 4,
                            ]
                        );
                    }
                    else{
                        $pack = Pack::create(
                            [
                                'name' => 'All-' . $line[$keys[2]] . ' Pack',
                                'xbox_price' => 0.0,
                                'ps4_price' => 0.0,
                                'author_id' => null,
                                'image' => strtolower($line[$keys[2]]) . '.png',
                                'guaranteed_cards' => 4,
                            ]
                        );
                        $currentTeam = $line[$keys[2]];
                    }

                    $currentPackId = $pack->id;
                    $createPackObject = false;
                }
                $cardType = CardType::firstOrCreate(['type' => $line[$keys[0]]]);
                if($typeTeam== true){
                    $cardObj = Card::firstOrCreate([
                        'type_id' => $cardType->id,
                        'league' => $line[$keys[1]],
                        'team' => $line[$keys[2]],
                        'player_name' => $line[$keys[3]],
                        'position' => $line[$keys[4]],
                        'synergy' => $line[$keys[5]],
                        'overall' => $line[$keys[6]],
                        'year' => \Illuminate\Support\Facades\Config::get('gameversion.currentVersion')]);

                    Card_pack::create(
                        ['probability' => $line[$keys[7]],
                            'card_id' => $cardObj->id,
                            'pack_id' => $currentPackId]
                    );
                }
                else{
                    $cardObj = Card::firstOrCreate([
                        'type_id' => $cardType->id,
                        'league' => $line[$keys[1]],
                        'team' => $currentTeam,
                        'player_name' => $line[$keys[3]],
                        'position' => $line[$keys[4]],
                        'synergy' => $line[$keys[5]],
                        'overall' => $line[$keys[6]],
                        'year' => \Illuminate\Support\Facades\Config::get('gameversion.currentVersion')]);

                    Card_pack::create(
                        ['probability' => $line[$keys[7]],
                            'card_id' => $cardObj->id,
                            'pack_id' => $currentPackId]
                    );
                }


            }

        });
    }
    private function getPS4PackPrice( $packId)
    {
        $total = 0;
        $pack = Pack::find($packId);
        $cards =  $pack->cards()->get();
        foreach($cards as $card){
            $total += (($card->ps4_price / \Illuminate\Support\Facades\Config::get('coins.ps4_price')) *  $card->pivot->probability) * $pack->guaranteed_cards;
        }

        $pack->ps4_price = ceil($total/100) * 100;
        $pack->save();
    }

    private function getXboxPackSize( $packId)
    {
        $total = 0;
        $pack = Pack::find($packId);
        $cards =  $pack->cards()->get();
        foreach($cards as $card){
            $total += (($card->xbox_price / \Illuminate\Support\Facades\Config::get('coins.xbox_price')) *  $card->pivot->probability) * $pack->guaranteed_cards;
        }
        $pack->xbox_price = ceil($total/100) * 100;
        $pack->save();
    }


}