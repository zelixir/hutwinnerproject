<?php

use Illuminate\Database\Seeder;
use App\PackCover;

class PackCoverSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PackCover::create([
            'image' => 'haha.png'
        ]);
        PackCover::create([
            'image' => 'okok.png'
        ]);
        PackCover::create([
            'image' => 'ayyt.png'
        ]);
        PackCover::create([
            'image' => 'pozz.png'
        ]);
    }
}
