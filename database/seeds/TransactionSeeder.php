<?php
/**
 * Created by PhpStorm.
 * User: samoumbarek
 * Date: 2018-12-25
 * Time: 9:51 PM
 */
use Illuminate\Database\Seeder;
use App\Transaction;
class TransactionSeeder extends Seeder
{
    public function run()
    {
        Transaction::create([
            'user_id'=>'1',
            'payment_id' => '001',
            'number' => 1090334,
            'amount' => 4,
            'description' => 'hello world'
        ]);
    }

}