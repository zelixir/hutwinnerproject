<?php
/**
 * Created by PhpStorm.
 * User: samoumbarek
 * Date: 2018-12-25
 * Time: 9:50 PM
 */
use Illuminate\Database\Seeder;
use App\Inventory;
class InventorySeeder extends Seeder
{
    public function run(){
        Inventory::create([
            'user_id'=>1,
            'card_id' => 1,
            'amount'=>2,
        ]);
        Inventory::create([
            'user_id'=>1,
            'card_id' => 2,
            'amount'=>6,
        ]);
        Inventory::create([
            'user_id'=>2,
            'card_id' => 3,
            'amount'=>4,
        ]);
        Inventory::create([
            'user_id'=>2,
            'card_id' => 4,
            'amount'=>1,
        ]);
    }


}