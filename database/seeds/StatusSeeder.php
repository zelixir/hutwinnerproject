<?php
/**
 * Created by PhpStorm.
 * User: samoumbarek
 * Date: 2019-01-02
 * Time: 1:22 PM
 */
use \Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    public function run(){
        \App\Status::create(
            [
                'type'=>'Approve'
            ]
        );
        \App\Status::create(
            [
                'type'=>'decline'
            ]
        );
        \App\Status::create(
            [
                'type'=>'pending'
            ]
        );
        \App\Status::create(
            [
                'type'=>'verify'
            ]
        );
        \App\Status::create(
            [
                'type'=>'unverified'
            ]
        );
    }
}