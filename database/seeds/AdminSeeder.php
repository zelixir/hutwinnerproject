<?php
/**
 * Created by PhpStorm.
 * User: samoumbarek
 * Date: 2018-12-25
 * Time: 9:49 PM
 */
use Illuminate\Database\Seeder;
use App\Admin;
class AdminSeeder extends Seeder
{
    public function run()
    {
        Admin::create([
            'firstname' => 'Test Tester',
            'lastname' => 'Last Tester',
            'admintype' => '1',
            'email' => 'test@example.com',
            'password' => Hash::make('helloworld')
        ]);
    }
}