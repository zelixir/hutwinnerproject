<?php
/**
 * Created by PhpStorm.
 * User: samoumbarek
 * Date: 2018-12-25
 * Time: 9:50 PM
 */

use Illuminate\Database\Seeder;
use App\CardType;
use Rap2hpoutre\FastExcel\FastExcel;

class CardTypeSeeder extends Seeder
{
    public function run()
    {
        $arr = $this->allTypes();
        foreach ($arr as $type) {
            CardType::create(
                ['type' => $type]
            );
        }
    }

    private function allTypes()
    {
        $collection = (new FastExcel)->import('storage/app/Datasheet.xlsx');
        $types = array_column(collect($collection)->unique('CARD')->toArray(), 'CARD');
        return $types;

    }
}