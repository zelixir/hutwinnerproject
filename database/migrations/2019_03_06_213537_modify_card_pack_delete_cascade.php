<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyCardPackDeleteCascade extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('card_pack', function(Blueprint $table){
            $table->dropForeign('card_pack_card_id_foreign');
            $table->foreign('card_id')->references('id')->on('cards')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('card_pack', function(Blueprint $table){
            $table->dropForeign('card_pack_card_id_foreign');
            $table->foreign('card_id')->references('id')->on('cards');
        });
    }
}
