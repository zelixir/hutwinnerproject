<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardsTable extends Migration
{
    /**
     * Run the migrations.
     * Each card represents a hockey player and each card has 10 attributes
     *  id: is a unique id that belongs to each card.
     *  type_id: is a foreign key that references to the 'card_type', which describes the type of the card. (E.g. LGD)
     *  league: the league in which the player plays. (E.g. NHL)
     *  team: the team in which the player plays. (E.g. EDM)
     *  player_name: the name of the player. (E.g. Paul Coffey)
     *  position: the position of the player. (E.g. RW)
     *  synergy: information about the player's synergy. (E.g. BM (1) NP (1) S (1))
     *  overall: the overall of the player. (E.g. 95)
     *  price: the price of the card.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('type_id')->unsigned();
            $table->string('league');
            $table->string('team');
            $table->string('player_name');
            $table->string('position');
            $table->string('synergy');
            $table->double('overall');
            $table->double('xbox_price')->default(0);
            $table->double('ps4_price')->default(0);
            $table->integer('year');
            $table->unique(['synergy','type_id','player_name','overall','league','team','position'],'cards_id');

            $table->foreign('type_id')->references('id')->on('card_types');
            $table->foreign('team')->references('team_abr')->on('team');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards');
    }
}
