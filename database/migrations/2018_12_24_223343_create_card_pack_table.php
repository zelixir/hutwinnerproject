<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardPackTable extends Migration
{
    /**
     * Run the migrations.
     * card_pack is a bridging table between cards table and packs table. It has 3 attributes
     *  card_id: a foreign key to the 'id' inside the cards table
     *  pack_id: the foreign key to the 'id' inside the packs table
     *  probability: the probability of getting a card in a pack
     *
     * @return void
     */
    public function up()
    {
        Schema::create('card_pack', function (Blueprint $table) {
            $table->integer('card_id')->unsigned();
            $table->integer('pack_id')->unsigned();
            $table->double('probability');

            $table->foreign('card_id')->references('id')->on('cards');
            $table->foreign('pack_id')->references('id')->on('packs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('card_packs');
    }
}
