<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Config;


class CreateUsersTable extends Migration
{
    /**
     * Run the migrations to create the Users table.
     * User table is defined as following.
     *  - A unique id to facilitate queries to the database
     *  - Each user will have a first name and last name
     *  - A user will have a unique email that they will use to login in the application
     *  - A integer that keeps tract of the times a user has not successfully logged in
     *  - A timestamp to keep track of the last invalid login -- will be used to redirect user to a 404 if he is tried multiple times
     *  - Each user will have an amount of coins that will allow them to purchase packs.
     *
     *
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('username')->unique();
            $table->string('firstname');
            $table->string('lastname');
            $table->string('email')->unique();
            $table->string('phone')->unique();
            $table->timestamp('time_failed')->nullable();
            $table->integer('invalid_tries')->default(0);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->integer('packs_created')->nullable();
            $table->timestamp('pack_first')->nullable();
            $table->integer('status_id')->unsigned()->default(Config::get('statuses.user.unverify'));
            $table->string('hut_team');
            $table->integer('coins')->default(Config::get('user.default_coins'));
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
