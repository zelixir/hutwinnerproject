<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacksTable extends Migration
{
    /**
     * Run the migrations.
     * Each pack has four attributes
     *  id: is a unique id that belongs to each pack.
     *  name: the name of the pack.
     *  price: the price of the pack (the sum of the card prices inside the pack).
     *  created_at: the time when the pack was created.
     *  author: the user id of the card author.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packs', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->double('xbox_price');
            $table->double('ps4_price');
            $table->integer('author_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('author_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packs');
    }
}
